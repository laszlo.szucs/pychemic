from __future__ import absolute_import
from __future__ import print_function

import numpy as np
import glob
import pickle
import matplotlib.pyplot as plt

from . import commons
from . import model

def find_nearest_index(array,value):
    '''
    Returns the index of the closes element in array.
    '''
    idx = (np.abs(array-value)).argmin()
    return idx


def findall(L, test):
    '''
    Returns the index of all elements in array L which are equal to value 
    argument.
    '''
    i=0
    indices = []
    while(True):
        try:
            # next value in list passing the test
            nextvalue = filter(test, L[i:])[0]

            # add index of this value in the index list,
            # by searching the value in L[i:] 
            indices.append(L.index(nextvalue, i))

            # iterate i, that is the next index from where to search
            i=indices[-1]+1
        #when there is no further "good value", filter returns [],
        # hence there is an out of range exeption
        except IndexError:
            return indices


def list_select(data, selectors):
    '''
    Given a data list and an integer or binary selector the function returns 
    a new data list containing only the selected (1 or True) elements.
    '''
    new_data = []
    # check element number
    if len(data) != len(selectors):
        raise ValueError('Data and selectors have different number of elements.')
    # loop over elements
    for i in range(len(data)):
        if selectors[i] in [1, True]:
            new_data.append(data[i])
    # return
    return new_data

def plot_abuns_med(plto, abuns=None, var=None, database=None, 
               species='CO',typ='dens', xrange=None, yrange=None, 
               xlog=True, xlabel=None, ylabel=None, facecolor = 'default', 
               edgecolor = 'default', linecolor = 'default',
               legloc = (0.8,0.9), title=None):
    '''
    Create line plot for physical condition - abundance distribution.
    
    :param plto: matplotlib axis object to plot into
    :param abuns: array containing the abundance data
    :param var: array containing the independent variable (usually density).
    :param database: instead of abuns and var, give a database (from extract_histo_db)
    :param species: name of specias to plot from database
    :param typ: options: [dens / temp / avs]
    '''
    if abuns == None or var == None:
        if database == None:
            raise ValueError("abuns and var or database must be given!")
        else:
            try:
                sindex = database['species'].index(species)
            except:
                raise ValueError("abuns & var not set, \
                                 {} not found in database".format(species))
            abuns = database['med_{}'.format(typ)][sindex]
            exec("{:s} = database[\'{:s}\']".format("var", typ))
            
    xlab = ''
    if xlog:
        var = np.log10(var)
        xlab = '$log\,$'
    
    if not xlabel:
        if typ == 'dens':
            xlabel = 'n [cm$^{-3}$]'
        elif typ == 'temp':
            xlabel = 'T$_{g}$ [K]'
        elif typ == 'avs':
            xlabel = 'A$_{v}$ [mag]'
        else:
            xlabel = 'unknown'
        xlabel = "{}{}".format(xlab,xlabel)
        
    if not ylabel:
        ylabel = '$log$ Fractional abundace'

    
    plto.plot(var, abuns, 'o-', color=facecolor)
    
    plto.set_xlabel(xlabel)
    plto.set_ylabel(ylabel)
    
    if title:
        plto.set_title(title)

    if bool(xrange) != bool(yrange):
        print( "Both xrange and yrange keywords has to be set!" )
    if xrange and yrange:
        ranges = [xrange[0],xrange[1],yrange[0],yrange[1]]
        plto.axis(ranges)

def plot_abuns_violin(plto, abuns=None, var=None, database=None, 
                      species='CO',typ='dens', xrange=None, yrange=None, 
                      xlog=True, xlabel=None, 
                      ylabel=None, 
                      showmeans=False, showextrema=True, showmedians=True, 
                      points=100, widths=0.3, facecolor = 'default', 
                      edgecolor = 'default', linecolor = 'default',
                      legloc = (0.8,0.9), title=None):
    '''
    Create violin plot for physical condition - abundance distribution.
    
    :param plto: matplotlib axis object to plot into
    :param abuns: array containing the abundance data
    :param var: array containing the independent variable (usually density).
    :param database: instead of abuns and var, give a database (from extract_histo_db)
    :param species: name of specias to plot from database
    :param typ: options: [dens / temp / avs]
    '''
    if abuns == None or var == None:
        if database == None:
            raise ValueError("abuns & var or database must be given!")
        else:
            try:
                sindex = database['species'].index(species)
            except:
                raise ValueError("abuns & var not set, \
                                 {} not found in database".format(species))
            abuns = database['abuns_{}'.format(typ)][sindex]
            exec("{:s} = database[\'{:s}\']".format("var", typ))
    # Remove zero elements because they lead to plotting issues

    xlab = ''
    if xlog:
        var = np.log10(var)
        xlab = '$log\,$'
    
    if not xlabel:
        if typ == 'dens':
            xlabel = 'n [cm$^{-3}$]'
        elif typ == 'temp':
            xlabel = 'T$_{g}$ [K]'
        elif typ == 'avs':
            xlabel = 'A$_{v}$ [mag]'
        else:
            xlabel = 'unknown'
        xlabel = "{}{}".format(xlab,xlabel)
        
    if not ylabel:
        ylabel = '$log$ Fractional abundace'
    
    # Create mask to avoid crash due to empty abuns list element
    mask = []
    for i in range(len(var)):
        if len(abuns[i]) >= 1:
            mask.append(1)
        else:
            mask.append(0)
    abuns = list_select(abuns,mask)
    var   = list_select(var,mask)
    
    violin_parts = plto.violinplot(abuns, var, points=points, widths=widths, showmeans=showmeans,
                      showextrema=showextrema, showmedians=showmedians)
    plto.grid(True)
    plto.annotate(species,legloc,xycoords="axes fraction",
                        bbox={'facecolor':'white', 'alpha':0.9, 'pad':5},
                        fontsize="large")
    if facecolor != 'default':
        for pc in violin_parts['bodies']:
            pc.set_facecolor(facecolor)
    if edgecolor != 'default':
        for pc in violin_parts['bodies']:
            pc.set_edgecolor(edgecolor)
    if linecolor != 'default':
        violin_parts['cbars'].set_color(linecolor)
        if showextrema:
            violin_parts['cmins'].set_color(linecolor)
            violin_parts['cmaxes'].set_color(linecolor)
        if showmedians:
            violin_parts['cmedians'].set_color(linecolor)
        if showmeans:
            violin_parts['cmeans'].set_color(linecolor)

    plto.set_xlabel(xlabel)
    plto.set_ylabel(ylabel)
    
    if title:
        plto.set_title(title)

    if bool(xrange) != bool(yrange):
        print( "Both xrange and yrange keywords has to be set!" )
    if xrange and yrange:
        ranges = [xrange[0],xrange[1],yrange[0],yrange[1]]
        plto.axis(ranges)

def check_instab(Cpabuns, time=None, crit=20.0, plot=False):
    '''
    Computes the relative difference of fractional abundance (given 
    in abuns arguments) between time steps. Notifies user if difference
    is larger than the critical value.
    '''
    diff = np.zeros(len(Cpabuns)-1)
    for i in range(len(diff)):
        diff[i] = np.abs(Cpabuns[i+1]-Cpabuns[i]) / Cpabuns[i]
    if plot:
        plt.plot(diff)    
        plt.show()
    return (diff > crit).any(), max(diff)


def create_histo_db(dens, temp, avs, database, tt=2.2e6,
                    species=['CO','C+','C','H','H2','H+','HCO+','ELECTR']):
    '''
    Creates a histogram database of chemical species as function of density,
    temperature and effective Av.
    
    :param dens: array of density bins
    :param temp: array of temperature bins
    :param avs: array of Av bins
    :param database: list of alchemic_sph result data (ChemModl class objects)
    :param tt: the histogram is computed in this time step
    :param species: name list of considered chemical species
    '''

    nn = len(dens)
    nT = len(temp)
    nAv = len(avs)
    ns = len(species)

    dens_pdf = np.zeros(nn)
    temp_pdf = np.zeros(nT)
    av_pdf   = np.zeros(nAv)
    
    abuns_dens = [ [[] for _ in xrange(nn)] for _ in xrange(ns)]
    abuns_temp = [ [[] for _ in xrange(nT)] for _ in xrange(ns)]
    abuns_avs  = [ [[] for _ in xrange(nAv)] for _ in xrange(ns)]
    
    ID_dens = [ [[] for _ in xrange(nn)] for _ in xrange(ns)]
    ID_temp = [ [[] for _ in xrange(nT)] for _ in xrange(ns)]
    ID_avs  = [ [[] for _ in xrange(nAv)] for _ in xrange(ns)]
    
    med_dens =  [ [ np.nan for _ in xrange(nn)] for _ in xrange(ns)]
    med_temp =  [ [ np.nan for _ in xrange(nT)] for _ in xrange(ns)]
    med_avs  =  [ [ np.nan for _ in xrange(nAv)] for _ in xrange(ns)]
    
    tgas_dens  = [ [] for _ in xrange(nn) ]
    tdust_dens = [ [] for _ in xrange(nn) ]
    av_dens    = [ [] for _ in xrange(nn) ]

    for ChMod in database:
    
        it = find_nearest_index(ChMod.time, tt)
    
        ibind = find_nearest_index(dens, ChMod.gdens[it])
        ibinAv = find_nearest_index(avs, ChMod.AvIS[it])
        ibinT = find_nearest_index(temp, ChMod.Tg[it])

        dens_pdf[ibind] = dens_pdf[ibind] + 1
        temp_pdf[ibinT] = temp_pdf[ibinT] + 1
        av_pdf[ibinAv]  = av_pdf[ibinAv] + 1
        
        
        for i in range(ns):
            iSpec = ChMod.spec.index( species[i] )
            
            logabuns = np.log10(ChMod.abuns[iSpec,it])
            
            abuns_dens[i][ibind].append( logabuns ) 
            abuns_temp[i][ibinT].append( logabuns ) 
            abuns_avs[i][ibinAv].append( logabuns ) 
            
            ID_dens[i][ibind].append( ChMod.ID )
            ID_temp[i][ibind].append( ChMod.ID ) 
            ID_avs[i][ibind].append( ChMod.ID ) 
    
        tgas_dens[ibind].append( ChMod.Tg[it] )
        tdust_dens[ibind].append( ChMod.Td[it] )
        av_dens[ibind].append( ChMod.AvIS[it] )
    
    for i in range(ns):

        for j in range(nn):
            if len(abuns_dens[i][j][:]) > 0:
                med_dens[i][j] = np.median( abuns_dens[i][j][:] )
        for j in range(nT):
            if len(abuns_temp[i][j][:]) > 0:
                med_temp[i][j] = np.median( abuns_temp[i][j][:] )
        for j in range(nAv):
            if len(abuns_avs[i][j][:]) > 0:
                med_avs[i][j] = np.median( abuns_avs[i][j][:] )
    
    results = {}
    
    results['species'] = species
    results["dens_pdf"] = dens_pdf
    results["temp_pdf"] = temp_pdf
    results["av_pdf"] = av_pdf
    results["dens"] = dens
    results["temp"] = temp
    results["avs"] = avs
    
    results["abuns_dens"] = abuns_dens
    results["abuns_temp"] = abuns_temp
    results["abuns_avs"] = abuns_avs

    results["ID_dens"] = ID_dens
    results["ID_temp"] = ID_temp
    results["ID_avs"] = ID_avs
    
    results["med_dens"] = med_dens
    results["med_temp"] = med_temp
    results["med_avs"] = med_avs
    
    results["tgas_dens"] = tgas_dens
    results["tdust_dens"] = tdust_dens
    results["av_dens"] = av_dens
    
    return results

def extract_histo_db(dens, temp, avs, folder=None, basename=None, tt=2.2e6, 
                     species=None, verbose=False, pickle_file=None):
    '''
    This is a wrapper around create_histo_db() which finds the alchemic_sph output 
    files and reads in the data in appropriate format and calls the main 
    histogram module.
    
    :param dens: array of density bins
    :param temp: array of temperature bins
    :param avs: array of Av bins
    :param folder: folder name containing alchemic_sph binary outputs
    :param tt: the histogram is computed in this time step
    :param species: name list of considered chemical species
    :param verbose: switch on detailed output
    '''
    if not species:
        species = ['H2','H+','C','C+','CO','HCO+','HE','HE+','ELECTR']
    if not folder:
        folder = '.'
    if not basename:
        basename = '*'
    
    list1 = glob.glob('{}/{}_?.bout'.format(folder,basename))
    list1.sort()
    list2 = glob.glob('{}/{}_??.bout'.format(folder,basename))
    list2.sort()
    flist = list1 + list2
    # species list
    specfile = glob.glob('{}/{}_species_*.dat'.format(folder,basename))

    models = []
    for f in flist:
        if verbose:
            print( "Reading {}".format(f) )
        tmp = model.read_alchemic(filename=f, specfile=specfile[0], verbose=verbose)
        models = models + tmp

    # Now convert it to a database
    histo = create_histo_db(dens, temp, avs, models, species=species)
    
    if pickle_file:
        if type(pickle_file) != str:
            print( flist[0] )
            pickle_file = "".join( flist[0].split('.').split('_')[:-1], '_' )
            pickle_file = '{}_histo.p'.format(pickle_file)
        pickle.dump( histo, open( pickle_file, "wb" ) )
    
    return histo


def plot_histo_diag(database,species=None,saveFig=False,legloc=(0.7,0.2),
                    title=None,mrow=10):
    '''
    Diagnostic plot of selected or all species in the database which show the 
    density, temperature and Av dependence of fractional abundances.
    
    :param mrow: maximum number of rows, if larger start new file / plot
    '''
    # parameters
    sw   = 3.5             # width of single panel
    sh   = 3.5             # hight of single panel
    col  = 3               # nr. columns
    
    if not species:
        species = database['species']
        
    ns = len(species)
    nr_fig = (ns / mrow) + (ns%mrow) / max([(ns%mrow),1])
    row = []
    
    for i in range(nr_fig):
        if ( (i+1) < nr_fig ) or ( ns%mrow == 0 ):
            row.append(mrow)
        else:
            row.append(ns%mrow)
    
    for r in range(nr_fig):
        
        fig, axes = plt.subplots(nrows=row[r], ncols=col, figsize=(sw*col,sh*row[r]) )
        
        for i in range(row[r]):
            ispec = (r*mrow) + i
            
            if (i+1) != row[r]:
                xlabel = ' '
            else:
                xlabel = None
            
            plot_abuns_violin(axes[i,0], database=database, typ='dens',
                              species=species[ispec], xlog=True, legloc=legloc,
                              xlabel=xlabel)
            plot_abuns_violin(axes[i,1], database=database, typ='temp',
                              species=species[ispec], xlog=True, legloc=legloc,
                              facecolor='green',edgecolor='blue',linecolor='black',
                              xlabel=xlabel,ylabel=' ')
            plot_abuns_violin(axes[i,2], database=database, typ='avs',
                              species=species[ispec], xlog=True, legloc=legloc,
                              facecolor='#2c7bb6',edgecolor='#023858',linecolor='black',
                              xlabel=xlabel,ylabel=' ')
            
        if saveFig:
            if type(saveFig) == str:
                fbase = saveFig
            else:
                fbase = "Fig_diag"
            commons.saveFig("{}_{}".format(fbase,r+1),ext='pdf')
            
def plot_histo_comp(dblist,species=None,saveFig=False,typ='dens',
                    legloc=(0.7,0.2),title=None,mrow=10):
    '''
    Compares abundance distribution in different models as a function of 
    the chosen physical conditions (dens, temp, avs). Currently it supports 
    the comparison of 4 models.
    
    :param dblist: list of model data ( output from extract_histo_db() )
    :param species: list of species to be plotted. If not set then all.
    :param saveFig: (logical or string) if set saves figure to pdf file.
    :param typ: quantity plotted on x-axis (options: 'dens', 'temp', 'avs')
    :param mrow: maximum number of rows, if larger start new file / plot
      
    :Example:
    
    a = pickle.load( open('a_histo.p','rb') )
    b = pickle.load( open('b_histo.p','rb'))
    c = pickle.load( open('c_histo.p','rb'))
    spec = ['CO','C+','C','HCO+']
    plot_histo_comp([a, b, c],species=spec)
    
    '''
    # parameters
    sw   = 3.5             # width of single panel
    sh   = 3.5             # hight of single panel
    
    c_edge = ['gray', 'blue', '#023858', '#023858']
    c_face = ['orange', 'green', '#2c7bb6', 'gray']
    c_line = ['black', 'black', 'black', 'black']
    
    nr_data = len(dblist)
    
    if not species:
        species = dblist[0]['species']
        
    if title:
        if len(title) == nr_data:
            pass
        else:
            raise ValueError('The number of models and titles do not match.')
        
    ns = len(species)
    col = nr_data
    nr_fig = (ns / mrow) + (ns%mrow) / max([(ns%mrow),1])
    row = []
    
    for i in range(nr_fig):
        if ( (i+1) < nr_fig ) or ( ns%mrow == 0 ):
            row.append(mrow)
        else:
            row.append(ns%mrow)
    
    for r in range(nr_fig):
        
        fig, axes = plt.subplots(nrows=row[r], ncols=col, figsize=(sw*col,sh*row[r]) )
        
        for i in range(row[r]):
            ispec = (r*mrow) + i
            
            for j in range(nr_data):
                
                facecolor = c_face[j]
                edgecolor = c_edge[j]
                linecolor = c_line[j]
                
                if j > 0:
                    ylabel = ' '
                else:
                    ylabel = None
                    
                if (i+1) != row[r]:
                    xlabel = ' '
                else:
                    xlabel = None
                    
                if i == 0 and title:
                    ctitle = title[j]
                else:
                    ctitle = None
                
                plot_abuns_violin(axes[i,j], database=dblist[j], typ=typ,
                                  species=species[ispec], xlog=True, legloc=legloc,
                                  facecolor=facecolor, edgecolor=edgecolor, 
                                  linecolor=linecolor, xlabel=xlabel, ylabel=ylabel,
                                  title=ctitle)
                if j == 0:
                    com_ymin, com_ymax = axes[i,j].get_ybound()
                else:
                    for k in range(j):
                        plot_abuns_med(axes[i,j], database=dblist[k], typ=typ,
                                       species=species[ispec], xlog=True, facecolor=c_face[k],
                                       xlabel=xlabel, ylabel=ylabel)
                    
                    ymin, ymax = axes[i,j].get_ybound()
                    if ymin < com_ymin:
                        com_ymin = ymin
                    if ymax > com_ymax:
                        com_ymax = ymax
                    for ax in axes[i,:]:
                        ax.set_ybound( (com_ymin,com_ymax) )
            
        if saveFig:
            if type(saveFig) == str:
                fbase = saveFig
            else:
                fbase = "Fig_comp"
            commons.saveFig("{}_{}".format(fbase,r+1),ext='pdf')
            
