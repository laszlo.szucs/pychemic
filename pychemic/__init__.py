"""
pychemic: ALCHEMIC_SPH / MPChem Python module
Laszlo Szucs, Garching, 2018-2019
"""

from . import commons
from . import model
from . import gadget
from . import splash
from . import sph_violinp
from . import diag
#from pychemic import network

#from pychemic.model import read_alchemic
#from pychemic.model import read_sipila2015
#from pychemic.model import read_semenov2010
#from pychemic.model import read_nahoon

#from pychemic.model import write_alchemic

#from pychemic.network import read_network

#from pychemic.network import add_grain
#from pychemic.network import read_kida_network
#from pychemic.gadget import read_gadget
#from pychemic.gadget import write_asplash
#from pychemic.gadget import read_history_dump
#from pychemic.model import plot_rad_dist
#from pychemic.model import write_nautilus
#from pychemic.model import read_nautilus
#from pychemic.sph_violinp import create_histo_db
#from pychemic.sph_violinp import extract_histo_db
#from pychemic.sph_violinp import plot_abuns_violin
#from pychemic.sph_violinp import plot_histo_diag
#from pychemic.sph_violinp import plot_histo_comp
#from pychemic.diag import plot_map_diag
#from pychemic.diag import plot_column_map
#from pychemic.diag import alchemic_to_asplash


__version__ = "0.1"
__author__ = "Laszlo Szucs"
__all__ = ["commons","model","gadget","splash","sph_violinp",
           "diag"]

# Check if pygraphviz is installed
try:
    import pygraphviz
    __all__.append("draw_chem_graph")
    from . import draw_chem_graph
except:
    pass
