""" Module for reading and analysing ALCHEMIC model results.

Examples:

1) Reading and plotting the results of the *Test_Semenov2010* test calculations.
The PyChem package is imported as ntw, then the model called TMC is read, first
from the ALCHEMIC output file *S2010_00000001.idl*, then from the file from the
Semenov (2010) paper, for comparision.::

    import pychemic as ntw
    tmc1 = ntw.read_alchemic("results/S2010_00000001.idl")
    s2010_tmc1 = ntw.read_semenov2010('TMC1/TMC1.semenov2010.idl')
    carbon_species = ["C+", "C", "CO", "gCO", "H2CO"]
    yrange = [1e-13,1e-3]
    tmc1.PlotAbuns(carbon_species, xlog=True, yrange=yrange)
    s2010_tmc1.PlotAbuns(carbon_species, oplot=True, marker='o', markevery=2,
            xlog=1, yrange=yrange)

.. image:: _resources/abuns_example_1.png
"""
from __future__ import absolute_import
from __future__ import print_function

import os
import glob
import sys
import warnings

from cycler import cycler
import numpy as np
import matplotlib.pyplot as plt
import h5py

from . import commons

class ChemModl:
    """ Class containg the chemical model data, parameters and metadata.

    Define the network structure, we need reactants, products, 3 coefficients
    and some space for potential comments.
    """
    def __init__(self, **kwargs):
        # Physical conditions as function of time
        self.x = kwargs.get('x',None)
        self.y = kwargs.get('y',None)
        self.z = kwargs.get('z',None)
        time = kwargs.get('time',None)
        self.time = time
        try:
            self.nt = len(time)
            self.tindex = np.arange(len(time))
        except:
            self.nt = 1
            self.tindex = 1
        self.rho = kwargs.get('rho',None)
        self.Tg = kwargs.get('Tg',None)
        self.Td = kwargs.get('Td',None)
        self.G0 = kwargs.get('G0',None)
        self.AvIS = kwargs.get('AvIS',None)
        self.AvSt = kwargs.get('AvSt',None)
        self.AvArr = kwargs.get('AvArr',None)
        self.fH2_IS = kwargs.get('fH2_IS',None)
        self.fH2_St = kwargs.get('fH2_St',None)
        self.fCO_IS = kwargs.get('fCO_IS',None)
        self.fCO_St = kwargs.get('fCO_St',None)
        self.ZetaCR = kwargs.get('ZetaCR',None)
        self.ZetaX = kwargs.get('ZetaX',None)
        self.gdens = kwargs.get('gdens',None)
        self.amu = kwargs.get('amu',None)
        self.H = kwargs.get('H',None)  # Smoothing length
        self.AvLocal = kwargs.get('AvLocal',None)
        # Abundances as function of time for each species
        spec = kwargs.get('spec',None)
        if spec != None:
            self.ns = len(spec)
            self.spec = spec
            self.sindex = np.arange(len(spec))
        else:
            self.ns = None
            self.spec = None
            self.sindex = None
        self.abuns = kwargs.get('abuns',None)
        # Reactions and their rates as function of time
        self.InitSpec = kwargs.get('InitSpec',None)
        self.InitAbuns = kwargs.get('InitAbuns',None)
        nre = len(kwargs.get('r1',''))
        self.rindex = np.arange(nre)
        self.nre = nre
        self.r1 = kwargs.get('r1',None)
        self.r2 = kwargs.get('r2',None)
        self.p1 = kwargs.get('p1',None)
        self.p2 = kwargs.get('p2',None)
        self.p3 = kwargs.get('p3',None)
        self.p4 = kwargs.get('p4',None)
        self.p5 = kwargs.get('p5',None)
        self.ak = kwargs.get('ak',None)
        # Metadata
        self.specfile = kwargs.get('specfile',None)
        self.ratefile = kwargs.get('ratefile',None)
        self.ID = kwargs.get('ID',None)
        self.rhod = kwargs.get('rhod',None)
        self.mdmg = kwargs.get('mdmg',None)
        self.albedo_UV = kwargs.get('albedo_UV',None)
        # Store the production and destruction rates for all species:
        
        #
            #
            #for ics in range(self.ns):
                #self.pdrates.append(ReactionRates(self, self.spec[ics]))
        #else:
        self.pdrates = None

    def __len__(self):
        return 1

    def GetRates(self):
        
        self.pdrates = []
        for ics in range(self.ns):
            self.pdrates.append(ReactionRates(self, self.spec[ics]))
            

    def PlotPhys(self, xlog=True, show=False, savefig=False, figext='eps',
                asprat=(7,8),title=False,fontsize="medium",extlegend=False,
                xlabel=False):
        """ Function for plotting the physical conditions as a function of time.

        The procedure also works when constant or single physical conditions
        are given (e.g. as when the Semenov (2010) or Sipila (2015) results
        are plotted).

        :param xlog: set the x axis logarithmically? (default True)
        :type xlog: boolean

        """
        plt.figure(figsize=asprat, dpi=120)
        mp = 1.6726e-24        # Mass of proton  [g]
        if xlog:
            time = self.time
            xscale = 'log'
            if not xlabel:
                xlabel = "t [yr]"
        else:
            time = self.time / 1e6
            xscale = 'linear'
            if not xlabel:
                xlabel = "t [Myr]"
        # Density
        plt.subplot(311)
        if not title:
            title = "Physical conditions with time"
        plt.title(title)
        try:
            datY = self.rho / (self.amu*mp)
        except:
            datY = self.rho / (1.4*mp)
        if ( type(datY) == float ):
            datY = np.zeros(len(time)) + datY
        plt.plot(time, datY, "g-", linewidth=2.0)
        plt.yscale('log')
        plt.xscale(xscale)
        plt.ylabel("$n\,\,[\mathrm{cm}^{-3}]$")
        # Set font sizes
        ax = plt.gca()
        for item in ([ax.xaxis.label, ax.yaxis.label] +
            ax.get_xticklabels() + ax.get_yticklabels()):
            if type(fontsize) != str:
                item.set_fontsize(fontsize-2)
            else:
                item.set_fontsize(fontsize)
        # Set plotting range
        if (datY.min() != datY.max()):
            plt.axis([time.min(), time.max(), datY.min(), datY.max()])
        plt.grid(True)
        if extlegend:
            plt.annotate(extlegend,(0.06,0.8),xycoords="axes fraction",
                        bbox={'facecolor':'white', 'alpha':0.9, 'pad':5},
                        fontsize=fontsize)
        # Temperatures
        plt.subplot(312)
        datY0 = self.Tg
        datY1 = self.Td
        if ( type(datY0) == float ):
            datY0 = np.zeros(len(time)) + datY0
            datY1 = np.zeros(len(time)) + datY1
        plt.plot(time,datY0,"r-", label="$T_\mathrm{gas}$", linewidth=2.0)
        plt.plot(time,datY1,"b-", label="$T_\mathrm{dust}$", linewidth=2.0)
        plt.legend(loc=0,fontsize=fontsize)
        plt.xscale(xscale)
        plt.ylabel("$T\,\,[\mathrm{K}]$")
        # Set font sizes
        ax = plt.gca()
        for item in ([ax.xaxis.label, ax.yaxis.label] +
            ax.get_xticklabels() + ax.get_yticklabels()):
            if type(fontsize) != str:
                item.set_fontsize(fontsize-2)
            else:
                item.set_fontsize(fontsize)
        # Set plotting range
        if (datY.min() != datY.max()):
           plt.axis([time.min(), time.max(), 0,
                    max([datY0.max(), datY1.max()])])
        plt.grid(True)
        # Mean Av
        plt.subplot(313)
        datY = self.AvIS
        if ( type(datY) == float ):
            datY = np.zeros(len(time)) + datY
        if np.any(self.AvArr):
            Aveff = np.zeros(len(time))
            for jj in range(len(time)):
                Aveff[jj] = commons.AvgAv(self.AvArr[jj,:],gamma=-2.5)
            datY = Aveff
            plt.fill_between(time, np.min(self.AvArr, axis=1),
                np.max(self.AvArr, axis=1),facecolor='#e5f5e0', interpolate=True)
            plt.fill_between(time, np.percentile(self.AvArr, 25., axis=1),
                np.percentile(self.AvArr, 75., axis=1),facecolor='#31a354', interpolate=True)
            Ymin = 0.0
            Ymax = np.max(np.percentile(self.AvArr, 75., axis=1))
        else:
            Ymin = 0.0
            Ymax = np.max(datY)
        plt.plot(time,datY, "y-", label="$A_\mathrm{V,eff} = -0.4\,\ln{( 1/N_\mathrm{pix}\,\sum{\exp{(-2.5 A_\mathrm{V}[i]}) })}$", linewidth=2.0)
        plt.xscale(xscale)
        plt.ylabel("$A_\mathrm{V} \,[\mathrm{mag}]$")
        plt.xlabel(xlabel)
        plt.legend(loc=2,fontsize=fontsize)
        # Set font sizes
        ax = plt.gca()
        for item in ([ax.xaxis.label, ax.yaxis.label] +
            ax.get_xticklabels() + ax.get_yticklabels()):
            if type(fontsize) != str:
                item.set_fontsize(fontsize-2)
            else:
                item.set_fontsize(fontsize)
        # Set plotting range
        if (datY.min() != datY.max()):
            plt.axis([time.min(), time.max(), Ymin, Ymax])
        plt.grid(True)
        plt.tight_layout()
        # Finish
        if show:
            plt.show()
        if savefig != False:
            if type(savefig) != str:
                savefig = 'abuns_example'
            commons.saveFig(savefig,ext=figext)

    def PlotAbuns(self, species=["H2","CO"], title=False, xrange=None,
                yrange=[1e-10,1e0], xlog=True, marker='', markevery=1,
                show=False, oplot=False, legcol=1, legtitle=False, asprat=(7,5),
                extlegend=False, extlegloc=(0.06,0.09), fontsize=17,
                savefig=False, figext='eps',linestyle='solid'):
        """ Plots the abundances of selected species as a function of time.

        When dealing with networks containing multiple states of the same
        species (i.e. ortho/para or deuterated networks), then multiple
        elements can be plotted combined together.

        E.g. for plotting o-H2 and p-H2 together add the "oH2_+_pH2" string
        to the species list.

        :param species: list of species to be plotted default (H2, CO)
        :type species: str list
        :param title: title for the figure (default None)
        :type title: str
        :param xrange: range to be shown on the x axis (default [min(time),max(time)])
        :type xrange: float list
        :param yrange: range to be shown on the y axis (default [1e-10, 1])
        :type yrange: float list
        :param xlog: set the x axis logarithmically? (default True)
        :type xlog: boolean
        :param oplot: overplot on a previously created figure? (default False)
        :type oplot: boolean
        :param marker: symbols to be plotted on top of the lines (default None).
                For details see http://matplotlib.org/api/markers_api.html
        :type marker: str
        :param markevery: show every nth symbol (default 1)
        :type markevery: int
        :param legcol: number of columns in the legend (default 1)
        :type legcol: int
        :param show: plot the result in a new window? (default False)
        :type show: boolean
        :param extlegend: text to be added to the figure (default False)
        :type extlegend: string"""
        mp = 1.6726e-24        # Mass of proton  [g]
        if not oplot:
            plt.figure(figsize=asprat, dpi=120)
            if not linestyle:
                linestyle = 'solid'
        else:
            if not linestyle:
            	linestyle = 'dotted'
        ColourSet = ["b", "g", "r", "c", "m", "y", "k"]
        if len(species) < len(ColourSet):
            ColourSet = ColourSet[0:len(species)]
        plt.rc('axes', prop_cycle=(cycler('color', ColourSet) ))
        if not title:
            title = "Abundances of " + ", ".join(species)
        plt.title(title)
        # Search the present species
        if xlog:
            time = self.time
            plt.xscale('log')
            plt.xlabel("t [yr]")
        else:
            time = self.time/1e6
            plt.xlabel("t [Myr]")

        if xrange:
            xr = xrange
        else:
            xr = [max([time.min(),0.1]), time.max()]
        plt.axis([xr[0], xr[1], yrange[0], yrange[1]])

        for i in range(len(species)):
            try:
                if "_+_" in species[i]:
                    Specs = species[i].split("_+_")
                    Abun2Plot = np.zeros(len(self.abuns[0,:]))
                    for bb in range(len(Specs)):
                        ind1 = self.spec.index(Specs[bb])
                        Abun2Plot = Abun2Plot + self.abuns[ind1,:]
                else:
                    ind1 = self.spec.index(species[i])
                    Abun2Plot = self.abuns[ind1,:]
                plt.plot(time, Abun2Plot, linewidth=2.0, label=species[i],
                        marker=marker, markevery=markevery,linestyle=linestyle)
            except:
                print("Species {} not found in model!".format(species[i]))

        plt.yscale('log')
        plt.ylabel("Fractional abundance")
        leg = plt.legend(bbox_to_anchor=(1.03, 1), loc=2, borderaxespad=0.,
                    ncol=legcol,prop={'size':fontsize-5})
        leg.set_title(legtitle,prop={'size':fontsize-8})
        if extlegend:
            plt.annotate(extlegend,extlegloc,xycoords="axes fraction",
                        bbox={'facecolor':'white', 'alpha':0.94, 'pad':5},
                        fontsize=fontsize)
        # Set font sizes
        ax = plt.gca()
        for item in ([ax.xaxis.label, ax.yaxis.label] +
            ax.get_xticklabels() + ax.get_yticklabels()):
            if type(fontsize) != str:
                item.set_fontsize(fontsize-2)
            else:
                item.set_fontsize(fontsize)
        plt.grid(True)
        plt.tight_layout()
        #Finish
        if show:
            plt.show()
        if savefig != False:
            if type(savefig) != str:
                savefig = 'abuns_example'
            commons.saveFig(savefig,ext=figext)


    def Reduce(self,species=['H2','CO','H2O','CH3OH','NH3','N2H+','HCO+',
                'H2CO','H3+']):
        """ Reuduces the ChemModl object content to the species of interest
        to conserve memory storage.
        """
        warnings.warn("WARNING: this code is not ready yet!")
        # find selected species
        i_found = []
        s_found = []
        for s in species:
            try:
                i_found.append(self.spec.index(s))
                s_found.append(s)
            except:
                print( "{} not found in model!".format(s) )
        self.spec = s_found
        self.abuns = self.abuns[i_found,:]

    def PlotRateCoefs(self, topn=40, oplot=False, r1crit=False, r2crit=False,
                pcrit=False, ReactList=None, marker="o", extlegend=False,
                savefig=False, figext='eps', show=False):
        """ Plots the reaction rate coefficients on an ordered scatter plot.
        The list of reaction can be created automatically according criterion
        set by r1crit and r2crit keywords. A reaction set can also be chosen
        by setting the ReactList keyword. For a comparision between models
        use the oplot and marker options.

        The method returns a string list containing the names/id of the plotted
        reactions.

        :param topn: number of reactions to be plotted, sorted by rate coeff.
        :type topn: int
        :param oplot: overplot to existing graphic (default False)
        :type oplot: boolean
        :param r1crit: apply a selection criteria on the first reactnat (def. False)
        :type r1crit: boolean or string
        :param r2crit: apply a selection criteria on the second reactnat (def. False)
        :type r2crit: boolean or string
        :param ReactList: List of reactions to be plotted (e.g. "CO + PHOTON -> C")
        :type ReactList: string list
        :param marker: symbol to be plotted (default "o")
        :type marker: string
        :param extlegend: text to be added to the figure (default False)
        :type extlegend: string
        """
        if not oplot:
            ysize = 4 * topn // 10
            plt.figure(figsize=(7,ysize), dpi=120)
        if ReactList is None:
            akMaxs = np.zeros(self.nre)
            akDisp = np.zeros(self.nre)
            ReactString = []
            counter = 0
            for i in range(self.nre):
                if ( (r2crit == False or self.r2[i] == r2crit) and (r1crit == False or self.r1[i] == r1crit) and
                (pcrit == False or pcrit in [self.p1[i],self.p2[i],self.p3[i],self.p4[i]]) ):
                    try:
                        akMaxs[counter] = self.ak[i,:].max()
                        akDisp[counter] = self.ak[i,:].std()
                    except:
                        akMaxs[counter] = self.ak[i]
                        akDisp[counter] = None
                    counter = counter + 1
                    ReactString.append(self.r1[i] + " + " + self.r2[i] + " -> " +
                    self.p1[i] + " + " + self.p2[i] + " + " + self.p3[i])
            ReactString = np.array(ReactString)

            akMaxs = akMaxs[0:(len(ReactString))]
            akDisp = akDisp[0:(len(ReactString))]
            sindex = np.argsort(akMaxs)
            sindex = sindex[::-1]

            akMaxs = akMaxs[sindex]
            akDisp = akDisp[sindex]
            ReactString = ReactString[sindex]

            akMaxs = akMaxs[0:topn]
            akDisp = akDisp[0:topn]
            ReactString = ReactString[0:(topn)]
            ind = np.arange(min([topn,counter]))
        else:
            nre = len(ReactList)
            akMaxs = np.zeros(nre)
            akDisp = np.zeros(nre)
            ReactString = []
            ind = np.arange(nre)
            counter = 0
            for reaction in ReactList:
                Found = False
                for i in range(self.nre):
                    TMPReactString = self.r1[i] + " + " + self.r2[i] + " -> " + self.p1[i] + " + " + self.p2[i] + " + " + self.p3[i]
                    if TMPReactString == reaction:
                        try:
                            akMaxs[counter] = self.ak[i,:].max()
                            akDisp[counter] = self.ak[i,:].std()
                        except:
                            akMaxs[counter] = self.ak[i]
                            akDisp[counter] = None
                        ReactString.append(TMPReactString)
                        Found = True
                        continue
                if not Found:
                    print( "{} is not found!".format(reaction) )
                    ReactString.append(" ")
                    akMaxs[counter] = None
                    akDisp[counter] = None
                counter = counter + 1

        # Here comes the plotting part:
        plt.xscale('log')
        plt.plot(akMaxs, ind, marker=marker)
        plt.errorbar(akMaxs, ind, xerr=akDisp)
        plt.grid(True)
        
        plt.yticks(ind, ReactString)
        akmin = min(akMaxs[np.nonzero(akMaxs)])
        xr = [10.**(int(np.log10(akmin))-1), 10.**(np.log10(max(akMaxs))+1)]
        plt.axis([xr[0], xr[1], -0.5, max(ind)+0.5])
        # add annotation if given
        if extlegend != False:
            plt.annotate(extlegend,(0.1,0.1),xycoords="axes fraction",
                        bbox={'facecolor':'white', 'alpha':0.9, 'pad':5},
                        fontsize="large")
        # Save figure if needed
        if savefig != False:
            if type(savefig) != str:
                savefig = 'rate-coeff_example'
            commons.saveFig(savefig,ext=figext)
        if show:
            plt.show()
        # Return the reaction list
        ReactList = []
        for string in ReactString:
            ReactList.append(string)
        plt.xlabel('Reaction rate coefficient')
        plt.tight_layout()
        return ReactList

    def PlotRates(self, species="H2", title='', xrange=None, yrange=None,
                xlog=True, markevery=4, show=False, legcol=1, ntop=5,
                savefig=False,figext='eps'):
        """ Plots the reaction rates for selected species as a function of time.

        :param species: reactions of species are plotted (default H2)
        :type species: str
        :param ntop: number of important reactions to be plotted
        :type ntop: int
        :param title: title for the figure (default None)
        :type title: str
        :param xrange: range to be shown on the x axis (default None)
        :type xrange: float list
        :param yrange: range to be shown on the y axis (default None)
        :type yrange: float list
        :param xlog: set the x axis logarithmically? (default True)
        :type xlog: boolean
        :param markevery: show every nth symbol (default 1)
        :type markevery: int
        :param legcol: number of columns in the legend (default 1)
        :type legcol: int
        :param show: plot the result in a new window? (default False)
        :type show: boolean
        """
        if (type(self.pdrates) == type(None)):
            print ('ERROR (PlotRates): run/call GetRates() first!')
            return -1
        
        # Find the ReactionRates object in the pdrates list:
        SpecIndex = self.spec.index(species)
        # set axis and defaults
        if xlog:
            time = self.time
            xmin = 0.1
            S_xlabel = "t [yr]"
            S_xscale = "log"
        else:
            time = self.time/1e6
            xmin = 0.0
            S_xlabel = "t [Myr]"
            S_xscale = "linear"
        mp = 1.6726e-24        # Mass of proton  [g]
        # Colours and lines
        ColourSet = ["b", "g", "r", "c", "m", "y", "k", "b", "g", "r", "c",
                     "m", "y", "k", "b", "g", "r", "c", "m", "y", "k"]
        LineSet   = ['--', '--', '--', '--', '--', '--', '--', '-.', '-.',
                     '-.', '-.', '-.', '-.', '-.', ':', ':', ':', ':', ':',
                     ':', ':']
        # figure size
        fig = plt.figure(figsize=(8,15), dpi=120)

        # Total rates
        ax1 = plt.subplot(311)
        plt.title("Production and destruction rates", loc='left')
        plt.xlabel(S_xlabel, size="medium")
        plt.ylabel("rate [s$^{-1}$]", size="medium")
        plt.xscale(S_xscale)
        plt.yscale('log')
        # Data to be plotted:
        datY0 = self.pdrates[SpecIndex].ProdRateTot
        datY1 = self.pdrates[SpecIndex].DestRateTot
        # Plot:
        ax1.plot(time, datY0, "b-", linewidth=2.0, label="Total production")
        ax1.plot(time, datY1, "r-", linewidth=2.0, label="Total destruction")
        # Set the plotting range:
        if yrange == None:
            yr = [min([min(datY0), min(datY1)]), max([max(datY0), max(datY1)])]
            if yr[0] < yr[1]*1e-10:
                yr[0] = yr[1]*1e-10
        else:
            yr = yrange
        if xrange == None:
            xr = [xmin, time.max()]
        else:
            xr = xrange
        plt.axis([xr[0], xr[1], yr[0], yr[1]])
        # Add the legend
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.,
                    ncol=legcol)
        plt.grid(True)

        # Production
        ax2 = plt.subplot(312)
        plt.title("Production rates", loc='left')
        plt.xlabel(S_xlabel, size="medium")
        plt.ylabel("rate [s$^{-1}$]", size="medium")
        plt.xscale(S_xscale)
        plt.yscale('log')
        # Plot the data:
        ax2.plot(time, datY0, "b-",linewidth=2.0, label="Total rate")
        # How many reactions to show?
        ntop_p = min([ntop, self.pdrates[SpecIndex].nProd])
        # Set line properties
        plt.rc('axes', prop_cycle=(cycler('color', ColourSet)
                + cycler('linestyle', LineSet) ) )
        # Cycle around the reactions:
        pmin, pmax = 1., 0.
        for ii in range(ntop_p):
            datY = self.pdrates[SpecIndex].ProdRate[ii,:]
            ReactionLabel = (self.pdrates[SpecIndex].ProdR1[ii].replace("+","$^{+}$") +
                    ' + ' + self.pdrates[SpecIndex].ProdR2[ii].replace("+","$^{+}$") +
                    ' -> ' + self.pdrates[SpecIndex].ProdP1[ii].replace("+","$^{+}$"))
            if self.pdrates[SpecIndex].ProdP2[ii] != '':
                ReactionLabel = (ReactionLabel +
                ' + ' + self.pdrates[SpecIndex].ProdP2[ii].replace("+","$^{+}$"))
            if self.pdrates[SpecIndex].ProdP3[ii] != '':
                ReactionLabel = (ReactionLabel +
                ' + ' + self.pdrates[SpecIndex].ProdP3[ii].replace("+","$^{+}$"))
            if self.pdrates[SpecIndex].ProdP4[ii] != '':
                ReactionLabel = (ReactionLabel +
                ' + ' + self.pdrates[SpecIndex].ProdP4[ii].replace("+","$^{+}$"))
            if self.pdrates[SpecIndex].ProdP5[ii] != '':
                ReactionLabel = (ReactionLabel +
                ' + ' + self.pdrates[SpecIndex].ProdP5[ii].replace("+","$^{+}$"))
            ReactionLabel.replace("PHOTON","$\gamma$")
            ReactionLabel.replace("CRP","$\Zeta_{CR}$")
            ReactionLabel.replace("CRPHOT","$\gamma_{CR}$")
            ax2.plot(time, datY, 'o-', label=ReactionLabel, linewidth=2.0,
                        markevery=markevery)
            # find the best plotting range
            pmin = min([pmin, min(datY)])
        if not yrange:
            pmax = 10.0**( int(np.log10(max(datY0)) ) )
            expdif = np.log10(pmax) - np.log10(pmin)
            if (abs(expdif) > 8.):
                pmin = 10.0**(np.log10(pmax) - 8)
            yr = [pmin, pmax]
        else:
            yr = yrange
        plt.axis([xr[0], xr[1], yr[0], yr[1]])
        # Add the legend
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.,
                    ncol=legcol)
        plt.grid(True)

        # Destruction
        ax3 = plt.subplot(313)
        plt.title("Destruction rates", loc='left')
        plt.xlabel(S_xlabel,size="medium")
        plt.ylabel("rate [s$^{-1}$]", size="medium")
        plt.xscale(S_xscale)
        plt.yscale('log')
        # Plot the data:
        ax3.plot(time, datY1, "r-", linewidth=2.0, label="Total rate")
        # How many reactions to show?
        ntop_d = min([ntop, self.pdrates[SpecIndex].nDest])
        # Set line properties
        plt.rc('axes', prop_cycle=(cycler('color', ColourSet)
                + cycler('linestyle', LineSet) ) )
        # Cycle around the reactions:
        pmin, pmax = 1., 0.
        for ii in range(ntop_d):
            datY = self.pdrates[SpecIndex].DestRate[ii,:]
            ReactionLabel = (self.pdrates[SpecIndex].DestR1[ii].replace("+","$^{+}$")
                + ' + ' + self.pdrates[SpecIndex].DestR2[ii].replace("+","$^{+}$") +
                ' -> ' + self.pdrates[SpecIndex].DestP1[ii].replace("+","$^{+}$"))
            if self.pdrates[SpecIndex].DestP2[ii] != '':
                ReactionLabel = (ReactionLabel + ' + ' +
                      self.pdrates[SpecIndex].DestP2[ii].replace("+","$^{+}$"))
            if self.pdrates[SpecIndex].DestP3[ii] != '':
                ReactionLabel = (ReactionLabel + ' + ' +
                      self.pdrates[SpecIndex].DestP3[ii].replace("+","$^{+}$"))
            if self.pdrates[SpecIndex].DestP4[ii] != '':
                ReactionLabel = (ReactionLabel + ' + ' +
                      self.pdrates[SpecIndex].DestP4[ii].replace("+","$^{+}$"))
            if self.pdrates[SpecIndex].DestP5[ii] != '':
                ReactionLabel = (ReactionLabel + ' + ' +
                      self.pdrates[SpecIndex].DestP5[ii].replace("+","$^{+}$"))
            ReactionLabel.replace("PHOTON","$\gamma$")
            ReactionLabel.replace("CRP","$\Zeta_{CR}$")
            ReactionLabel.replace("CRPHOT","$\gamma_{CR}$")
            ax3.plot(time, datY, 'o-', label=ReactionLabel, linewidth=2.0,
                    markevery=markevery)
            # find the best plotting range
            pmin = min([pmin, min(datY)])
        if not yrange:
            pmax = 10.0**( int(np.log10(max(datY1)) ) )
            expdif = np.log10(pmax) - np.log10(pmin)
            if (abs(expdif) > 8.):
                pmin = 10.0**(np.log10(pmax) - 8)
            yr = [pmin, pmax]
        else:
            yr = yrange
        plt.axis([xr[0], xr[1], yr[0], yr[1]])
        # Add the legend
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.,
                ncol=legcol)
        plt.grid(True)
        plt.tight_layout()
        # Save the figure?
        if savefig != False:
            if type(savefig) != str:
                savefig = 'rate_example'
            commons.saveFig(savefig,ext=figext)
        # Finish
        if show:
            plt.show()
    def GetAbuns(self, spec=None, time=None, ntime=-1):
        """ Obtains the abundance of the selected species at the selected
        timestep.

        :param species: name of species to be obtained (see ChemModl.spec).
        :type species: str
        :param time reactions of species are plotted (default: None)
        :type time: int
        """
        if spec == None:
            raise ValueError('GetAbuns() takes a species name, non given!')
        nget = None
        if not time:
            tget = self.time(ntime)
        else:
            tget = time
        if (type(spec) == list):
            Ab = []
            for s in spec:
                ind =self.spec.index(s)
                Ab.append(np.interp(tget, self.time, self.abuns[ind,:]))
            Ab = np.array(Ab)
        else:
            ind = self.spec.index(spec)
            Ab = np.interp(tget, self.time, self.abuns[ind,:])
        return Ab

class ReactionRates:
    """ Class provides a convenient way to find and store reactions and rates

    A ChemModl object need to be given to construct
    """
    def __init__(self, model, species):
        # Arrays and lists containing the Production side
        Pr_R1, Pr_R2 = [], []
        Pr_P1, Pr_P2, Pr_P3, Pr_P4, Pr_P5 = [], [], [], [], []
        Pr_Rate     = np.zeros((model.nre,model.nt))
        Pr_RateTot  = np.zeros(model.nt)
        Pr_RateCoef = np.zeros((model.nre,model.nt))
        Pr_RateMax  = np.zeros(model.nre)
        Pr_counter = 0
        # Arrays and lists containing the Destruction side
        Dst_R1, Dst_R2 = [], []
        Dst_P1, Dst_P2, Dst_P3, Dst_P4, Dst_P5 = [], [], [], [], []
        Dst_Rate     = np.zeros((model.nre,model.nt))
        Dst_RateTot  = np.zeros(model.nt)
        Dst_RateCoef = np.zeros((model.nre,model.nt))
        Dst_RateMax  = np.zeros(model.nre)
        Dst_counter = 0
        # Loop over the chemical reactions:
        for ii in range(len(model.r1)):
            products = [model.p1[ii], model.p2[ii], model.p3[ii], model.p4[ii],
                         model.p5[ii]]
            reactants= [model.r1[ii], model.r2[ii]]
            
            if species in products:
                Pr_R1.append(model.r1[ii])
                Pr_R2.append(model.r2[ii])
                Pr_P1.append(model.p1[ii])
                Pr_P2.append(model.p2[ii])
                Pr_P3.append(model.p3[ii])
                Pr_P4.append(model.p4[ii])
                Pr_P5.append(model.p5[ii])
                R1_index = model.spec.index(model.r1[ii])
                # Reaction "partners" like CRPHOT, PHOTON, FREEZE are not
                # listed in model.spec for these the expression is
                # k = n_r1 * ak. If two reactants are present then the rate is
                # given by k = n_r1 * n_r2 * ak. To differentiate we check for
                # the presence of R2 in model.spec:
                if model.r2[ii] in model.spec:
                    R2_index = model.spec.index(model.r2[ii])
                else:
                    R2_index = -np.nan
                # get the rate coefficient:
                #
                # if the rate coefficient is time dependent
                if model.ak.ndim > 1:
                    ak = model.ak[ii,:]
                # if the rate coefficient is *NOT* time dependent
                else:
                    ak = np.ones(model.nt) * model.ak[ii]
                # Store the rate coefficient and calculate the rate:
                Pr_TMP = ak * model.abuns[R1_index,:] * model.gdens
                if np.isfinite(R2_index):
                    Pr_TMP = Pr_TMP * model.abuns[R2_index,:] * model.gdens
                Pr_Rate[Pr_counter,:] = Pr_TMP
                if model.r2[ii] not in ['FREEZE','DESORB']:
                    Pr_RateTot[:] = Pr_RateTot[:] + Pr_TMP
                Pr_RateCoef[Pr_counter,:] = ak
                Pr_RateMax[Pr_counter] = Pr_TMP.max()
                Pr_counter = Pr_counter + 1
            #elif species in reactants:
                #Dst_R1.append(model.r1[ii])
                #Dst_R2.append(model.r2[ii])
                #Dst_P1.append(model.p1[ii])
                #Dst_P2.append(model.p2[ii])
                #Dst_P3.append(model.p3[ii])
                #Dst_P4.append(model.p4[ii])
                #Dst_P5.append(model.p5[ii])
                #R1_index = model.spec.index(model.r1[ii])
                ## Reaction "partners" like CRPHOT, PHOTON, FREEZE are not listed
                ## in model.spec for these the expression is k = n_r1 * ak. If
                ## two reactants are present then the rate is given by k = n_r1
                ## * n_r2 * ak. To differentiate we check for the presence of R2
                ## in model.spec:
                #if model.r2[ii] in model.spec:
                    #R2_index = model.spec.index(model.r2[ii])
                #else:
                    #R2_index = -np.nan
                ## get the rate coefficient:
                ## if the rate coefficient is time dependent
                #if model.ak.ndim > 1:
                    #ak = model.ak[ii,:]
                ## if the rate coefficient is *NOT* time dependent
                #else:
                    #ak = np.ones(model.nt) * model.ak[ii]
                ## Store the rate coefficient and calculate the rate:
                #Dst_TMP = ak * model.abuns[R1_index] * model.gdens
                #if np.isfinite(R2_index):
                    #Dst_TMP = Dst_TMP * model.abuns[R2_index] * model.gdens
                #Dst_Rate[Dst_counter,:] = Dst_TMP
                #Dst_RateTot[:] = Dst_RateTot[:] + Dst_TMP
                #Dst_RateCoef[Dst_counter,:] = ak
                #Dst_RateMax[Dst_counter] = Dst_TMP.max()
                #Dst_counter = Dst_counter + 1
        # Order the reactions by maximum rate
        Pr_Rate = Pr_Rate[0:Pr_counter,:]
        Pr_RateCoef = Pr_RateCoef[0:Pr_counter,:]
        Pr_RateMax = Pr_RateMax[0:Pr_counter]
        Pr_RateSort = Pr_RateMax
        for ii in range(Pr_counter):
            if np.isfinite(Pr_RateMax[ii]):
                Pr_RateSort[ii] = np.nanmax(Pr_Rate[ii,1:] / Pr_RateTot[1:])
            else:
                Pr_RateSort[ii] = 0.0
        OrderIndexPr = Pr_RateSort.argsort()
        # reverse it!
        OrderIndexPr = OrderIndexPr[::-1]
        self.ProdRate = Pr_Rate[OrderIndexPr,:]
        self.ProdRateCoef = Pr_RateCoef[OrderIndexPr,:]
        self.ProdRateMax = Pr_RateMax[OrderIndexPr]
        self.ProdRateTot = Pr_RateTot
        
        self.ProdR1 = np.array(Pr_R1)[OrderIndexPr]
        self.ProdR2 = np.array(Pr_R2)[OrderIndexPr]
        self.ProdP1 = np.array(Pr_P1)[OrderIndexPr] 
        self.ProdP2 = np.array(Pr_P2)[OrderIndexPr]
        self.ProdP3 = np.array(Pr_P3)[OrderIndexPr]
        self.ProdP4 = np.array(Pr_P4)[OrderIndexPr]
        self.ProdP5 = np.array(Pr_P5)[OrderIndexPr]
        self.nProd = len(self.ProdR1)

        #Dst_Rate = Dst_Rate[0:Dst_counter,:]
        #Dst_RateCoef = Dst_RateCoef[0:Dst_counter,:]
        #Dst_RateMax = Dst_RateMax[0:Dst_counter]
        #for ii in range(Dst_counter):
            #Dst_RateMax[ii] = np.nanmax(Dst_Rate[ii,:] / Dst_RateTot)
        #OrderIndexDst = Dst_RateMax.argsort()
        ## reverse it!
        #OrderIndexDst = OrderIndexDst[::-1]
        #self.DestRate = Dst_Rate[OrderIndexDst,:]
        #self.DestRateCoef = Dst_RateCoef[OrderIndexDst,:]
        #self.DestRateMax = Dst_RateMax[OrderIndexDst]
        #self.DestRateTot = Dst_RateTot
        #self.DestR1 = commons.sortbynparr(Dst_R1,OrderIndexDst)
        #self.DestR2 = commons.sortbynparr(Dst_R2,OrderIndexDst)
        #self.DestP1 = commons.sortbynparr(Dst_P1,OrderIndexDst)
        #self.DestP2 = commons.sortbynparr(Dst_P2,OrderIndexDst)
        #self.DestP3 = commons.sortbynparr(Dst_P3,OrderIndexDst)
        #self.DestP4 = commons.sortbynparr(Dst_P4,OrderIndexDst)
        #self.DestP5 = commons.sortbynparr(Dst_P5,OrderIndexDst)
        #self.nDest = len(self.DestR1)
        
        return

def read_alchemic(filename="hwcloud_00215090.idl", binary=False,
            hdf5=False, specfile=None, verbose=True, id_list=None):
    """ Reads the ALCHEMIC .idl output file to a ChemModl objects to work with.

    Reads the ALCHEMIC .idl output file to an object this can then be analysed
    and plotted. The code works similarly to its predeceser IDL code
    read_alchemic.pro

    :param filename: file containing the results of ALCHEMIC calculations.
                     Typically with .idl extension.
    :type filename: str
    :param perline: number of items per line the input file (default 9)
    :type perline: int
    """
    if verbose:
        print( "Reading {} ...".format(filename) )
    #
    if binary or (filename.rsplit('.')[-1] == 'bout'):
        binary = True
    #
    elif hdf5 or (filename.rsplit('.')[-1] in ['hdf5','h5']):
        hdf5 = True
    #
    # Read the data from the file
    if binary:
        #
        # get the species names in the model
        #try:
        ChSplit = "."
        ChTmp = filename.rsplit(ChSplit)[0:-1]
        if specfile == None:
            specfile = ChSplit.join(ChTmp) + "_species.dat"
        f = open(specfile,"r")
        print("Species names are searched in {}...".format(specfile))
        ns = np.int32(f.readline().replace("\n","").strip().rsplit('=')[1])
        spec = commons.readIDLlikeArrayFromFile(f,nx=ns,dtype=np.str_,perline=9)
        #except:
            #raise ValueError("ERROR in reading ALCHEMIC output: no *_species.dat found!")
        # Open the binary model file
        binfile = open(filename,"rb")
        # first read the dimensions of the data
        MetaType = np.dtype([("meta", (np.int32,3) )])
        data0 = np.fromfile(binfile,count=1,dtype=MetaType)
        nCell = data0['meta'][0][0]
        nTs = data0['meta'][0][1]
        nSpec = data0['meta'][0][2]
        # List containing models
        data1 = []
        for p in range(nCell):
        # structure and read the main data for the first model
            if (p > 0):
               try:
                   data0 = np.fromfile(binfile,count=1,dtype=MetaType)
                   nTs = data0['meta'][0][1]
                   nSpec = data0['meta'][0][2]
               except:
                   print( "{} Binary file incomplete: {}, {} (last, expected)".format(filename,p,nCell) )
                   if verbose:
                       cont = raw_input("Continue anyway? (yes/no) ")
                       if cont not in ["yes","y"]:
                           sys.exit(0)
                   # tell the following code that we have less cells
                   nCell = p
                   break
            ModelType = np.dtype([("ID",(np.int32,1)),
                       ("time",(np.double,nTs)),("x",(np.double,nTs)),
                       ("y",(np.double,nTs)),("z",(np.double,nTs)),
                       ("rho",(np.double,nTs)),("AvSt",(np.double,nTs)),
                       ("AvIS",(np.double,nTs)),("Tg",(np.double,nTs)),
                       ("Td",(np.double,nTs)),("ZetaCR",(np.double,nTs)),
                       ("gdens",(np.double,nTs)),
                       ("abuns",(np.double,(nTs,nSpec)))])
            data1.append( np.fromfile(binfile,count=1,dtype=ModelType) )
        if nCell > 1 and verbose:
            print( "{} particle in binary file! Creating ChemModl list...".format(nCell) )
        ModelList = []
        for p in range(nCell):
        # unpack the data for the test (only one cell)
            ID = data1[p]['ID'][0]
            time = data1[p]['time'][0] / 31557600.  # seconds to years
            x = data1[p]['x'][0]
            y = data1[p]['y'][0]
            z = data1[p]['z'][0]
            rho = np.transpose(data1[p]['rho'][0])
            AvSt = data1[p]['AvSt'][0]
            AvIS = data1[p]['AvIS'][0]
            Tg = data1[p]['Tg'][0]
            Td = data1[p]['Td'][0]
            ZetaCR = data1[p]['ZetaCR'][0]
            gdens = np.transpose(data1[p]['gdens'][0])
            abuns = np.transpose(data1[p]['abuns'][0])
            args = {'ID':ID,'time':time,'Tg':Tg,'Td':Td,'rho':rho,'gdens':gdens,
                    'AvSt':AvSt,'AvIS':AvIS,'ZetaCR':ZetaCR,'spec':spec,
                    'x':x,'y':y,'z':z,'abuns':abuns}
            ModelList.append(ChemModl(**args))
        # if only one element is in the binary return that, if not then return
        # the list!
        if nCell == 1:
            model = ModelList[0]
        else:
            model = ModelList
            
    elif hdf5:
        
        ModelList = []
    
        f = h5py.File(filename, 'r')
    
        contents = f.keys()
        
        # Read run parameters
        
        # Read network data
        spec = list(f[u'network']['spec_name'][()])
        r1 = list(f[u'network']['r1'][()])
        r2 = list(f[u'network']['r2'][()])
        p1 = list(f[u'network']['p1'][()])
        p2 = list(f[u'network']['p2'][()])
        p3 = list(f[u'network']['p3'][()])
        p4 = list(f[u'network']['p4'][()])
        p5 = list(f[u'network']['p5'][()])
        
        # TODO: quick fix, do it better
        for i in range(len(spec)):
            spec[i] = spec[i].decode('ascii')
        
        for i in range(len(r1)):
            r1[i] = r1[i].decode('ascii')
            r2[i] = r2[i].decode('ascii')
            p1[i] = p1[i].decode('ascii')
            p2[i] = p2[i].decode('ascii')
            p3[i] = p3[i].decode('ascii')
            p4[i] = p4[i].decode('ascii')
            p5[i] = p5[i].decode('ascii')
        
        # Read model data
        for group_name in contents:
            if group_name == u'network':
                pass
            elif (id_list is not None) and (group_name not in id_list):
                pass
            else:
                ID = f[group_name]['ID'][0] #.value[0]
                time = f[group_name]['time'][()] / 31557600.
                Tg = f[group_name]['Tg'][()]
                Td = f[group_name]['Td'][()]
                rho = f[group_name]['rho'][()]
                gdens = f[group_name]['gdens'][()]
                AvSt = f[group_name]['AvSt'][()]
                AvIS = f[group_name]['AvIS'][()]
                ZetaCR = f[group_name]['ZetaCR'][()]
                ZetaX = f[group_name]['ZetaX'][()]
                G0 = f[group_name]['G0'][()]
                x = f[group_name]['x_loc'][()]
                y = f[group_name]['y_loc'][()]
                z = f[group_name]['z_loc'][()]
                    
                abuns = f[group_name]['abuns'][()]
                try:
                    ak = f[group_name]['ak'][()]
                except:
                    ak = np.zeros_like(r1, dtype=np.float)
                    
                args = {'ID':ID,'time':time,'Tg':Tg,'Td':Td,'rho':rho,
                        'gdens':gdens,'AvSt':AvSt,'AvIS':AvIS,
                        'ZetaCR':ZetaCR,'x':x,'y':y,'z':z,'abuns':abuns.T,
                        'spec':spec,'r1':r1,'r2':r2,'p1':p1,'p2':p2,
                        'p3':p3,'p4':p4,'p5':p5,'ak':ak.T, 'G0':G0}

                ModelList.append(ChemModl(**args))
            
        f.close()
        
        # if only one element is in the binary return that, if not then return
        # the list!
        if len(group_name) == 1:
            model = ModelList[0]
        else:
            model = ModelList
    
    else:
        f = open(filename,"r")
        f.readline()
        specfile = f.readline().replace("\n","").strip()
        ratefile = f.readline().replace("\n","").strip()
        ipon = int(f.readline().replace("\n","").strip())
        ID = int(f.readline().replace("\n","").strip())
        x = float(f.readline().replace("\n","").strip())
        y = float(f.readline().replace("\n","").strip())
        z = float(f.readline().replace("\n","").strip())
        rhod = float(f.readline().replace("\n","").strip())
        mdmg = float(f.readline().replace("\n","").strip())
        grain = float(f.readline().replace("\n","").strip())
        albedo_UV = float(f.readline().replace("\n","").strip())
        tlast = float(f.readline().replace("\n","").strip())
        tfirst = float(f.readline().replace("\n","").strip())
        nstep = int(f.readline().replace("\n","").strip())

        time = commons.readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
        Tg = commons.readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
        Td = commons.readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
        rho = commons.readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
        AvSt = commons.readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
        AvIS = commons.readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
        G0 = commons.readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
        fH2_St = commons.readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
        fCO_St = commons.readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
        fH2_IS = commons.readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
        fCO_IS = commons.readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
        ZetaCR = commons.readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
        ZetaX = commons.readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
        gdens = commons.readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)
        amu = commons.readIDLlikeArrayFromFile(f,nx=nstep,dtype=np.double)

        nInitSpec = int(f.readline().replace("\n","").strip())
        InitSpec = []
        InitAbuns = np.zeros(nInitSpec)
        for i in range(nInitSpec):
            line = (f.readline()).split()
            InitSpec.append(line[0].strip())
            InitAbuns[i] = float(line[1].replace("D","E"))

        ns = int(f.readline().replace("\n","").strip())
        spec = commons.readIDLlikeArrayFromFile(f,nx=ns,dtype=np.str_,perline=9)

        nre = int(f.readline().replace("\n","").strip())
        r1 = commons.readIDLlikeArrayFromFile(f,nx=nre,dtype=np.str_,perline=9)
        r2 = commons.readIDLlikeArrayFromFile(f,nx=nre,dtype=np.str_,perline=9)
        p1 = commons.readIDLlikeArrayFromFile(f,nx=nre,dtype=np.str_,perline=9)
        p2 = commons.readIDLlikeArrayFromFile(f,nx=nre,dtype=np.str_,perline=9)
        p3 = commons.readIDLlikeArrayFromFile(f,nx=nre,dtype=np.str_,perline=9)
        p4 = commons.readIDLlikeArrayFromFile(f,nx=nre,dtype=np.str_,perline=9)
        p5 = commons.readIDLlikeArrayFromFile(f,nx=nre,dtype=np.str_,perline=9)

        ak = commons.readIDLlikeArrayFromFile(f,nx=nre,ny=nstep,dtype=np.double)
        abuns = commons.readIDLlikeArrayFromFile(f,nx=ns,ny=nstep,dtype=np.double)

        args = {'x':x, 'y':y, 'z':z, 'rhod':rhod, 'mdmg':mdmg, 'grain':grain,
                    'albedo':albedo_UV, 'time':time, 'Tg':Tg, 'Td':Td,
                    'rho':rho, 'AvSt':AvSt, 'AvIS':AvIS, 'G0':G0,
                    'fH2_St':fH2_St, 'fH2_IS':fH2_IS, 'fCO_IS':fCO_IS,
                    'fCO_St':fCO_St, 'ZetaCR':ZetaCR, 'ZetaX':ZetaX,
                    'gdens':gdens, 'amu':amu, 'spec':spec, 'r1':r1, 'r2':r2,
                    'p1':p1, 'p2':p2, 'p3':p3, 'p4':p4, 'p5':p5, 'ak':ak,
                    'abuns':abuns, 'InitSpec':InitSpec, 'InitAbuns':InitAbuns,
                    'specfile':specfile, 'ratefile':ratefile, 'ID':ID}
        # Here is the returned object!
        model = ChemModl(**args)

    # return the chemical model
    return model

def read_sipila2015(filename='Sipila2015_fig1.dat',nt=100,nh=12,verbose=True):
    """ Reads model results from Sipila (2015) chemical calculations.

    :param filename: file containing the results of Sipila (2015) calculations.
    :type filename: str
    :param nt: number of timesteps in input file (default 100)
    :type nt: int
    :param nh: number of header lines
    :type nh: int
    """
    if verbose:
        print( "Reading {} ...".format(filename) )
    mp = 1.6726000e-24    # proton mass in grams
    year = 31557600.0     # year in seconds
    # Read the data from the file
    f = open(filename,"r")
    # Read and work with the model physical properties
    for i in range(nh):
        line = f.readline().replace("#","").split()
        try:
            print("%s = %d" % (line[0],float(line[1])) )
            exec("%s = %d" % (line[0],float(line[1])), locals())
        except:
            continue
    rho = n_H * (1.4*mp)
    f.readline()  # read the separating character
    # Read and work with the species names
    spec = f.readline().replace("#","").split()
    for i in range(len(spec)):
        spec[i] = spec[i].replace('Mg','MG')
        spec[i] = spec[i].replace('He','HE')
        spec[i] = spec[i].replace('Fe','FE')
        spec[i] = spec[i].replace('Si','SI')
        spec[i] = spec[i].replace('Na','NA')
        spec[i] = spec[i].replace('Cl','CL')
        spec[i] = spec[i].replace('g','G')
        spec[i] = spec[i].replace('e-','ELECTR')
        if ( '*' in spec[i] ):
            spec[i] = 'g' + spec[i].replace('*','')
    nspec = len(spec)
    # Read the abundances now (actually these are number densities)
    time = np.zeros(nt)
    abuns = np.zeros((nspec,nt))
    for i in range(nt):
        line = f.readline().split()
        time[i] = float(line[0])
        abuns[:,i] = np.array(line[1:], dtype=np.double) / n_H
    f.close()
    # Collect the data to a dictionary to be passed to the class creator
    args = {'x':0, 'y':0, 'z':0, 'grain':a_g, 'time':time/year,
            'Tg':float(T_gas), 'Td':float(T_dust), 'rho':rho,
            'AvIS':float(A_V), 'G0':1.7, 'ZetaCR':float(CRP),
            'gdens':float(n_H), 'spec':spec, 'abuns':abuns}
    model = ChemModl(**args)
    return model

def read_nahoon(filename="plot.dat",ns=454,nt=124,verbose=True,
                kappa_file=None,network_file=None):
    """ Reads model results from the Nahoon/Nautilus code (Wakelam et al. 2015).

    :param filename: file containing the chemical calculation results.
    :type filename: str
    :param ns: number of species (default for kida.uv.2014 is 454)
    :type perline: int
    :param nt: number of time steps (default 124)
    """
    mp = 1.6726000e-24             # proton mass in cgs
    # Create the species and abundances arrays
    spec = []
    abuns = np.zeros((ns,nt))
    time = np.zeros(nt)
    if verbose:
        print( "Reading {} ...".format(filename) )
    # Read the data from the file
    f = open(filename,"r")
    f.readline()
    T = float(f.readline().replace("Gas temperature (K)","").strip())
    n_H = float(f.readline().replace("Total H density (cm-3)","").strip())
    rho = n_H * (1.4*mp)
    time[:] = np.array(f.readline().replace("D","E").split())

    for i in range(ns-1):
        line = (f.readline().replace("D","E")).split()
        spec.append(line[0])
        abuns[i,:] = line[1::]

    for i in range(len(spec)):
        spec[i] = spec[i].replace('Mg','MG')
        spec[i] = spec[i].replace('He','HE')
        spec[i] = spec[i].replace('Fe','FE')
        spec[i] = spec[i].replace('Si','SI')
        spec[i] = spec[i].replace('Na','NA')
        spec[i] = spec[i].replace('Cl','CL')
        spec[i] = spec[i].replace('GRAIN','G')
        spec[i] = spec[i].replace('e-','ELECTR')
        spec[i] = spec[i].replace('Photon','PHOTON')
        spec[i] = spec[i].replace('CRP','CRPHOT')
        spec[i] = spec[i].replace('CR','CRP')
        if ( 'X' in spec[i] ):
            spec[i] = 'g' + spec[i].replace('X','')
    f.close()

    '''
    try:
        if not kappa_file:
            kappa_file = "Kout" + filename[9::]
        if not network_file:
            network_file = 'kida.uva.2014'
        f = open(kappa_file,"r")
        if verbose:
            print("Reading the network and the rate coefficients...")
        tmp_network = network.read_network(input_file=network_file,
                    skiplines=0)
        rindex = []
        ratecoef = []
        tmp = f.readline()
        tmp2 = f.readline()
        for line in f:
            line = line.split()
            rindex.append(int(line[0]))
            ratecoef.append(float(line[1]))
        r1 = []
        r2 = []
        p1 = []
        p2 = []
        p3 = []
        p4 = []
        p5 = []
        ak = []
        for i in range(tmp_network.nre):
            r1.append(tmp_network.r1[i])
            r2.append(tmp_network.r2[i])
            p1.append(tmp_network.p1[i])
            p2.append(tmp_network.p2[i])
            p3.append(tmp_network.p3[i])
            p4.append(tmp_network.p4[i])
            p5.append(tmp_network.p5[i])
            try:
                loc = rindex.index(tmp_network.index[i])
                ak.append(ratecoef[loc])
            except:
                print( "{}, {}, {} Not found!".format(tmp_network.index[i], tmp_network.r1[i], tmp_network.r2[i]) )
        ak = np.array(ak)

        args = {'x':0, 'y':0, 'z':0, 'mdmg':1E-2, 'grain':1E-5,
            'time':time, 'Tg':T, 'Td':T, 'rho':rho, 'AvIS':30., 'G0': 1.7,
            'ZetaCR':1.3E-17, 'gdens':n_H, 'amu':1.4, 'spec':spec,
            'abuns':abuns, 'r1':r1, 'r2':r2, 'p1':p1, 'p2':p2, 'p3':p3,
            'p4':p4, 'p5':p5, 'ak':ak}
    except:
    '''
    args = {'x':0, 'y':0, 'z':0, 'mdmg':1E-2, 'grain':1E-5,
                'time':time, 'Tg':T, 'Td':T, 'rho':rho, 'AvIS':30., 'G0': 1.7,
                'ZetaCR':1.3E-17, 'gdens':n_H, 'amu':1.4, 'spec':spec,
                'abuns':abuns}

    model = ChemModl(**args)
    return model

def read_semenov2010(filename="TMC1.osu0803.idl",perline=9):
    """ Reads model results from Semenov (2010) chemical calculations.

    :param filename: file containing legacy ALCHEMIC calculations, where no
            time dependent physical conditions are considered.
    :type filename: str
    :param perline: number of items per line in input file (default 9)
    :type perline: int
    """
    mp = 1.6726000e-24             # proton mass in cgs
    print( "Reading {} ...".format(filename) )
    # Read the data from the file
    f = open(filename,"r")
    f.readline()
    specfile = f.readline().replace("\n","").strip()
    ratefile = f.readline().replace("\n","").strip()
    ID = int(f.readline().replace("\n","").strip())
    x = float(f.readline().replace("\n","").strip())
    y = float(f.readline().replace("\n","").strip())
    z = 0.0
    T = float(f.readline().replace("\n","").strip())
    rho = float(f.readline().replace("\n","").strip())
    rhod = float(f.readline().replace("\n","").strip())
    mdmg = float(f.readline().replace("\n","").strip())
    grain = float(f.readline().replace("\n","").strip())
    AvSt = float(f.readline().replace("\n","").strip())
    AvIS = float(f.readline().replace("\n","").strip())
    G0 = float(f.readline().replace("\n","").strip())
    ZetaCR = float(f.readline().replace("\n","").strip())
    ZetaX = float(f.readline().replace("\n","").strip())
    albedo_UV = float(f.readline().replace("\n","").strip())
    tlast = float(f.readline().replace("\n","").strip())
    tfirst = float(f.readline().replace("\n","").strip())

    nInitSpec = int(f.readline().replace("\n","").strip())
    InitSpec = []
    InitAbuns = np.zeros(nInitSpec)
    for i in range(nInitSpec):
        line = (f.readline()).split()
        InitSpec.append(line[0].strip())
        InitAbuns[i] = float(line[1].replace("D","E"))
    ns = int(f.readline().replace("\n","").strip())
    print("reading the species")
    nlines = ns // perline
    reminder = ns % perline
    if reminder > 0:
        nlines = nlines + 1
    StrList = []
    for i in range(nlines):
        StrList.append(f.readline())
    StrTmp = " ".join(StrList)
    spec = StrTmp.split()
    for i in range(len(spec)):
        spec[i] = spec[i].strip()

    nstep = int(f.readline().replace("\n","").strip())
    time = np.array(f.readline().split())
    time = time.astype(np.double)

    nre = int(f.readline().replace("\n","").strip())
    ak = commons.readIDLlikeArrayFromFile(f,nx=nre,dtype=np.double)
    abuns = commons.readIDLlikeArrayFromFile(f,nx=ns,ny=nstep,dtype=np.double)
    gdens = rho / (1.4 * mp)
    abuns = abuns / gdens

    args = {'x':x, 'y':y, 'z':z, 'rhod':rhod, 'mdmg':mdmg, 'grain':grain,
            'albedo_UV':albedo_UV, 'time':time, 'Tg':T, 'Td':T, 'rho':rho,
            'AvSt':AvSt, 'AvIS':AvIS, 'G0':G0, 'ZetaCR':ZetaCR, 'ZetaX':ZetaX,
            'gdens':gdens, 'amu':1.4, 'spec':spec, 'ak':ak, 'abuns':abuns,
            'InitSpec':InitSpec, 'InitAbuns':InitAbuns, 'specfile':specfile,
            'ratefile':ratefile}
    model = ChemModl(**args)
    return model

def read_nautilus(dir='.',specfile='species.out',ncell=1):
    mp = 1.6726e-24
    amu = 1.4
    # Get all output files
    file_list = glob.glob(dir+'/abundances.*.alc.out')
    file_list.sort()
    nt = len(file_list)
    # Read the species names
    f = open(dir+"/"+specfile,"r")
    spec_ascii = f.readlines()
    f.close()
    nspec = 5*len(spec_ascii)
    s_index = np.zeros(nspec,dtype=np.int32)
    spec = np.chararray(nspec,itemsize=12)
    c_ind = 0
    c_spec = 0
    for line in spec_ascii:
        tmp = line.replace(')','').split()
        for i in range(len(tmp)):
            if i % 2 == 0:
                s_index[c_ind] = np.int32(tmp[i])
                c_ind = c_ind + 1
            else:
                spec[c_spec] = tmp[i]
                c_spec = c_spec + 1
    # remove 0 elements from the arrays if not all lines have 5 elements
    s_index = s_index[0:c_ind]
    spec = spec[0:c_spec]
    spec = spec.tolist()
    nspec = len(spec)

    print( "Reading Nautilus model from {} ...".format(dir) )

    for iss in range(nspec):
        spec[iss] = spec[iss].replace('J','g')
        spec[iss] = spec[iss].replace('e-','ELECTR')
        spec[iss] = spec[iss].replace('He','HE')
        spec[iss] = spec[iss].replace('Fe','FE')

    # Create the arrays to store the model data0
    time = np.zeros(nt)
    Tg = np.zeros(nt)
    Td = np.zeros(nt)
    gdens = np.zeros(nt)
    AvIS = np.zeros(nt)
    Xrate = np.zeros(nt)
    abuns = np.zeros((nspec,nt))
    year = 31557600.0     # year in seconds
    for it in range(nt):
        binfile = open(file_list[it],'rb')
        MetaType = np.dtype([("meta", (np.int32,2) )])
        PhysType = np.dtype([("phys", (np.float64,6) )])
        AbunType = np.dtype([("abuns", (np.float64,nspec) )])
        meta0 = np.fromfile(binfile,count=1,dtype=MetaType)
        data0 = np.fromfile(binfile,count=1,dtype=PhysType)
        abun0 = np.fromfile(binfile,count=1,dtype=AbunType)

        time[it] = data0['phys'][0][0] / year
        gdens[it] = data0['phys'][0][3]
        Tg[it] = data0['phys'][0][1]
        Td[it] = data0['phys'][0][2]
        AvIS[it] = data0['phys'][0][4]
        abuns[:,it] = abun0["abuns"][0][:]

        binfile.close()

    args = {'ID':0,'time':time,'Tg':Tg,'Td':Td,'rho':gdens*amu*mp,'gdens':gdens,
            'AvIS':AvIS,'spec':spec,'abuns':abuns}

    return ChemModl(**args)

#        MetaType = np.dtype([("time", (np.float64,(1,1))])
#        data0 = np.fromfile(binfile,count=1,dtype=MetaType)
#        print fileout, data0['time']
#        binfile.close()


    return 1


def write_alchemic(models=None, filename='1environ.inp',mode='w',
            tmin=1.0E-2,tmax=1.0E+7,nt=100,rho=1.0E+4,Tg=10.0,Td=10.0,
            G0=0.0,AvSt=0.0,AvIS=10.0,ZetaCR=1.0E-17,amu=1.4,final=None):
    ''' Writes one or more ChemModl object to ALCHEMIC physical conditions
    input file (1environ.inp). This is based on the old make_environ.pro
    IDL code.

    :param models: A ChemModl object or a list of ChemModl objects to be
            written to the output ALCHEMIC physical condition file.
    :type models: ChemModl or list
    :param filename: Name of the ALCHEMIC physical condition file to be written
    :type filename: string
    :param tmin: Starting time for pseudo-time dependent model.
    :type tmin: float
    :param tmax: Ending time for pseudo-time dependent model.
    :type tmax: float
    '''
    mp = 1.6726000e-24    # proton mass in grams
    # Check the type of models
    if models == None:
        print('No ChemModl was given, writing model given in arguments!')
        Simple = True
    else:
        Simple = False
        if (not type(models) == list ) and (not isinstance(models,ChemModl) ):
            raise ValueError('Invalied input variable! (only takes ChemModl object)')
    if Simple:
        # How many models will be written?
        params = [rho,Tg,Td,AvIS,ZetaCR]
        types = []
        nPars = [1]
        for pari in params:
            types.append(type(pari))
            if types[-1] in [list, np.ndarray]:
                nPars.append(len(pari))
        nModl = max(nPars)
        ID = np.arange(nModl) + 1
        # Determine the timesteps
        dlgt = (np.log10(tmax)-np.log10(tmin)) / (nt-1)
        time = 10**(np.arange(nt) * dlgt + np.log10(tmin))
        # Open file for writing
        FileExists = os.path.isfile(filename)
        if FileExists and (mode == "a"):
            fdat = open(filename, mode)
        else:
            fdat = open(filename, "w")
            fdat.write("# ID x y z rho Tg  Td  G0  AvSt  AvIS  ZetaCR  ZetaX  "
                + "fH2_St  fCO_St fH2_IS  fCO_IS T00 T01 Hr  T0  TF\n")
            fdat.write('{:8n}\n'.format(1))      # dummy
            fdat.write('{:8n}\n'.format(nModl))  # number of models in the file
        # Start to write the models
        for j in range(nModl):
            fdat.write('{:8n}\n'.format(nt-1))         # number of timesteps
            for i in np.arange(nt-1)+1:
                fdat.write('{:8n}  '.format(ID[j]))
                fdat.write('{:11.4E}  {:11.4E}  {:11.4E}  '.format(0,0,0))
                if types[0] == list:
                    fdat.write('{:11.4E}  '.format(rho[j]*mp*amu))
                else:
                    fdat.write('{:11.4E}  '.format(rho*mp*amu))
                if types[1] == list:
                    fdat.write('{:11.4E}  '.format(Tg[j]))
                else:
                    fdat.write('{:11.4E}  '.format(Tg))
                if types[2] == list:
                    fdat.write('{:11.4E}  '.format(Td[j]))
                else:
                    fdat.write('{:11.4E}  '.format(Td))
                fdat.write('{:11.4E}  {:11.4E}  '.format(G0,AvSt))
                if types[3] == list:
                    fdat.write('{:11.4E}  '.format(AvIS[j]))
                else:
                    fdat.write('{:11.4E}  '.format(AvIS))
                if types[4] == list:
                    fdat.write('{:11.4E}  '.format(ZetaCR[j]))
                else:
                    fdat.write('{:11.4E}  '.format(ZetaCR))
                fdat.write('{:11.4E}  '.format(0))
                fdat.write('{:11.4E}  {:11.4E}  '.format(1,1))
                if types[3] == list:
                    fdat.write('{:11.4E}  '.format(np.exp(-2.5*AvIS[j])))
                else:
                    fdat.write('{:11.4E}  '.format(np.exp(-2.5*AvIS)))
                if types[3] == list:
                    fdat.write('{:11.4E}  '.format(np.exp(-1.7*AvIS[j])))
                else:
                    fdat.write('{:11.4E}  '.format(np.exp(-1.7*AvIS)))
                fdat.write('{:11.4E}  {:11.4E}  '.format(time[i-1],time[i]))
                fdat.write('{:11.4E}  {:11.4E}  {:11.4E}'.format(0,0,0))
                # New line
                fdat.write('\n')
        fdat.close()
        return filename + " was written!"
    else:
        # How many models will be written?
        if isinstance(models,ChemModl):
            nModl = 1
            models = [models]
        else:
            nModl = len(models)
        # Open file for writing
        FileExists = os.path.isfile(filename)
        if FileExists and (mode == "a"):
            fdat = open(filename, mode)
        else:
            fdat = open(filename, "w")
            fdat.write("# ID x y z rho Tg  Td  G0  AvSt  AvIS  ZetaCR  ZetaX  "
                + "fH2_St  fCO_St fH2_IS  fCO_IS T00 T01 Hr  T0  TF\n")
            fdat.write('{:8n}\n'.format(1))      # dummy
            fdat.write('{:8n}\n'.format(nModl))  # number of models in the file
        # Start to write the models
        for m in models:
            if final:
                fin = min([final,m.nt])
            else:
                fin = m.nt-1
            fdat.write('{:8n}\n'.format(fin-1))         # number of timesteps
            for i in np.arange(1,fin):
                fdat.write('{:8n}  '.format(m.ID))
                coords = [0.0,0.0,0.0]
                # Write coordinates
                if np.all(m.x) and np.shape(m.x):
                    coords[0] = m.x[i]
                if np.all(m.y) and np.shape(m.y):
                    coords[1] = m.y[i]
                if np.all(m.z) and np.shape(m.z):
                    coords[2] = m.z[i]
                fdat.write('{:11.4E}  {:11.4E}  {:11.4E}  '.format(coords[0],coords[1],coords[2]))
                # Write local model parameters
                fdat.write('{:11.4E}  '.format(m.rho[i]))
                fdat.write('{:11.4E}  '.format(m.Tg[i]))
                fdat.write('{:11.4E}  '.format(m.Td[i]))
                # Write the local irradiation parameters
                if np.any(m.G0):
                    if (np.size(m.G0) > 1):
                        fdat.write('{:11.4E}  '.format(m.G0[i]))
                    else:
                        fdat.write('{:11.4E}  '.format(m.G0))
                else:
                    fdat.write('{:11.4E}  '.format(0))
                # AvSt
                if np.any(m.AvSt):
                    fdat.write('{:11.4E}  '.format(m.AvSt[i]))
                else:
                    fdat.write('{:11.4E}  '.format(0))
                # Write the Av towards the ISRF
                fdat.write('{:11.4E}  '.format(m.AvIS[i]))
                # Write cosmic ray flux
                if type(m.ZetaCR) == np.ndarray:
                    fdat.write('{:11.4E}  '.format(m.ZetaCR[i]))
                else:
                    fdat.write('{:11.4E}  '.format(m.ZetaCR))
                # Write X-ray flux
                fdat.write('{:11.4E}  '.format(0))
                # Write local shielding coefficients
                if np.any(m.fH2_St):
                    fdat.write('{:11.4E}  '.format(m.fH2_St[i]))
                elif np.any(m.AvSt):
                    fdat.write('{:11.4E}  '.format(np.exp(-2.5*m.AvSt[i])))
                else:
                    fdat.write('{:11.4E}  '.format(0.0))
                if np.any(m.fCO_St):
                    fdat.write('{:11.4E}  '.format(m.fCO_St[i]))
                elif np.any(m.AvSt):
                    fdat.write('{:11.4E}  '.format(np.exp(-1.7*m.AvSt[i])))
                else:
                    fdat.write('{:11.4E}  '.format(0.0))
                # Write shielding coefficients measured towards the ISRF
                if np.any(m.fH2_IS):
                    fdat.write('{:11.4E}  '.format(m.fH2_IS[i]))
                else:
                    fdat.write('{:11.4E}  '.format(np.exp(-2.5*m.AvIS[i])))
                if np.any(m.fCO_IS):
                    fdat.write('{:11.4E}  '.format(m.fCO_IS[i]))
                else:
                    fdat.write('{:11.4E}  '.format(np.exp(-1.7*m.AvIS[i])))
                # Write the time parameters
                if (i == 1):
                    tstart = 0.0E0
                else:
                    tstart = m.time[i]
                fdat.write('{:11.4E}  {:11.4E}  '.format(tstart,m.time[i+1]))
        	# Write dummy parameters, maybe used in the future
                fdat.write('{:11.4E}  {:11.4E}  {:11.4E}  '.format(0,0,0))
                # Write the Av towards the ISRF if HEALPIX data is available
                if np.all(m.AvArr):
                    for Avtmp in m.AvArr[i,:]:
                        if np.all(m.AvLocal):
                            AvLocal = m.AvLocal[i]
                        else:
                            AvLocal = 0.0
                        fdat.write('{:11.4E}  '.format(Avtmp+AvLocal))
                # New line
                fdat.write('\n')
        fdat.close()
        return filename + " was written!"

def write_nautilus(models=None, filename='structure_evolution.dat',mode='w',
            tmin=1.0E-2,tmax=1.0E+7,nt=100,
            rho=1.0E+4,Tg=10.0,Td=10.0,AvIS=10.0,ZetaCR=1.0E-17,amu=1.4):
    ''' Writes (currently) *one* ChemModl object to NAUTILUS physical conditions
    input file (1environ.inp). This is based on the write_alchemic() function.

    :param models: A ChemModl object to be written to the output ALCHEMIC
    		   physical condition file.
    :type models: ChemModl
    :param filename: Name of the NAUTILUS physical condition file to be written
    :type filename: string
    :param tmin: Starting time for pseudo-time dependent model.
    :type tmin: float
    :param tmax: Ending time for pseudo-time dependent model.
    :type tmax: float
    '''
    mp = 1.6726000e-24    # proton mass in grams
    # Check the type of models
    if models == None:
        print('No ChemModl was given, writing model given in arguments!')
        Simple = True
    else:
        Simple = False
        if (not type(models) == list ) and (not isinstance(models,ChemModl) ):
            raise ValueError('Invalied input variable! (only takes ChemModl object)')
    if Simple:
        # How many models will be written?
        params = [rho,Tg,Td,AvIS,ZetaCR]
        types = []
        nPars = [1]
        for pari in params:
            types.append(type(pari))
            if types[-1] in [list, np.ndarray]:
                nPars.append(len(pari))
        nModl = max(nPars)
        ID = np.arange(nModl) + 1
        # Determine the timesteps
        dlgt = (np.log10(tmax)-np.log10(tmin)) / (nt-1)
        time = 10**(np.arange(nt) * dlgt + np.log10(tmin))
        # Open file for writing
        FileExists = os.path.isfile(filename)
        if FileExists and (mode == "a"):
            fdat = open(filename, "w")
            warnings.warn("Warning! Output file already exists and only single model can be written, overwriting!")
        else:
            fdat = open(filename, "w")
        # Start to write the models
        if nModl > 1:
            raise ValueError('Only a single model can be written to NAUTILUS input at the moment! Aborting!')
        else:
            fdat.write("! time    log(Av)    log(n)    log(Tg)   log(Td)   \n! (yr)    (mag)      (cm-3)    (K)       (K)   \n")
            for i in range(nt):
            	fdat.write('{:9.3E} '.format(time[i]))
            	fdat.write('{:9.3E} '.format(np.log10(AvIS)))
            	fdat.write('{:9.3E} '.format(np.log10(rho)))
            	fdat.write('{:9.3E} '.format(np.log10(Tg)))
            	fdat.write('{:9.3E} '.format(np.log10(Td)))
            	# New line
            	fdat.write('\n')
            fdat.close()
            return filename + " was written!"
    else:
        # How many models will be written?
        # TODO: Write multiple models to (a single) file(s) for NAUTILUS
        if isinstance(models,ChemModl):
            nModl = 1
            models = [models]
        else:
            nModl = len(models)
            raise ValueError('Only a single model can be written to NAUTILUS input at the moment! Aborting!')
        # Open file for writing
        FileExists = os.path.isfile(filename)
        if FileExists and (mode == "a"):
            fdat = open(filename, "w")
            warnings.warn("Warning! Output file already exists and only single model can be written, overwriting!")
        else:
            fdat = open(filename, "w")
            fdat.write("! time    log(Av)    log(n)    log(T)   \n! (yr)   (mag)      (cm-3)    (K)       \n")
        # Start to write the models
        for m in models:
            for i in np.arange(m.nt):
                fdat.write('{:11.3E} '.format(m.time[i]))
                fdat.write('{:11.3E} '.format(np.log10(m.AvIS[i])))
                fdat.write('{:11.3E} '.format(np.log10(m.gdens[i])))
                fdat.write('{:11.3E} '.format(np.log10(m.Tg[i])))
                fdat.write('{:11.3E} '.format(np.log10(m.Td[i])))
                # New line
                fdat.write('\n')
        fdat.close()
        return filename + " was written!"

def plot_rad_dist(models, toplot='rho', pos0=[0.0,0.0,0.0], nt=-1,
            xlog=False, ylog=False, oplot=False, unit='au', marker='.',
            markevery=1, savefig=False, title=False, show=False, rrange=False,
            ylabel='$n_\mathrm{H}$ [cm$^{-2}$]', UseMax=False):
    '''
    Plots the radial distribution of physical quantities and chemical
    abundances from a chosen location (e.g. center of prestellar core).

    :param models: A list of ChemModl objects to be plotted as a function of /<r/>
    :type models: list
    :param toplot: string name of quantity to be plotted
    :type toplot: string
    :param pos0: xyz coordinates of the central position.
    :type pos0: array
    :param nt: index of snapshot/timestep to be plotted.
    :type nt: int
    :param unit: Unit of the distance (cm, au or pc)
    :type unit: string
    :param marker: Symbol to be plotted.
    :type marker: string
    :param savefig: If specified then the figure will be saved. If string then
                    this will be used as filename.
    :type savefig: bool or string
    :param rrange: Gives the plotting range [x0,x1,y0,y1].
    :type rrange: array
    :param ylabel: Gives the y-axis label.
    :type ylabel: string
    :param UseMax: Sets central location to the density peak.
    :type UseMax: bool
    '''
    mp = 1.6726e-24        # Mass of proton  [g]
    au = 1.4959800e+13
    pc = 3.0857200e+18
    nModels = len(models)
    if not oplot:
        plt.figure(figsize=(7,5), dpi=120)
    else:
        pass
    if not title:
        title = "Radial distribution of " + toplot
    # Determine the density maximum location if needed
    gdens = np.ndarray(nModels)
    for i in range(nModels):
        gdens[i] = models[i].gdens[nt]
    if UseMax:
        imax = gdens.argmax()
        pos0 = [models[imax].x[nt],models[imax].y[nt],models[imax].z[nt]]
    # Get the quantities to plot
    xarr = np.ndarray(nModels)
    yarr = np.ndarray(nModels)
    zarr = np.ndarray(nModels)
    rarr = np.ndarray(nModels)
    yaxis = np.ndarray(nModels)
    for i in range(nModels):
        xarr[i] = models[i].x[nt] - pos0[0]
        yarr[i] = models[i].y[nt] - pos0[1]
        zarr[i] = models[i].z[nt] - pos0[2]
        rarr[i] = np.sqrt(xarr[i]**2 + yarr[i]**2 + zarr[i]**2)
        if toplot == 'Tg':
            yaxis[i] = models[i].Tg[nt]
        elif toplot == 'Td':
            yaxis[i] = models[i].Td[nt]
        elif toplot == 'rho':
            yaxis[i] = models[i].gdens[nt]
        else:
            try:
                ind = models[i].spec.index(toplot)
                yaxis[i] = models[i].abuns[ind,nt]
            except:
                print( "Species/variable {} not found in model!".format(toplot) )
    if unit == 'au':
        rarr = rarr/au
    if unit == 'pc':
        rarr = rarr/pc
    else:
        pass
    # Plot the data
    plt.plot(rarr, yaxis, marker, markevery=markevery)
    plt.axis(rrange)
    if ylog:
        plt.yscale('log')
    if xlog:
        plt.xscale('log')
    plt.grid(True)
    plt.title(title)
    plt.xlabel('$r$ [au]')
    plt.ylabel(ylabel)
    #Finish
    if show:
        plt.show()
    if savefig != False:
        if type(savefig) != str:
            savefig = 'radial_profile'
        commons.saveFig(savefig,ext=figext)
    # Return some of the calculated data:
    return {'r':rarr, 'y': yaxis, 'pos0':pos0, 'gdens':gdens}

def plot_abuns_dist(models, quantity='z', species=['H2','H','H+','CO','C+','C'], 
                    nt=-1, tt=None, order=True, xscale='linear', yscale='log',
                    xlabel=None, oplot=False, figsize=(8,6), show=False,
                    plotphys=False, xlim=None, ylim=None):
    '''
    Plot abundances from a set of models as a function of spatial location 
    or (column) density.
    
    :param models: list containg ChemModl objects
    :param quantity: string specifying the physical parameter plotted on the 
                   absissa. Can be any array stored in ChemModl object. 
                   (Default is the x coordinate)
    :param species: list of string containing chemical species names to be plotted
    :param nt: integer specifying the plotted time step
    :param tt: specifies the plotted time in years (double precision number). If 
               set then nt parameter is ignored. Abundances at the closes time 
               step to tt will be plotted. (Default = None)
    :param order: if True then order the plotted data by increasing "quantity" 
               value
    :param xscale: sets the scaling of x coordinate (default = 'linear')
    :param yscale: sets the scaling of y coordinate (default = 'log')
    :param xlabel:
    :param oplot:
    :param figsize:
    :param show:
    '''
    mp = 1.6726e-24        # Mass of proton  [g]
    au = 1.4959800e+13
    pc = 3.0857200e+18
    
    nz = len(models)
    ns = len(species)
    z = np.zeros(nz)
    zloc = np.zeros(nz)
    abuns = np.zeros((nz,ns))
    gdens = np.zeros(nz)
    tgas  = np.zeros(nz)
        
    if tt:
        nt = np.abs(models[0].time - tt).argmin()
        
    for i in range(nz):
        #z[i] = models[i].z[0]
        zloc[i] = models[i].z[nt]
        gdens[i] = models[i].gdens[nt]
        tgas[i] = models[i].Tg[nt]
        if (quantity == 'coldens'):
            z[i] = models[i].gdens[nt]
        else:
            z[i] = eval("models[i].{}[nt]".format(quantity))
            
        for j in range(ns):
            isp = models[i].spec.index(species[j])
            abuns[i,j] = models[i].abuns[isp,nt]
    
    if order:
        order = z.argsort()
        z = z[order]
        abuns = abuns[order,:]
        zloc = zloc[order]
        gdens = gdens[order]
        tgas = tgas[order]
        
    if quantity == 'coldens':
        order = zloc.argsort()
        z = z[order]
        abuns = abuns[order,:]
        zloc = zloc[order]
        gdens = gdens[order]
        tgas = tgas[order]
        
        zloc = (zloc.max() - zloc)[::-1]
        z = z[::-1]
        tgas = tgas[::-1]
        gdens = gdens[::-1]
        abuns = abuns[::-1,:]
        #zloc = np.insert(zloc,-1,zloc[-1]+abs(zloc[-1]-zloc[-2]))
        z = np.cumsum( z* zloc)
        
    
    if quantity in ['x','y','z']:
        z = z / au
        if not xlabel:
            xlabel = "{} [au]".format(quantity)
    else:
        if not xlabel:
            xlabel = quantity
    
    if not oplot:
        fig = plt.figure(figsize=figsize)
        plt.grid()
    
    for j in range(ns):
        plt.plot(z, abuns[:,j], label=species[j], lw=2)
        plt.xscale(xscale)
        plt.yscale(yscale)
        plt.xlabel(xlabel)
        plt.ylabel('n$_{x}$/n$_\mathrm{H,tot}$')
        plt.title("r = {:.2E} yr".format(models[0].time[nt]))
        plt.xlim(xlim)
        plt.ylim(ylim)
        
    plt.legend(fontsize=14)
        
    if show:
        plt.show()
        
    if plotphys:
        plt.figure(figsize=figsize)
        plt.grid()
        plt.plot(z, tgas, lw=2)
        plt.xscale(xscale)
        plt.yscale(yscale)
        plt.xlabel(xlabel)
        plt.ylabel('Tgas [K]')
        plt.xlim(xlim)
        
    return {quantity:z, 'abuns':abuns, 'sorder':order, 'spec':species}
    
    
