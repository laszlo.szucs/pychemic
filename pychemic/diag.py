from __future__ import absolute_import
from __future__ import print_function

from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
import glob

from . import commons

def plot_column_map(fig, plto, map1, map2=None, lab1='', lab2='', 
            xlabel=None, ylabel=None, shrink=0.9,
           legloc=(0.8,0.7),species='CO'):
    '''
    Plots the column density map based on a FITS file.
    '''
    cmap = 'CMRmap_r'
    
    #matplotlib.rcParams.update({'font.size': 15})
    #plt.figure(figsize=(11,11), dpi=120)
    if not xlabel:
        xlabel = 'x [pc]'
    if not ylabel:
        ylabel = 'y [pc]'
    
    dmap = plto.imshow(map1, cmap=cmap, origin='lower', extent=[-0.7, 0.7, -0.7, 0.7])
    dmap.axes.xaxis.set_label_text(xlabel)
    dmap.axes.yaxis.set_label_text(ylabel)

    fig.colorbar(dmap, label=lab1, ax=plto, shrink=shrink)

    if map2.any():
        co_cont = plto.contour(map2, hold='on', colors='k',
            origin='lower', extent=[-0.7, 0.7, -0.7, 0.7])
        co_cont.collections[0].set_label(lab2)
        plt.clabel(co_cont, inline=1, fontsize=8, manual=False)
    
    plto.annotate(species,legloc,xycoords="axes fraction",
                  bbox={'facecolor':'white', 'alpha':0.9, 'pad':5},
                  fontsize=22)

    #lg = plt.legend(fontsize=18, loc=2)
    
def plot_map_diag( fits_dir, h2fits=None, legloc=(0.15,0.75), 
                   saveFig=False,
                   species=None ):
    '''
    Plots column density maps, stored in fits files in a given directory, on a 
    mcol * mrow grid.
    '''
    # parameters
    fig_w = 4.5             # width of single panel
    fig_h = 3.5             # hight of single panel
    mcol  = 4               # maximum number of columns
    mrow  = 5              # maximum number of rows, if larger start new file / plot
    
    fits_list_found = glob.glob(fits_dir+'/*.fits')
    imag_list_found = []
    spec_list_found = []
    for f in fits_list_found:
        hudlist = fits.open(f)
        imag_data = hudlist[0].data
        imag_list_found.append( imag_data )
        try:
            spec_name = hudlist[0].header['SPEC']
            spec_list_found.append( spec_name )
        except:
            spec_list_found.append( 'unknown' )
            print( "No SPEC header data found in {}.".format(f) )
    
    if species != None:
        imag_list = []
        spec_list = []
        for s in species:
            try:
                imag_list.append( imag_list_found[ spec_list_found.index(s) ] )
                spec_list.append( s )
            except:
                print( "INFO: species {} not found in fits folder.".format(s) )
                pass
    else:
        imag_list = imag_list_found
        spec_list = spec_list_found

    # Load the H2 column density file if given in argument.
    h2col = None
    lab2  = None
    if h2fits:
        try:
            h2col = fits.getdata(h2fits, ext=0)
            h2col = np.log10(h2col)
            lab2='log $N$(H$_{2}$) [cm$^{-2}$]'
        except:
            print( "INFO: no H2 column density file ({}) found!".format(h2fits) )
            pass
    
    # Determine the number of figures
    nr_maps = len(imag_list)
    
    nr_fig = (nr_maps / (mrow*mcol) ) + ( nr_maps%(mrow*mcol) ) / max([(nr_maps%(mrow*mcol)),1])
    ncol = []
    nrow = []
    
    # Plot the figures
    for i in range(nr_fig):
        
        nr_maps_prev = i * (mcol*mrow)
        nr_maps_curr = min( [(nr_maps - nr_maps_prev), mcol*mrow] )
        
        ncol = min( [mcol, nr_maps_curr] )
        if nr_maps_curr > mcol:
            nrwc = nr_maps_curr / mcol + (nr_maps_curr%mcol) /   \
                   max([nr_maps_curr%mcol,1])
            nrow = min( [mrow, nrwc] )
        else:
            nrow = 1
        
        fig, axes = plt.subplots( nrows=nrow, ncols=ncol, 
                                  figsize=(ncol*fig_w, nrow*fig_h) )
        
        plt.tight_layout()
        
        icol = 0
        irow = 0
        
        for j in range(nr_maps_prev, nr_maps_prev+nr_maps_curr):
            imag = imag_list[j]
            spec = spec_list[j]

            if nrow > 1:
                axc = axes[irow,icol]
            else:
                axc = axes[icol]

            if (irow+1) != nrow:
                xlabel = ' '
            else:
                xlabel = None
            if icol > 0:
                ylabel = ' '
            else:
                ylabel = None

            plot_column_map(fig, axc, imag, h2col, 
                            lab1='$N$({}) [cm$^{}$]'.format(spec,-2), 
                            lab2=lab2, species=spec, 
                            legloc=legloc, xlabel=xlabel, 
                            ylabel=ylabel, shrink=1.0)
            
            if (j-nr_maps_prev+1) != nr_maps_curr:
                if icol == (mcol-1):
                    irow = irow + 1
                    icol = 0
                else:
                    icol = icol + 1
    
        if (icol+1) != mcol and nrow > 1:
            for k in range(icol+1,mcol):
                    axes[irow,k].axis('off')
            
        if saveFig:
            if type(saveFig) == str:
                fbase = saveFig
            else:
                fbase = "Fig_comp"
            commons.saveFig("{}_{}".format(fbase,r+1),ext='pdf')
            
    return {'spec':spec_list, 'imag':imag_list}
        
