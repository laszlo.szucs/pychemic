from __future__ import absolute_import
from __future__ import print_function

from scipy.io import readsav
import numpy as np
import os
import glob

def write_asplash(ModelList=None,filename='asplash.dat',colname='columns',
                  CordRenorm=False,snapshot=-1,Speclist=
                  ["H2","CO","H2O","N2H+","NH3","HCN","HNC","CH3OH"],
                  verbose=False,savname="hw_cloud_1.2box.sav",savdata=None,
                  mode="w"):
    ''' 
    Writes a list of ChemModl models to a Splash (Price 2009) ascii file
    to be then interpolated to a regular grid. For this additional data is
    needed besides the chemical information: location (x,y,z), smoothing length
    (h), and ideally velocity (vx,vy,vz).
    '''
    
    # do tests first:
    nCell = len(ModelList)
    time = ModelList[0].time
    if (ModelList == None) or (nCell == 1):
        raise ValueError("A list of ChemModl object needs to be given as input!")
    # Set the conversion factors to Gadget internal units
    ulength = 1.0E17
    umass = 1.991E33
    uvel = 36447.3E0
    utime = 2.74368E12
    udens = 1.991E-18 #umass / ulength^3.
    # Renormalise coordinates if asked
    xCent = 0.0E0
    yCent = 0.0E0
    zCent = 0.0E0
    if CordRenorm:
        PartDens = np.zeros(nCell)
        for i in range(nCell):
            PartDens[i] = ModelList[i].gdens[snapshot]
        IndRhoMax = PartDens.argmax()
        xCent = ModelList[IndRhoMax].x[snapshot]
        yCent = ModelList[IndRhoMax].y[snapshot]
        zCent = ModelList[IndRhoMax].z[snapshot]
    # Find the index of selected species
    SpecIndex = []
    for ss in Speclist:
        try:
            tmpInd = ModelList[0].spec.index(ss)
            SpecIndex.append(tmpInd)
        except:
            print( "{} not found in the input chemical models, skiping it!".format(ss) )
            SpecIndex.remove(ss)
    # get addidional information about the particles (vx,vy,vz,h)
    if type(savdata) != np.recarray:
        if not os.path.isfile(savname):
            raise ValueError(savname+" does not exist, but needed to proceed!")
        # continue if found
        print("Reading Gadget IDL save from '%s'..." % savname),
        PartData = readsav(savname,verbose=verbose).tracedpart # this is np.recarray
        print("Done")
    else:
        PartData = savdata
    IDL_IDs = [int(i) for i in PartData.ID.tolist()]
    IDL_time = PartData.TIME[0]  # time from the first SPH particle, should be the same for all
    IDL_pos = PartData.POS.tolist()
    IDL_vel = PartData.vel.tolist()
    IDL_rho = PartData.rho.tolist()
    IDL_h = PartData.H.tolist()
    IDL_mass = PartData.MASS.tolist()
    IDL_Aveff = PartData.aveff.tolist()  # Angle average Av from raytracing (HEALPIX)
    IDL_Avtot = PartData.totav.tolist()  # Angle averaged Av + local Av contribution
    #now write the ascii file!
    # check if output file exists:
    FileExists = os.path.isfile(filename)
    if FileExists and (mode == "a"):
        fdat = open(filename, mode)
        fcol = open(colname, "w")
    else:
        fdat = open(filename, "w")
        fcol = open(colname, "w")
        fdat.write('{:11.4E}\n'.format(time[snapshot]))
        fdat.write('{:8d}  {:8d}  {:8d}  {:8d}  {:8d}  {:8d}\n'.format(nCell,
                                                                       0,0,0,
                                                                       0,0))
    for i in range(nCell):
        # find the current particle in the Gadget IDL save data
        IDL_loc = IDL_IDs.index(ModelList[i].ID)
        pos = IDL_pos[IDL_loc][(snapshot-1)]
        vel = IDL_vel[IDL_loc][(snapshot-1)]
        h = IDL_h[IDL_loc][(snapshot-1)]
        mass = IDL_mass[IDL_loc][(snapshot-1)]
        rho = IDL_rho[IDL_loc][(snapshot-1)]
        aveff = IDL_Aveff[IDL_loc][(snapshot-1)]
        avtot = IDL_Avtot[IDL_loc][(snapshot-1)]
        # write the location
        fdat.write( '{:11.4E}  {:11.4E}  {:11.4E}  '.format(   # x, y, z
            (pos[0]-xCent)/ulength,
            (pos[1]-yCent)/ulength,
            (pos[2]-zCent)/ulength ) )
        if not fcol.closed: fcol.write('x\ny\nz\n')
        # write the velocities
        fdat.write( '{:11.4E}  {:11.4E}  {:11.4E}  '.format(   # x, y, z
            (vel[0]-xCent)/uvel,
            (vel[1]-yCent)/uvel,
            (vel[2]-zCent)/uvel ) )
        if not fcol.closed: fcol.write("vx\nvy\nvz\n")
        # write the particle mass, density and temperatures
        fdat.write( '{:11.4E}  '.format(
            mass/umass ))         # particle mass
        if not fcol.closed: fcol.write('particle mass\n')
        fdat.write( '{:11.4E}  {:11.4E}  {:11.4E}  '.format(
            ModelList[i].rho[snapshot]/udens,   # density
            ModelList[i].Tg[snapshot],          # gas temperature
            ModelList[i].Td[snapshot] ))         # dust temperature
        if not fcol.closed: fcol.write('density\nTgas\nTdust\n')
        # write the smoothing length
        fdat.write( '{:11.4E}  '.format(
            h/ulength ))         # smoothing length
        if not fcol.closed: fcol.write('h\n')
        fdat.write( '{:11.4E}  {:11.4E}  '.format( aveff, avtot ) )
        if not fcol.closed: fcol.write('Aveff\nAvTot\n')
        # finally, write the chemical species * density (i.e. number density)
        for jj in range(len(SpecIndex)):
            fdat.write( '{:11.4E}  '.format(
                ModelList[i].abuns[SpecIndex[jj],snapshot]*
                ModelList[i].gdens[snapshot]) )
            if not fcol.closed: fcol.write(Speclist[jj]+"\n")
        fdat.write("\n")            # get to the next particle
        if not fcol.closed: fcol.close()   # all columns are set, close the file

    fdat.close()
    # free up memory
    del IDL_IDs, IDL_time, IDL_pos, IDL_vel, IDL_rho, IDL_h, IDL_mass, ModelList
    
    return
    

def alchemic_to_asplash(al_dir, snap_file, nt=174, spec_list=None, asp_name=None, 
                        colname=None, verbose=False):
    ''' 
    Combines a set of ALCHEMIC_SPH binary outputs with a simulation snapshot 
    to produce an ASPLASH ascii input file. The SPH data and the number density
    of chemical species is stored in the columns of the output file.
    
    ASPLASH can then be used to visualise or to interpolate the SPH data to 
    regular grid.
    '''
    # Get ALCHEMIC_SPH output file names
    if not os.path.isdir(al_dir):
        raise ValueError('Error: {} directory does not exist!'.format(al_dir))
    
    ResModel = glob.glob(al_dir+"/*.bout")
    ResModel.sort()
    SpecFile = glob.glob(al_dir+"/*.dat")
    SpecFile.sort()
    
    if len(ResModel) == 0:
        raise ValueError('Error: {} directory does not contain *.bout files!'.format(al_dir))
    elif verbose:
        print( "INFO: files found in {} directory:\n".format(al_dir) )
        for s in ResModel:
            print( s.replace(al_dir+'/', '') )
    
    # read the SPH IDL save data once then provide it to the asplash writing routine
    if not snap_file:
        snap_file = '../../hwcloud_nt180.sav'
    PartData = readsav(snap_file,verbose=verbose).tracedpart
    
    # Set default species if not specified by user
    if not spec_list:
        spec_list = ["H","H2","CO","C","C+","HCO+",'ELECTR']
    
    # Set default output name
    if not asp_name:
        asp_name = 'asplash_ALCHEMIC_SPH_n{}.dat'.format(nt)
    
    if not colname:
        colname = 'columns'
        
    # Read models and output to ASPLASH ascii file
    for i in range(len(ResModel)):
        tmp = model.read_alchemic(filename=ResModel[i], specfile=SpecFile[0], 
                                  verbose=verbose)
        # in case of static models nt=173 is the equivalent of nt=174 in 
        # the dynamic models
        if tmp[0].nt <= nt:
            nt_c = tmp[0].nt-1
        else:
            nt_c = nt
        write_asplash(ModelList=tmp,savdata=PartData,filename=asp_name,
                      colname=colname,Speclist=spec_list,mode='a',
                      snapshot=nt_c,verbose=verbose)
        del tmp
        
    if verbose:
        print( "INFO: all done and {} is written!".format(asp_name) )
        
    return
