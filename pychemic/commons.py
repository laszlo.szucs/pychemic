"""
al_commons contains functions used in a number of PyChemic modules.
"""
from __future__ import absolute_import
from __future__ import print_function

import numpy as np
import warnings
import os
import matplotlib.pyplot as plt

__all__ = ["readIDLlikeArrayFromFile","add2nparr","chargenumber","exactmatch",
           "sortbynparr","saveFig","atom_count","AvgAv"]

# TODO: replace readIDLlikeArrayFromFile() with below new code
#def readIDLlikeArrayFromFile(file, nx=0, ny=1, perline=10, dtype=np.double,
                             #ElemLength=0):
    #""" Reads a chunk of an ascii text file into a 1 or 2 dim. numpy array.

    #:param file: file handle to be read from (the file needs to be oened
                 #first)
    #:type file: ...
    #:param nx: number of elements in the first dimension (default 0)
    #:type nx: int
    #:param ny: number of elements in the second dimension (default 1)
    #:type ny: int
    #:param perline: number of records per line in the ascii file (default 10)
    #:type perline: int
    #:param dtype: type of the returned array (default np.double)
    #:type dtype: Python type name (str, int, float or double)
    #:param ElemLength: length of single record when string array is read
            #(default 0)
    #:type ElemLength: int

    #:returns: A numpy array with a type defined by the dtype argument. The array
        #can be one or two dimensional.

    #:warnings: If the ascii file would be read over the end of the file.
    #"""
    ## find out how many lines needs to be read to have the requested array
    ## length.This is controlled by the perline variable. This might not be
    ## always 10!
    #nlines = (nx * ny) // perline
    #reminder = (nx * ny) % perline
    #if reminder > 0:
        #nlines = nlines + 1
    ## The data will be stored in the StrTmp string
    #StrList = []

    #for i in range(nlines):
        #CurrentLine = file.readline().replace("\n", "")
        #print (CurrentLine, len(CurrentLine))
        #if (CurrentLine == ""):
            #warnings.warn("Warning! Reading longer than the file's end.")
        #StrList.append(CurrentLine)

    #if (dtype == np.double) or (dtype == np.float):
        
        #StrTmp = "".join(StrList)
        #StrTmp = StrTmp.replace("D", "E")
        
        #array = np.frombuffer(StrTmp, dtype=np.dtype("S12"))
        #array = np.array(array, dtype=dtype)

    #else:
        
        #StrTmp = "".join(StrList)
        #array = np.frombuffer(StrTmp, dtype=("S12")).tolist()
        
        #for i in range( len(array) ):
            #array[i] = array[i].decode('UTF-8').strip()

    #if (ny > 1):
        #array = array.reshape(nx, ny)

    #return array

def readIDLlikeArrayFromFile(file, nx=0, ny=1, perline=10, dtype=np.double,
                             ElemLength=0):
    """ Reads a chunk of an ascii text file into a 1 or 2 dim. numpy array.

    :param file: file handle to be read from (the file needs to be oened
                 first)
    :type file: ...
    :param nx: number of elements in the first dimension (default 0)
    :type nx: int
    :param ny: number of elements in the second dimension (default 1)
    :type ny: int
    :param perline: number of records per line in the ascii file (default 10)
    :type perline: int
    :param dtype: type of the returned array (default np.double)
    :type dtype: Python type name (str, int, float or double)
    :param ElemLength: length of single record when string array is read
            (default 0)
    :type ElemLength: int

    :returns: A numpy array with a type defined by the dtype argument. The array
        can be one or two dimensional.

    :warnings: If the ascii file would be read over the end of the file.
    """
    # find out how many lines needs to be read to have the requested array
    # length.This is controlled by the perline variable. This might not be
    # always 10!
    nlines = (nx * ny) // perline
    reminder = (nx * ny) % perline
    if reminder > 0:
        nlines = nlines + 1
    # The data will be stored in the StrTmp string, by adding up all the read
    # lines.
    StrList = []
    # Start reading in the matrix (array) line by line (j index stands for
    # line, i index for column)
    for i in range(nlines):
        CurrentLine = file.readline()
        if (CurrentLine == ""):
            warnings.warn("Warning! Reading longer than the file's end.")
        StrList.append(CurrentLine)
    StrTmp = " ".join(StrList)
    StrTmp = StrTmp.replace("\n", " ")
    if (dtype == np.double) or (dtype == np.float):
        try:
            StrTmp = StrTmp.replace("D", "E")
            array = np.array(StrTmp.split(), dtype=dtype)
        except:
            StrTmp = StrTmp.replace("D", "E")
            StrTmpArr = []
            if ElemLength == 0:
                ElemLength = 11 #len(StrTmp) / float(nx * ny)
            print ("---Reading with alternative method, using " +
                str(ElemLength).strip() + " characters per element!")
            start = 1
            for jj in range(nx * ny):
                StrTmpElem = StrTmp[start:(start + ElemLength)]
                if (jj > 0) and ( (jj + 1) % 10 == 0):
                    start = start + ElemLength + 3
                else:
                    start = start + ElemLength + 1
                if ('E' not in StrTmpElem):
                    StrTmpArr.append('0.00')
                else:
                    StrTmpArr.append(StrTmpElem)
            array = np.array(StrTmpArr, dtype=dtype)
    else:
       chunks, chunk_size = len(StrTmp), len(StrTmp) // (nx - 1)
       array = [ StrTmp[i:(i + chunk_size)] for i in range(0, chunks,
                chunk_size) ]
       for i in range( len(array) ):
           array[i] = array[i].strip()
    if (ny > 1):
        array = array.reshape(nx, ny)
    return array

def add2nparr(array, newelement):
    """ Utility function to add new elements to a numpy array.

    The procedure involves resizing the array and assigning the new element
    to the given value.

    :param array: numpy array to be extended
    :type array: numpy.array
    :param newelement: element to be added to the numpy array
    :type newelement: str, float or double
    :returns: *numpy.ndarray* -- the imput array extended with a single
            element at end location.
    :raises: TypeError
    """
    if "numpy" in str(type(array)):
        CurrentLength = len(array)
        array.resize(CurrentLength + 1, refcheck=False)
        array[CurrentLength] = newelement
        return array
    else:
        raise TypeError("Error in add2nparr(): not numpy array, type:" +
                        str(type(array)))

def chargenumber(SpecToFind):
    """ This function calculates the charge number from a given species.

    The species name is given as argument. The return value is the sum of
    charges found in the species (name). Negative charges indicated by "-",
    positive charges by "+". Up to doubly ionised species are recognised.
    In some cases linear and cyclic molecules are differentiated by "l-" and
    "c-". The minus signs after "l" and "c" are ignored.

    :param SpecToFind: the charge number of this species is determined.
    :type SpecToFind: str

    :returns: *int* -- positive, neutral or neagtive charge number of the species
    """
    chargeplus = 0
    chargeminus = 0
    SpecToFind = SpecToFind.replace("l-","")
    SpecToFind = SpecToFind.replace("c-","")
    if ("++" in SpecToFind):
        chargeplus = chargeplus + 2
    elif ("+" in SpecToFind):
        chargeplus = chargeplus + 1
    if ("--" in SpecToFind):
        chargeminus = chargeminus + 2
    elif ("-" in SpecToFind) or ("ELECTR" in SpecToFind):
        chargeminus = chargeminus + 1
    return chargeplus - chargeminus

def exactmatch(chararray, char):
    """ This function searches for matching string in a numpy.char array.

    At the array locations where exact match is found the resulting integer
    array is set to 0. At any other locations the integer is set to -1.

    :param chararray: character array to be searched
    :type chararray: numpy.chararray
    :param char: string to be found in the array
    :type char: str

    :returns: *numpy.intarr* -- The values in the array might either be zero or
        -1. Zeros indicating the location in the input array where exact match
        has been found.
    """
    # initialize match indicator array
    indicator = np.zeros(len(chararray), dtype=np.int32) - 1
    for i in range( len(chararray) ):
        if (chararray[i] == char):
           indicator[i] = 0
    return indicator

def sortbynparr(lista, indarr, reverse=False):
    """
    This function returns a sorted list by the values in an integer array.

    This is handy for sorting the reaction strings (reactant and product names)
    by their rates. The number of elements of the list and the array does not
    have to match.

    :param lista: list object to be sorted, normaly containing strings
    :type lista: list
    :param indarr: indarr numpy integer array containing the sorted indexes
    :type indarr: numpy.array
    :param reverse: reverse if set the indarr is reversed (default False)
    :type reverse: boolean

    :returns: *list* -- List sorted according the indexes given in indarr argument.
    """
    listaCp = []
#    if len(lista) != len(indarr):
#        print "Warning: len(lista) is not equal len(indarr): " + str(len(lista)) + "!=" + str(len(indarr))
#        print "Is this correct?"
    if reverse:
        indarr = indarr[::-1]
    for i in range(len(indarr)):
        listaCp.append(lista[indarr[i]])
    return listaCp

def saveFig(path, ext='png', close=True, verbose=True, transparent=True):
    """
    Save a figure from pyplot to a specified file format.

    :param path: The path (and filename, without extension) to save the figure to.
    :type path: string
    :param ext: The file extension (default='png'). This must be supported by
                the active matplotlib backend (see matplotlib.backends module).
                Most backends support 'png', 'pdf', 'ps', 'eps', and 'svg'.
    :type ext: string
    :param close: Whether to close the figure after saving (default=True).
                  If you want to save the figure multiple times (e.g., to
                  multiple formats), you should NOT close it in between
                  saves or you will have to re-plot it.
    :type close: boolean
    :param verbose: Whether to print information about when and where the image
                    has been saved (default=True).
    :type verbose: boolean
    """

    # Extract the directory and filename from the given path
    directory = os.path.split(path)[0]
    filename = "%s.%s" % (os.path.split(path)[1], ext)
    if directory == '':
        directory = '.'

    # If the directory does not exist, create it
    if not os.path.exists(directory):
        os.makedirs(directory)

    # The final path to save to
    savepath = os.path.join(directory, filename)

    if verbose:
        print("Saving figure to '%s'..." % savepath),

    # Actually save the figure
    plt.savefig(savepath,bbox_inches='tight',transparent=transparent)

    # Close it
    if close:
        plt.close()

    if verbose:
        print("Done")

def atom_count(tmpSpec):
    """ For each species in the reactions, calculate the atom number.

    This is necessary when reducing the chemical network by molecule mass.
    """
    # Initialization of atom's names:
    Atoms1C = ['H','C','N','O','S','P','D']
    mass1C  = [1.00000E+00,1.20110E+01,1.40067E+01,1.59994E+01,3.20660E+01,
               3.09738E+01,2.00000E+00]
    Atoms2C = ['HE','FE','SI','NA','MG','CL']
    mass2C  = [4.00260E-00,5.58470E+01,2.80855E+01,2.29898E+01,2.43050E+01,
               3.54527E+01]

    # Remove 'g' and ortho and para states and the mantel 'm' species?
    tmpSpec = tmpSpec.replace('g','')
    tmpSpec = tmpSpec.replace('o','')
    tmpSpec = tmpSpec.replace('p','')
    tmpSpec = tmpSpec.replace('m','')
    # Remove '-' and '+' ionization states
    tmpSpec = tmpSpec.replace('-','')
    tmpSpec = tmpSpec.replace('+','')
    # Take care of the PHOTONS and ELECTR
    if tmpSpec == 'PHOTON':
        tmpMass = 0.
        tmpSpec = ''
        tmpAtms = 0
    if tmpSpec == 'ELECTR':
        tmpMass = 5.4462409e-07
        tmpSpec = ''
        tmpAtms = 0
    if tmpSpec == 'CRPHOT':
        tmpMass = 0.
        tmpSpec = ''
        tmpAtms = 0
    tmpMass = 0.
    tmpAtms = 0
    # do it for the two letter names
    for s2i in range( len(Atoms2C) ):
        while Atoms2C[s2i] in tmpSpec:
            strindex = tmpSpec.find(Atoms2C[s2i])
            if ( strindex >= 0 ):
                # check if mulitple atoms?
                multipl = 1.
                ToRemove = 0
                if strindex+2 <= len(tmpSpec)-1:
                    if tmpSpec[strindex+2].isdigit() :
                        multipl = float(tmpSpec[strindex+2])
                        ToRemove = 1
                # check if 2 digits
                if strindex+3 <= len(tmpSpec)-1:
                    if tmpSpec[strindex+2:strindex+3].isdigit() :
                        multipl = float(tmpSpec[strindex+2:strindex+3])
                        ToRemove = 2
                if ToRemove > 0:
                    tmpSpec = tmpSpec[:strindex+2] + tmpSpec[(strindex+2+ToRemove):]
                # add up the mass, remove the atom from the string
                tmpMass = tmpMass + ( multipl * mass2C[s2i] )
                tmpSpec = tmpSpec[:strindex] + tmpSpec[(strindex+2):]
                tmpAtms = tmpAtms + multipl
    # do it for the one letter names
    for s1i in range( len(Atoms1C) ):
        while Atoms1C[s1i] in tmpSpec:
            strindex = tmpSpec.find(Atoms1C[s1i])
            if ( strindex >= 0 ):
                # check if mulitple atoms?
                multipl = 1.
                ToRemove = 0
                if strindex+1 <= len(tmpSpec)-1:
                    if tmpSpec[strindex+1].isdigit() :
                        multipl = float(tmpSpec[strindex+1])
                        ToRemove = 1
                        # check if 2 digits
                if strindex+2 <= len(tmpSpec)-1:
                    if tmpSpec[strindex+1:strindex+2+1].isdigit() :
                        multipl = float(tmpSpec[strindex+1:strindex+1+2])
                        ToRemove = 2
                if ToRemove > 0:
                    tmpSpec = tmpSpec[:strindex+1] + tmpSpec[(strindex+1+ToRemove):]
                # add up the mass
                tmpMass = tmpMass + ( multipl * mass1C[s1i] )
                tmpSpec = tmpSpec[:strindex] + tmpSpec[(strindex+1):]
                tmpAtms = tmpAtms + multipl

    return {'amass': tmpMass, 'natm': tmpAtms}

def AvgAv(array, npix=48, gamma=-2.5):
    ''' Calculates the effective visual exintction from an array of
    HEALPIX extinction values.
    '''
    # Check if array has npix number of elements
    if len(array) == npix:
        AvMean = np.log((np.exp(gamma*array)).sum() / float(npix)) / gamma
    else:
        print( "Error in AvgAv(): {} != {}".format(len(array),npix) )
        AvMean = None
    return AvMean
