"""
This module is responsible for reading, editing and writing chemical networks.

Functions and class definitions for the Ortho/Para and deuteration chemical
network used for the chemical clock testing project. The network is from Oli
(Sipila 2015).

Here we are dealing with conversion of the network to the ALCHEMIC format.
This involves changing notations for species and adding explicitly FREEZE and
DESORB keywords/processes to the network.

Example:

1) Building the Sipila (2016) network from scratch. (Can be also found in the
_network/build folder).::

    import pychemic as ntw
    # readin the raw network files
    kida=ntw.read_network(input_file="kida.uva.2014", skiplines=0)
    upds = ntw.read_network(input_file="updates_legal2014.dat", skiplines=2)
    # updating kida with the upds network
    kida.updaterates(upds, eps=1e-60)
    # adding grain surface chemistry
    surfchem = ntw.add_grain(kida,grain_file="ir.15_grain_corrected",
                        adsorp_file='adsorp.energy_2015',AddAdsDes=True,
                        AddCRPH=False,AddGr=True,AddAll=True)
    # combining and writing network
    sp15 = kida + surfchem
    sp15.write2alchemic(networkfile="rreactions_kida2014UpdGrS.dat",
                        specfile="rspecies_kida2014UpdGrS.dat")


"""
from __future__ import absolute_import
from __future__ import print_function

import numpy as np
import os
import copy

from collections import OrderedDict

from . import al_commons


class network:
    """ Network structure containing species, reaction coefficients, metadata.

    We need reactants, products, 3 coefficients and some space for potential
    comments.
    """
    def __init__(self, index, r1, r2, p1, p2, p3, p4, p5, alpha, beta, gamma,
                 Tmin, Tmax, rtype, formul, multiform, comment):
        self.index = index
        self.nre = len(index)
        self.r1 = r1
        self.r2 = r2
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.p4 = p4
        self.p5 = p5
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma
        self.comment = comment
        self.Tmin = Tmin
        self.Tmax = Tmax
        # Additional data, calculated from the above data
        # 1) find the uniq species
        TMPspecies = set( np.concatenate([self.r1, self.r2, self.p1, self.p2,
                                          self.p3, self.p4, self.p5], axis=0) )

        self.species = list( TMPspecies )
        # 2) Convert to ALCHEMIC nomenclature
        self.grain2alchemic()
        self.amass = np.zeros( len(self.species) )
        self.anum  = np.zeros( len(self.species) )
        self.count_atoms()
        # 3) Set the reaction type according OSU (or KIDA)
        self.rtype  = rtype   #np.zeros( len(self.r1) )
        self.formul = formul
        self.multiform = multiform
        self.ndpl = self.findDuplicate()

    def findDuplicate(self):
        ndpl = np.zeros_like(self.multiform)
        for i in range(self.nre):
            ri1 = (self.r1[i] == np.array(self.r1))
            ri2 = (self.r2[i] == np.array(self.r2))
            pi1 = (self.p1[i] == np.array(self.p1))
            pi2 = (self.p2[i] == np.array(self.p2))
            pi3 = (self.p3[i] == np.array(self.p3))
            pi4 = (self.p4[i] == np.array(self.p4))
            pi5 = (self.p5[i] == np.array(self.p5))

            match = ri1 & ri2 & pi1 & pi2 & pi3 & pi4 & pi5
            ndpl[i] = len(np.argwhere(match == True))
        return ndpl

    def grain2alchemic(self):
        """ Function to convert the notation of the chemical network

        Function to convert the {species}* notation for grain surface species
        that Oli is using to g{species} notation that we are using in ALCHEMIC

        Conversions to be done:

               {species}*  -->  g{species} \n
                e-         -->  ELECTR \n
                g{-/+/0}   -->  G{-/+/0} \n
        """
        for i in range( len( self.r1 ) ):
            # Strip the witespaces
            self.r1[i] = self.r1[i].strip()
            self.r2[i] = self.r2[i].strip()
            self.p1[i] = self.p1[i].strip()
            self.p2[i] = self.p2[i].strip()
            self.p3[i] = self.p3[i].strip()
            self.p4[i] = self.p4[i].strip()
            self.p5[i] = self.p5[i].strip()
            # Work with the grain surface species names:
            #if ( '*' in self.r1[i] ):
                #self.r1[i] = 'g' + self.r1[i].replace('*','')
            #if ( '*' in self.r2[i] ):
                #self.r2[i] = 'g' + self.r2[i].replace('*','')
            #if ( '*' in self.p1[i] ):
                #self.p1[i] = 'g' + self.p1[i].replace('*','')
            #if ( self.p2[i] != '' ):
                #if ( '*' in self.p2[i] ):
                    #self.p2[i] = 'g' + self.p2[i].replace('*','')
            #if ( self.p3[i] != '' ):
                #if ( '*' in self.p3[i] ):
                    #self.p3[i] = 'g' + self.p3[i].replace('*','')
            #if (self.p4[i] != ''):
                #if ( '*' in self.p4[i] ):
                    #self.p4[i] = 'g' + self.p4[i].replace('*','')
            #if (self.p5[i] != ''):
                #if ( '*' in self.p5[i] ):
                    #self.p5[i] = 'g' + self.p5[i].replace('*','')
            # Change to ELECTR notation
            if ( 'e-' in self.r1[i] ):
                self.r1[i] = self.r1[i].replace('e-','ELECTR')
            if ( 'e-' in self.r2[i] ):
                self.r2[i] = self.r2[i].replace('e-','ELECTR')
            if ( 'e-' in self.p1[i] ):
                self.p1[i] = self.p1[i].replace('e-','ELECTR')
            if ( self.p2[i] != '' ):
                if ( 'e-' in self.p2[i] ):
                    self.p2[i] = self.p2[i].replace('e-','ELECTR')
            if ( self.p3[i] != '' ):
                if ( 'e-' in self.p3[i] ):
                    self.p3[i] = self.p3[i].replace('e-','ELECTR')
            if (self.p4[i] != ''):
                if ( 'e-' in self.p4[i] ):
                    self.p4[i] = self.p4[i].replace('e-','ELECTR')
            if (self.p5[i] != ''):
                if ( 'e-' in self.p5[i] ):
                    self.p5[i] = self.p5[i].replace('e-','ELECTR')
            # Convert the grains with different charges (G0,G+,G-)
            if ( self.r1[i] == 'g'):
                self.r1[i] = 'G0'
            if ( self.r1[i] == 'g+'):
                self.r1[i] = 'G+'
            if ( self.r1[i] == 'g-'):
                self.r1[i] = 'G-'

            if ( self.r2[i] == 'g'):
                self.r2[i] = 'G0'
            if ( self.r2[i] == 'g+'):
                self.r2[i] = 'G+'
            if ( self.r2[i] == 'g-'):
                self.r2[i] = 'G-'

            if ( self.p1[i] == 'g'):
                self.p1[i] = 'G0'
            if ( self.p1[i] == 'g+'):
                self.p1[i] = 'G+'
            if ( self.p1[i] == 'g-'):
                self.p1[i] = 'G-'

            if ( self.p2[i] == 'g'):
                self.p2[i] = 'G0'
            if ( self.p2[i] == 'g+'):
                self.p2[i] = 'G+'
            if ( self.p2[i] == 'g-'):
                self.p2[i] = 'G-'

            if ( self.p3[i] == 'g'):
                self.p3[i] = 'G0'
            if ( self.p3[i] == 'g+'):
                self.p3[i] = 'G+'
            if ( self.p3[i] == 'g-'):
                self.p3[i] = 'G-'

            if ( self.p4[i] == 'g'):
                self.p4[i] = 'G0'
            if ( self.p4[i] == 'g+'):
                self.p4[i] = 'G+'
            if ( self.p4[i] == 'g-'):
                self.p4[i] = 'G-'

            if ( self.p5[i] == 'g'):
                self.p5[i] = 'G0'
            if ( self.p5[i] == 'g+'):
                self.p5[i] = 'G+'
            if ( self.p5[i] == 'g-'):
                self.p5[i] = 'G-'
            # Convert the {Mg} notation to {MG} to avoid degeneracy of the {g}
            # (i.e. grain species)
            self.r1[i] = self.r1[i].replace('Mg','MG')
            self.r2[i] = self.r2[i].replace('Mg','MG')
            self.p1[i] = self.p1[i].replace('Mg','MG')
            self.p2[i] = self.p2[i].replace('Mg','MG')
            self.p3[i] = self.p3[i].replace('Mg','MG')
            self.p4[i] = self.p4[i].replace('Mg','MG')
            self.p5[i] = self.p5[i].replace('Mg','MG')

            self.r1[i] = self.r1[i].replace('He','HE')
            self.r2[i] = self.r2[i].replace('He','HE')
            self.p1[i] = self.p1[i].replace('He','HE')
            self.p2[i] = self.p2[i].replace('He','HE')
            self.p3[i] = self.p3[i].replace('He','HE')
            self.p4[i] = self.p4[i].replace('He','HE')
            self.p5[i] = self.p5[i].replace('He','HE')

            self.r1[i] = self.r1[i].replace('Fe','FE')
            self.r2[i] = self.r2[i].replace('Fe','FE')
            self.p1[i] = self.p1[i].replace('Fe','FE')
            self.p2[i] = self.p2[i].replace('Fe','FE')
            self.p3[i] = self.p3[i].replace('Fe','FE')
            self.p4[i] = self.p4[i].replace('Fe','FE')
            self.p5[i] = self.p5[i].replace('Fe','FE')

            self.r1[i] = self.r1[i].replace('Si','SI')
            self.r2[i] = self.r2[i].replace('Si','SI')
            self.p1[i] = self.p1[i].replace('Si','SI')
            self.p2[i] = self.p2[i].replace('Si','SI')
            self.p3[i] = self.p3[i].replace('Si','SI')
            self.p4[i] = self.p4[i].replace('Si','SI')
            self.p5[i] = self.p5[i].replace('Si','SI')

            self.r1[i] = self.r1[i].replace('Na','NA')
            self.r2[i] = self.r2[i].replace('Na','NA')
            self.p1[i] = self.p1[i].replace('Na','NA')
            self.p2[i] = self.p2[i].replace('Na','NA')
            self.p3[i] = self.p3[i].replace('Na','NA')
            self.p4[i] = self.p4[i].replace('Na','NA')
            self.p5[i] = self.p5[i].replace('Na','NA')

            self.r1[i] = self.r1[i].replace('Cl','CL')
            self.r2[i] = self.r2[i].replace('Cl','CL')
            self.p1[i] = self.p1[i].replace('Cl','CL')
            self.p2[i] = self.p2[i].replace('Cl','CL')
            self.p3[i] = self.p3[i].replace('Cl','CL')
            self.p4[i] = self.p4[i].replace('Cl','CL')
            self.p5[i] = self.p5[i].replace('Cl','CL')
            # Convert the KIDA notation to what we use:
            self.r1[i] = self.r1[i].replace('GRAIN','G')
            self.r2[i] = self.r2[i].replace('GRAIN','G')
            self.p1[i] = self.p1[i].replace('GRAIN','G')
            self.p2[i] = self.p2[i].replace('GRAIN','G')
            self.p3[i] = self.p3[i].replace('GRAIN','G')
            self.p4[i] = self.p4[i].replace('GRAIN','G')
            self.p5[i] = self.p5[i].replace('GRAIN','G')
            
            if self.r1[i] != 'XRAYS':
                self.r1[i] = self.r1[i].replace('X','g')
            if self.r2[i] != 'XRAYS':
                self.r2[i] = self.r2[i].replace('X','g')
            # XRAYS can not be produced, thus:
            self.p1[i] = self.p1[i].replace('X','g')
            self.p2[i] = self.p2[i].replace('X','g')
            self.p3[i] = self.p3[i].replace('X','g')
            self.p4[i] = self.p4[i].replace('X','g')
            self.p5[i] = self.p5[i].replace('X','g')
            # The following is an ugly fix for the case when chemical network added up
            # and grain2alchemic() is called the second time and the network is KIDA 
            # based.
            # TODO: better fix is to strictly call grain2alchemic() once
            if 'kida' in self.comment.lower() and '|' not in self.comment.lower():
                if self.r1[i] == 'CRP':
                    self.r1[i] = 'CRPHOT'
                if self.r2[i] == 'CRP':
                    self.r2[i] = 'CRPHOT'
            #Only the first or the second reactant:
            if self.r1[i] == 'CR':
                self.r1[i] = 'CRP'
            if self.r2[i] == 'CR':
                self.r2[i] = 'CRP'
            if self.r1[i] == 'Photon':
                self.r1[i] = 'PHOTON'
            if self.r2[i] == 'Photon':
                self.r2[i] = 'PHOTON'
        # TODO don't allow PHOTON product, while it is not followed
            self.p1[i] = self.p1[i].replace('Photon','')
            self.p2[i] = self.p2[i].replace('Photon','')
            self.p4[i] = self.p4[i].replace('Photon','')
            self.p3[i] = self.p3[i].replace('Photon','')
            self.p5[i] = self.p5[i].replace('Photon','')

        TMPspecies = set( np.concatenate([self.r1, self.r2, self.p1, self.p2,
                                          self.p3, self.p4, self.p5],axis=0) )
        # Discard "species" that are present in the network but not followed
        TMPspecies.discard('')
        TMPspecies.discard('           ')
        TMPspecies.discard('FREEZE')
        TMPspecies.discard('PHOTON')
        TMPspecies.discard('DESORB')
        TMPspecies.discard('CRPHOT')
        TMPspecies.discard('CRP')
        TMPspecies.discard('XRAYS')
        # Create a species list
        self.species = list( TMPspecies )
        self.species.sort()
        # Return some information to the screen
        print( 'Converting to ALCHEMIC nomenclature:' )
        print( '{species}* notation --> g{species} notation' )
        print( '{e-} notation --> {ELECTR} notation' )
        print( '{He,Fe,Mg,Si,Cl,Na} notation --> {HE,FE,MG,SI,CL,NA} notation' )
        print( 'Done!\n' )

    def __add__(self, other):
        """ Adds two networks element wise (all elments of the input are kept).

        Create a network (r1, r2, p1...p4, alpha, beta, gamma) which combines
        the gas-phase and grain surface reactions.
        """
        if other.index[0] == 1:
           other.index = max(self.index) + other.index
        new_network = network(np.concatenate([self.index,other.index],axis=0),
                              np.concatenate([self.r1,other.r1],axis=0),
                              np.concatenate([self.r2,other.r2],axis=0),
                              np.concatenate([self.p1,other.p1],axis=0),
                              np.concatenate([self.p2,other.p2],axis=0),
                              np.concatenate([self.p3,other.p3],axis=0),
                              np.concatenate([self.p4,other.p4],axis=0),
                              np.concatenate([self.p5,other.p5],axis=0),
                              np.concatenate([self.alpha,other.alpha],axis=0),
                              np.concatenate([self.beta,other.beta],axis=0),
                              np.concatenate([self.gamma,other.gamma],axis=0),
                              np.concatenate([self.Tmin,other.Tmin],axis=0),
                              np.concatenate([self.Tmax,other.Tmax],axis=0),
                              np.concatenate([self.rtype,other.rtype],axis=0),
                              np.concatenate([self.formul,other.formul],axis=0),
                              np.concatenate([self.multiform,other.multiform],axis=0),
                              self.comment + ' | ' + other.comment )
        return new_network

    def delete(self, R1='', R2='', P1='', P2=''):
        """ Delete reactions from the network based on the involved species.
        """
        # loop over the network
        toDelete = []
        for i in range(len(self.r1)):
            if (self.r1[i] == R1 and self.r2[i] == R2 and \
                self.p1[i] == P1 and self.p2[i] == P2):
                toDelete.append(i)
        if len(toDelete) > 0:
            self.index = np.delete(self.index, toDelete)
            self.nre = len(self.index)
            self.r1 = np.delete(self.r1, toDelete)
            self.r2 = np.delete(self.r2, toDelete)
            self.p1 = np.delete(self.p1, toDelete)
            self.p2 = np.delete(self.p2, toDelete)
            self.p3 = np.delete(self.p3, toDelete)
            self.p4 = np.delete(self.p4, toDelete)
            self.p5 = np.delete(self.p5, toDelete)
            self.alpha = np.delete(self.alpha, toDelete)
            self.beta = np.delete(self.beta, toDelete)
            self.gamma = np.delete(self.gamma, toDelete)
            self.Tmin = np.delete(self.Tmin, toDelete)
            self.Tmax = np.delete(self.Tmax, toDelete)
            TMPspecies = set( np.concatenate([self.r1, self.r2, self.p1, self.p2,
                                              self.p3, self.p4, self.p5], axis=0) )
            self.species = list( TMPspecies )
            # 2) Convert to ALCHEMIC nomenclature
            self.amass = np.zeros( len(self.species) )
            self.anum  = np.zeros( len(self.species) )
            self.count_atoms()
            # 3) Set the reaction type according OSU (or KIDA)
            self.rtype  = np.delete(self.rtype, toDelete)   #np.zeros( len(self.r1) )
            self.formul = np.delete(self.formul, toDelete)
            self.multiform = np.delete(self.multiform, toDelete)
            self.ndpl = np.delete(self.ndpl, toDelete)

    def sortreactants(self):
        """ Sorts the reactant and product names to aphabetical order.

        The index and order of the reactions does not change, only the order of
        R1 and R2; and P1, P2, P3, P4 and P5, respectively.
        The main network does not change, but a new object with network class
        will be returned.
        """
        sortedntw = copy.deepcopy(self)
        # Order the main network
        for i in range(len(sortedntw.r1)):
            reacTMP = [sortedntw.r1[i],sortedntw.r2[i]]
            reacTMP.sort(key=self.atomnumber, reverse=True)
            if self.atomnumber(reacTMP[0]) == self.atomnumber(reacTMP[1]):
                reacTMP.sort()         # if atom and charge number the same, sort by alphabet.
            prodTMP = [sortedntw.p1[i],sortedntw.p2[i],sortedntw.p3[i],sortedntw.p4[i],sortedntw.p5[i]]
            prodTMP.sort(key=self.atomnumber, reverse=True)
            if self.atomnumber(prodTMP[0]) == self.atomnumber(prodTMP[1]):
                prodTMPsort = prodTMP[0:2]
                prodTMPsort.sort()    # sort only the first two elements, not expected to
                prodTMP[0] = prodTMPsort[0] # have more equal atom/ionisation state
                prodTMP[1] = prodTMPsort[1] # species.
            sortedntw.r1[i] = reacTMP[0]
            sortedntw.r2[i] = reacTMP[1]
            sortedntw.p1[i] = prodTMP[0]
            sortedntw.p2[i] = prodTMP[1]
            sortedntw.p3[i] = prodTMP[2]
            sortedntw.p4[i] = prodTMP[3]
            sortedntw.p5[i] = prodTMP[4]
        return sortedntw

    def atomnumber(self, SpecToFind):
        """ This function returns the atom number of species in the network.

        This method takes a species name and searches the NETWORK object
        species list for the given name. If found it will return the atom
        number of the species.
        """
        if SpecToFind in ["", "PHOTON", "XRAYS", "CRPHOT", "ELECTR", "CRP",
                          "DESORB", "FREEZE"]:
           atmnum = 0  # large value that this goes to the end of the list
        elif SpecToFind in ["G0", "G-", "G+"]:
           atmnum = 1
        else:
           try:
              index = self.species.index(SpecToFind)
              atmnum = self.anum[index]
              if ("+" in SpecToFind) or ("-" in SpecToFind):
                  atmnum = atmnum + 100
           except:
              print( "Species {SpecToFind} is not found in species list!" )
              atmnum = -1000  # put the unknown species to the beginning

        return atmnum

    def updaterates(self, other, eps=0.001, addnew=True, updold=True):
        """ Updates the gas phase rates using a second network.

        The second network is input and must have network class, as defind in
        this code. The reactants and the products for each reactions are
        ordered in alphabetic order, then they are compared to find the
        matching reactions. When found the reaction coefficients of the
        primary network are updated with those in the secondary. The updated
        network is returned.

        :param self: primary network
        :type self: pychemic.network
        :param other: secondary network
        :type other: pychemic.network
        :param eps: threshold of difference in old and new coeff. (default 0.01)
        :type eps: float or double
        :param addnew: add reactions from secondary if not in primary
                    (default True)
        :type addnew: boolean
        :param updold: update the coefficients of matching reactions
                    (default True)
        :type updold: boolean
        """
        base = self.sortreactants()
        update = other.sortreactants()

        NreactNew = 0            # new
        NreactUpd = 0            # updated
        NreactNU =  0            # not unique
        NreactFound = 0          # found in both networks

        NewReactions = []
        UpdatedReactions = []

        for i in range(len(update.r1)):
            r1find = al_commons.exactmatch(base.r1, update.r1[i])
            r2find = al_commons.exactmatch(base.r2, update.r2[i])
            p1find = al_commons.exactmatch(base.p1, update.p1[i])
            p2find = al_commons.exactmatch(base.p2, update.p2[i])
            p3find = al_commons.exactmatch(base.p3, update.p3[i])
            p4find = al_commons.exactmatch(base.p4, update.p4[i])
            p5find = al_commons.exactmatch(base.p5, update.p5[i])
            loc = r1find + r2find + p1find + p2find + p3find + p4find + p5find
            loc2 = np.where(loc >= 0)
            if ( len(loc2[0]) > 1 ):
               print( "\nReaction is duplicate (:4):".format(len(loc2[0])) )
               print( "{} + {} -> {} + {} + {} + {}".format(update.r1[i],
                                                            update.r2[i],
                                                            update.p1[i],
                                                            update.p2[i],
                                                            update.p3[i],
                                                            update.p4[i]) )

#               print( update.r1[i], update.r2[i], update.p1[i], update.p2[i] )
#               print( base.r1[loc2], base.r2[loc2], base.p1[loc2], base.p2[loc2] )
#               print( r1find[loc2], r2find[loc2], p1find[loc2], p2find[loc2] )
               NreactNU = NreactNU + 1
               NreactFound = NreactFound + 1
               what2do = raw_input("Update all these non-unique reactions? (y/n)")
               if (what2do in ['y','yes'] and updold):
                   for j in range(len(loc2[0])):
                       self.alpha[loc2[0][j]]  = update.alpha[i]
                       self.beta[loc2[0][j]]   = update.beta[i]
                       self.gamma[loc2[0][j]]  = update.gamma[i]
#                       self.Tmin[loc2[0][j]]  = update.Tmin[i]   # temperature range
#                       self.Tmax[loc2[0][j]]  = update.Tmax[i]   # is not updated!
                       self.rtype[loc2[0][j]]  = update.rtype[i]
                       self.formul[loc2[0][j]] = update.formul[i]
#                       self.multiform[loc2[0][j]] = update.multiform[i]
#                       self.ndpl[loc2[0][j]] = update.ndpl[i]
                       NreactUpd = NreactUpd + 1
            elif ( len(loc2[0]) == 1 ):
               NreactFound = NreactFound + 1
               dalpha = 0.0
               dbeta = 0.0
               dgamma = 0.0
               if update.alpha[i] != 0e0:
                  dalpha = ( abs(self.alpha[loc2]-update.alpha[i]) /
                            update.alpha[i] )
               if update.beta[i] != 0e0:
                  dbeta = ( abs(self.beta[loc2]-update.beta[i]) /
                            update.beta[i] )
               if update.gamma[i] != 0e0:
                  dgamma = ( abs(self.gamma[loc2]-update.gamma[i]) /
                            update.gamma[i] )

               if (dalpha > eps) or (dbeta > eps) or (dgamma > eps):

                  UpdatedReactions.append((update.r1[i], ' + ', update.r2[i],
                        " -> ", update.p1[i], " + ", update.p2[i], "..."))
                  UpdatedReactions.append( ('Old:', str(self.alpha[loc2][0]),
                        str(self.beta[loc2][0]), str(self.gamma[loc2][0])) )
                  UpdatedReactions.append( ('New:', str(update.alpha[i]),
                        str(update.beta[i]), str(update.gamma[i])) )
                  if updold:
                      self.alpha[loc2]  = update.alpha[i]
                      self.beta[loc2]   = update.beta[i]
                      self.gamma[loc2]  = update.gamma[i]
                      self.Tmin[loc2]  = update.Tmin[i]
                      self.Tmax[loc2]  = update.Tmax[i]
                      self.rtype[loc2]  = update.rtype[i]
                      self.formul[loc2] = update.formul[i]
                      self.multiform[loc2] = update.multiform[i]
                      self.ndpl[loc2] = update.ndpl[i]
                  NreactUpd = NreactUpd + 1
            else:
               NewReactions.append( (update.r1[i], ' + ', update.r2[i], " -> ",
                    update.p1[i], " + ", update.p2[i], "...") )
               NreactNew = NreactNew + 1
               if addnew:
                   self.r1 = al_commons.add2nparr(np.copy(self.r1), update.r1[i])
                   self.r2 = al_commons.add2nparr(np.copy(self.r2), update.r2[i])
                   self.p1 = al_commons.add2nparr(np.copy(self.p1), update.p1[i])
                   self.p2 = al_commons.add2nparr(np.copy(self.p2), update.p2[i])
                   self.p3 = al_commons.add2nparr(np.copy(self.p3), update.p3[i])
                   self.p4 = al_commons.add2nparr(np.copy(self.p4), update.p4[i])
                   self.p5 = al_commons.add2nparr(np.copy(self.p5), update.p5[i])
                   self.alpha = al_commons.add2nparr(np.copy(self.alpha),
                                    update.alpha[i])
                   self.beta = al_commons.add2nparr(np.copy(self.beta),
                                    update.beta[i])
                   self.gamma = al_commons.add2nparr(np.copy(self.gamma),
                                    update.gamma[i])
                   self.Tmin = al_commons.add2nparr(np.copy(self.Tmin),
                                     update.Tmin[i])
                   self.Tmax = al_commons.add2nparr(np.copy(self.Tmax),
                                 update.Tmax[i])
                   self.rtype = al_commons.add2nparr(np.copy(self.rtype),
                                    update.rtype[i])
                   self.formul = al_commons.add2nparr(np.copy(self.formul),
                                    update.formul[i])
                   self.multiform = al_commons.add2nparr(np.copy(self.multiform),
                                    update.multiform[i])
                   self.ndpl = al_commons.add2nparr(np.copy(self.ndpl),
        			    update.ndpl[i])
                   index = self.index[-1] + 1
                   self.index = al_commons.add2nparr(np.copy(self.index),index)
                   # Missing species
                   self.addspec_ifnot_present(update, update.r1[i])
                   self.addspec_ifnot_present(update, update.r2[i])
                   self.addspec_ifnot_present(update, update.p1[i])
                   self.addspec_ifnot_present(update, update.p2[i])
                   self.addspec_ifnot_present(update, update.p3[i])
                   self.addspec_ifnot_present(update, update.p4[i])

        print( "\nNetwork is combined with {}!\n".format(update.comment) )
        print( "Number of reactions in both networks: {}".format(NreactFound) )
        print( "Number of reactions updated: {}".format(NreactUpd) )
        if updold:
            print( "Their rates are updated!" )
        else:
            print( "Their rates are NOT updated!" )
        print( "-------------------------------------------" )
        for i in UpdatedReactions:
            print( '    '.join(i) )
        print( "                                            " )
        print( "Number of non-unique reactions: {}".format(NreactNU) )
        print( "Number of new reactions       : {}".format(NreactNew) )
        if addnew:
            print( "Which are added!" )
        else:
            print( "Which are NOT added!" )
        print( "-------------------------------------------" )
        for i in NewReactions:
            print( '    '.join(i) )
        print( "                                            " )

    def addspec_ifnot_present(self, update, species):
        """ Add species to the network species list if not present already.

        :param self: primary network
        :type self: pychemic.network
        :param update: secondary network, where the new species already present
        :type update: pychemic.network
        :param species: species string to be added to the network
        :type  species: str
        """
        DiscardSpecList = ["CRP","CRPHOT","FREEZE","DESORB","XRAYS","PHOTON"]
        if ( (species != '') and (species not in self.species) and
                (species not in DiscardSpecList) ):
           print( "{} is missing!".format(species) )
           SpecNum = len(self.species)
           UpdateSpecLoc = update.species.index(species)
           self.species.append(update.species[UpdateSpecLoc])
           self.anum.resize(SpecNum + 1, refcheck=False)
           self.amass.resize(SpecNum + 1, refcheck=False)
           self.anum[SpecNum] = update.anum[UpdateSpecLoc]
           self.amass[SpecNum] = update.amass[UpdateSpecLoc]

    def check(self):
        """ Checks the chemical network for consistency.

        This method checks the network for self-consistency, mass and charge
        conservations. At the moment the test is only done for the atomic
        number conservation.
        """
        print( "Checking for atomic number and charge conservation..." )
        ErrorNum = 0
        for i in range(len(self.r1)):
            leftSide = self.atomnumber(self.r1[i]) + self.atomnumber(self.r2[i])
            leftCharge = ( al_commons.chargenumber(self.r1[i]) +
                           al_commons.chargenumber(self.r2[i]) )
            if (leftSide <= -2000):
               leftSide + 2000
               print( "leftSide << 2000!" )
            if (leftSide <= -1000):
               print( "leftSide << 1000!" )
            if (leftSide >= 200):
               leftSide = leftSide - 200.
            if (leftSide >= 100):
               leftSide = leftSide - 100.
            rightSide = ( self.atomnumber(self.p1[i]) +
                         self.atomnumber(self.p2[i]) +
                         self.atomnumber(self.p3[i]) +
                         self.atomnumber(self.p4[i]) +
                         self.atomnumber(self.p5[i]) )
            rightCharge = ( al_commons.chargenumber(self.p1[i]) +
                           al_commons.chargenumber(self.p2[i]) +
                           al_commons.chargenumber(self.p3[i]) +
                           al_commons.chargenumber(self.p4[i]) +
                           al_commons.chargenumber(self.p5[i]) )
            if (rightSide <= -2000):
#               leftSide + 2000
               print( "leftSide << 2000!" )
            if (rightSide <= -1000):
               print( "leftSide << 1000!" )
#            if (rightSide >= 500):
#               rightSide = rightSide - 500.
#            if (rightSide >= 400):
#               rightSide = rightSide - 400.
            if (rightSide >= 300):
               rightSide = rightSide - 300.
            if (rightSide >= 200):
               rightSide = rightSide - 200.
            if (rightSide >= 100):
               rightSide = rightSide - 100.
            if (leftSide != rightSide):
               print( "Error atomic mass is not conserved!" )
               print( "{} + {} -> {} + {} + {}".format(self.r1[i], self.r2[i],
                                                       self.p1[i], self.p2[i],
                                                       self.p3[i]) )
               print( "{} <-> {}".format(leftSide,rightSide) )
               ErrorNum = ErrorNum + 1
            if (leftCharge != rightCharge):
               print( "Error charge is not conserved!" )
               print( "{} + {} -> {} + {} + {}".format(self.r1[i], self.r2[i],
                                                       self.p1[i], self.p2[i],
                                                       self.p3[i]) )
               print( "{} <-> {}".format(leftCharge,rightCharge) )
               ErrorNum = ErrorNum + 1
        if ErrorNum !=0:
           print( "Errors have been found!" )
        else:
           print( "Check complete: no errors!" )

    def write2alchemic(self, networkfile='rreactions_op_Sipila2015.dat',
        specfile='rspecies_op_Sipila2015.dat'):
        """ Write network in the ALCHEMIC input format.

        :param networkfile: network file (default rreactions_op_Sipila2015)
        :type networkfile: str
        :param specfile: species file (default rspecies_op_Sipila2015.dat)
        :type specfile: str
        """
        if os.path.isfile( networkfile ):
           print( "File {} already exists!".format(networkfile) )
           what2do = raw_input('Do you want to overwrite? (y/n)')
           if what2do == 'n' or what2do == 'no':
              print( "No file was written!" )
              return
        outfile = open(networkfile, 'w')
        # Write the header
        nreact = len( self.index )
        outfile.write( ('%6u ' + self.comment + '\n') % nreact )
        for i in range( len( self.index) ):
            outfile.write('%6u %-12s%-12s%-12s%-12s%-12s%-12s%-12s%-12s%9.2E%9.2E%9.2E %6u %6u %9.2E %9.2E %6u %6u\n'
                    % ( self.index[i], self.r1[i],
                    self.r2[i],' ',self.p1[i],self.p2[i],self.p3[i], self.p4[i],
                    self.p5[i], self.alpha[i], self.beta[i], self.gamma[i],
                    self.rtype[i], self.formul[i], self.Tmin[i], self.Tmax[i]+1.,
                    self.multiform[i], self.ndpl[i]) )
        # Tmax[i]+1 is added to correct the problem in the KIDA that if a reaction
        # has more than 1 temperature range, then the lower Tmax is 1 K lower than
        # the high temperature Tmin.
        outfile.close()
        print( "File {} is written!".format(networkfile) )
        # Write the species file
        if os.path.isfile( specfile ):
           print( "File {} already exists!".format(specfile) )
           what2do = raw_input('Do you want to overwrite? (y/n)')
           if what2do == 'n' or what2do == 'no':
              print( "No file was written!" )
              return
        outfile = open(specfile, 'w')
        nspec = len( self.species )
        outfile.write( ('%6u ' + self.comment + '\n') % nspec )
        for i in range( nspec ):
            outfile.write( '%-12s\n' % (self.species[i]) )
        outfile.close()
        print( "File {} is written!".format(specfile) )

    def write2osu(self, networkfile='osu_network.dat'):
        '''
        Write network in the OSU input format.

        :param networkfile: network file (default osu_network.dat)
        :type networkfile: str
        '''
        if os.path.isfile( networkfile ):
           print( "File {} already exists!".format(networkfile) )
           what2do = raw_input('Do you want to overwrite? (y/n)')
           if what2do == 'n' or what2do == 'no':
              print( "No file was written!" )
              return
        outfile = open(networkfile, 'w')
        
        #
        # Write the header
        nrspec = len( self.species )
        nreact = len( self.index )
        
        #
        # Write species
        outfile.write( ('%6u \n') % nrspec )
        
        for i in range(nrspec):
            outfile.write( ('%-12s \n') % self.species[i] )
            
        #
        # Write reactions
        outfile.write( ('%6u \n') % nreact )
        
        for i in range(nreact):
            outfile.write(' %4u %-10s%-10s%-10s%-10s%-10s%-10s%-10s%-10s%8.2E %5.2F %8.2E\n'
                    % (self.index[i], self.r1[i], self.r2[i], ' ', self.p1[i], 
                       self.p2[i], self.p3[i], self.p4[i], self.p5[i], 
                       self.alpha[i], self.beta[i], self.gamma[i]) )

        # Done
        outfile.close()
        print( "File {} is written!".format(networkfile) )

# COUNT_ATOMS
#

#
    def count_atoms( self ):
        """ For each species in the reactions, calculate the atom number.

        This is necessary when reducing the chemical network by molecule mass.
        """
        # Initialization of atom's names:
        Atoms1C = ['H','C','N','O','S','P','D']
        mass1C  = [1.00000E+00,1.20110E+01,1.40067E+01,1.59994E+01,3.20660E+01,
                   3.09738E+01,2.00000E+00]
        Atoms2C = ['HE','FE','SI','NA','MG','CL']
        mass2C  = [4.00260E-00,5.58470E+01,2.80855E+01,2.29898E+01,2.43050E+01,
                   3.54527E+01]
        for tmpSpec in self.species:
            ct = self.species.index(tmpSpec)  # what is the current index?
            # Remove 'g' and ortho and para states and the mantel 'm' species?
            tmpSpec = tmpSpec.replace('g','')
            tmpSpec = tmpSpec.replace('o','')
            tmpSpec = tmpSpec.replace('p','')
            tmpSpec = tmpSpec.replace('m','')
            # Remove '-' and '+' ionization states
            tmpSpec = tmpSpec.replace('-','')
            tmpSpec = tmpSpec.replace('+','')
            # Take care of the PHOTONS and ELECTR
            if tmpSpec == 'PHOTON':
               tmpMass = 0.
               tmpSpec = ''
               tmpAtms = 0
            if tmpSpec == 'ELECTR':
               tmpMass = 5.4462409e-07
               tmpSpec = ''
               tmpAtms = 0
            if tmpSpec == 'CRPHOT':
               tmpMass = 0.
               tmpSpec = ''
               tmpAtms = 0
            tmpMass = 0.
            tmpAtms = 0
            # do it for the two letter names
            for s2i in range( len(Atoms2C) ):
                while Atoms2C[s2i] in tmpSpec:
                     strindex = tmpSpec.find(Atoms2C[s2i])
                     if ( strindex >= 0 ):
                     # check if mulitple atoms?
                        multipl = 1.
                        ToRemove = 0
                        if strindex+2 <= len(tmpSpec)-1:
                           if tmpSpec[strindex+2].isdigit() :
                                multipl = float(tmpSpec[strindex+2])
                                ToRemove = 1
                     # check if 2 digits
                        if strindex+3 <= len(tmpSpec)-1:
                           if tmpSpec[strindex+2:strindex+3].isdigit() :
                                multipl = float(tmpSpec[strindex+2:strindex+3])
                                ToRemove = 2
                        if ToRemove > 0:
                           tmpSpec = tmpSpec[:strindex+2] + tmpSpec[(strindex+2+ToRemove):]
                     # add up the mass, remove the atom from the string
                        tmpMass = tmpMass + ( multipl * mass2C[s2i] )
                        tmpSpec = tmpSpec[:strindex] + tmpSpec[(strindex+2):]
                        tmpAtms = tmpAtms + multipl
            # do it for the one letter names
            for s1i in range( len(Atoms1C) ):
                while Atoms1C[s1i] in tmpSpec:
                     strindex = tmpSpec.find(Atoms1C[s1i])
                     if ( strindex >= 0 ):
                        # check if mulitple atoms?
                        multipl = 1.
                        ToRemove = 0
                        if strindex+1 <= len(tmpSpec)-1:
                           if tmpSpec[strindex+1].isdigit() :
                                multipl = float(tmpSpec[strindex+1])
                                ToRemove = 1
                        # check if 2 digits
                        if strindex+2 <= len(tmpSpec)-1:
                           if tmpSpec[strindex+1:strindex+2+1].isdigit() :
                                multipl = float(tmpSpec[strindex+1:strindex+1+2])
                                ToRemove = 2
                        if ToRemove > 0:
                           tmpSpec = tmpSpec[:strindex+1] + tmpSpec[(strindex+1+ToRemove):]
                        # add up the mass
                        tmpMass = tmpMass + ( multipl * mass1C[s1i] )
                        tmpSpec = tmpSpec[:strindex] + tmpSpec[(strindex+1):]
                        tmpAtms = tmpAtms + multipl
            self.amass[ct] = tmpMass
            self.anum[ct] = tmpAtms
        print( "Counting mass and atom number is done!\n" )
#
# READ_NETWORK
#
def read_kida_network(input_file='ir.15_grain', 
                      comment='This is KIDA(-like) network'):
    """
    Reads chemical network in KIDA style format to network object.
    
    :param input_file: network file name
    :param comment: given string is stored in the network object for reference
    """
    
    skiplist =  ['!',"#","\n"]
    
    # Create data lists:
    index  = []
    rtype  = []
    formul = []
    multiform = []
    r1 = []
    r2 = []
    p1 = []
    p2 = []
    p3 = []
    p4 = []
    p5 = []
    alpha = []
    beta  = []
    gamma = []
    dumy1 = []
    dumy2 = []
    # Add temperature range
    Tmin  = []
    Tmax = []
    
    
    f = open(input_file, 'r')
    counter = 0
    for line in f:
        # skip comment lines 
        if ( line[0] not in skiplist):  
            r1.append( line[0:10] )           # we deal with T dependences in ALCHEMIC.
            r2.append( line[10:20] )
            p1.append( line[33:44] )
            p2.append( line[44:55] )
            p3.append( line[55:66] )
            p4.append( line[66:77] )
            p5.append( '' )

            # Apply a fix here, there are 2 expressions for recombinations with grains
            # In ALCHEMIC the KIDA recombination is marked by grain in the R2 location.
            if 'GRAIN' in r1[-1]:
                tmp = r1[-1]
                r1[-1] = r2[-1]
                r2[-1] = tmp
                

            alpha.append( np.double( line[90:100] ) )
            beta.append( np.double( line[101:111] ) )
            gamma.append( np.double( line[112:122] ) )
            # Temperature dependences
            Tmin.append( np.double( line[150:155] ) )
            Tmax.append( np.double( line[157:162] ) )

            index.append( np.int(line[166:171]) )
            rtype.append( np.int(line[146:148]) )
            formul.append( np.int(line[163:165]) )
            multiform.append( np.int(line[172:173] ) ) # are there duplicate, higher
                                                       # version of this reaction?

            counter = counter + 1
    f.close()
    
    # Convert lists to numpy arrays
    index = np.asarray(index, dtype=np.int32) 
    rtype = np.asarray(rtype, dtype=np.int32)
    formul = np.asarray(formul, dtype=np.int32)
    multiform = np.asarray(multiform, dtype=np.int32)
    r1 = np.asarray(r1, dtype='|S12')
    r2 = np.asarray(r2, dtype='|S12')
    p1 = np.asarray(p1, dtype='|S12')
    p2 = np.asarray(p2, dtype='|S12')
    p3 = np.asarray(p3, dtype='|S12')
    p4 = np.asarray(p4, dtype='|S12')
    p5 = np.asarray(p5, dtype='|S12')
    alpha = np.asarray(alpha, dtype=np.float64)
    beta = np.asarray(beta, dtype=np.float64)
    gamma = np.asarray(gamma, dtype=np.float64)
    Tmin = np.asarray(Tmin, dtype=np.float64)
    Tmax = np.asarray(Tmax, dtype=np.float64)
    
    
    print( "Reading file {} is done!\n".format(input_file) )
    # Create and return the network
    return network(index, r1, r2, p1, p2, p3, p4, p5, alpha, beta, gamma,
                   Tmin, Tmax, rtype, formul, multiform, comment)

def read_network(input_file='ir.15_grain', skiplines=0,
                 comment='This is Oli\'s grain network'):
    """ Reads network files from difference sources to PyChemic network.
    """
    # Get the number of lines for array creation
    with open(input_file) as network_file:
        nlines = len(network_file.readlines()) - 1 - skiplines
    # Create the arrays:
    index  = np.zeros(nlines,dtype=np.int32)
    rtype  = np.zeros(nlines,dtype=np.int32)
    formul = np.zeros(nlines,dtype=np.int32)
    multiform = np.zeros(nlines,dtype=np.int32)
    r1 = np.chararray(nlines,11)
    r2 = np.chararray(nlines,11)
    p1 = np.chararray(nlines,11)
    p2 = np.chararray(nlines,11)
    p3 = np.chararray(nlines,11)
    p4 = np.chararray(nlines,11)
    p5 = np.chararray(nlines,11)
    alpha = np.zeros(nlines,dtype=np.float64)
    beta  = np.zeros(nlines,dtype=np.float64)
    gamma = np.zeros(nlines,dtype=np.float64)
    dumy1 = np.zeros(nlines,dtype=np.int8)
    dumy2 = np.zeros(nlines,dtype=np.int8)
    # Add temperature range
    Tmin  = np.zeros(nlines,dtype=np.float64)
    Tmax = np.zeros(nlines,dtype=np.float64)
# Read the data by lines and split the lines by character spacing
    f = open(input_file, 'r')
    linecounter = 0
    if ('kida' in input_file) or ('ruaud_2015.in' in input_file):
        comment='This is KIDA(-like) network'
        for line in f:
            if (line[0] not in ['!',"#","\n"]):  # skip comment lines and duplicate reactions -> no need any more
                r1[linecounter] = line[0:10]            # we deal with T dependences in ALCHEMIC.
                r2[linecounter] = line[10:20]
                p1[linecounter] = line[33:44]
                p2[linecounter] = line[44:55]
                p3[linecounter] = line[55:66]
                p4[linecounter] = line[66:77]
                p5[linecounter] = ''

                # Apply a fix here, there are 2 expressions for recombinations with grains
                # In ALCHEMIC the KIDA recombination is marked by grain in the R2 location.
                if 'GRAIN' in r1[linecounter]:
                    r1[linecounter] = line[10:20]
                    r2[linecounter] = line[0:10]

                alpha[linecounter] = line[90:100]
                beta[linecounter] = line[101:111]
                gamma[linecounter] = line[112:122]
                # Temperature dependences
                Tmin[linecounter] = line[150:155]
                Tmax[linecounter] = line[157:162]

                index[linecounter]  = line[166:171]
                rtype[linecounter]  = line[146:148]
                formul[linecounter] = line[163:165]
                multiform[linecounter] = line[172:173] # are there duplicate, higher
                                                       # version of this reaction?

                linecounter = linecounter + 1
        # remove the skipped lines
        r1 = r1[0:linecounter].copy()
        r2 = r2[0:linecounter].copy()
        p1 = p1[0:linecounter].copy()
        p2 = p2[0:linecounter].copy()
        p3 = p3[0:linecounter].copy()
        p4 = p4[0:linecounter].copy()
        p5 = p5[0:linecounter].copy()

        alpha = alpha[0:linecounter].copy()
        beta = beta[0:linecounter].copy()
        gamma = gamma[0:linecounter].copy()
        Tmin = Tmin[0:linecounter].copy()
        Tmax = Tmax[0:linecounter].copy()

        index = index[0:linecounter].copy()
        rtype = rtype[0:linecounter].copy()
        formul = formul[0:linecounter].copy()
        multiform = multiform[0:linecounter].copy()
    elif input_file[0:5] == 'grain':
        comment='This is grain surface ntw'
        for line in f:
            if line[0] != '!':
                r1[linecounter] = line[7:18]
                r2[linecounter] = line[19:30]
                p1[linecounter] = line[43:54]
                p2[linecounter] = line[55:66]
                p3[linecounter] = line[67:78]
                p4[linecounter] = line[79:90]
                p5[linecounter] = ''

                alpha[linecounter] = line[104:112]
                beta[linecounter] = line[112:121]
                gamma[linecounter] = line[121:130]
                # Temperature dependences
                Tmin[linecounter] = -9999.
                Tmax[linecounter] = 9999.

                index[linecounter]  = linecounter+1  #line[0:6]
                rtype[linecounter]  = line[130:133]
                formul[linecounter] = 3
                multiform[linecounter] = 1
                linecounter = linecounter + 1
    elif input_file[0:3] == 'osu':
        comment='This is OSU network'
        for line in f:
            if line[0] != '!':
                r1[linecounter] = line[0:8]
                r2[linecounter] = line[8:24]
                p1[linecounter] = line[24:32]
                p2[linecounter] = line[32:40]
                p3[linecounter] = line[40:48]
                p4[linecounter] = line[48:56]
                p5[linecounter] = ''

                alpha[linecounter] = line[64:73]
                beta[linecounter]  = line[73:82]
                gamma[linecounter] = line[82:91]
                # Temperature dependences
                Tmin[linecounter] = -9999.
                Tmax[linecounter] = 9999.

                index[linecounter]  = linecounter+1  #line[0:6]
                rtype[linecounter]  = line[91:93]
                formul[linecounter] = 3
                multiform[linecounter] = 1
                linecounter = linecounter + 1
    elif 'ir.15_grain' in input_file:
        comment='This is OSU network edited by Olli'
        for line in f:
            if line[0] != '!':
                r1[linecounter] = line[6:17]
                r2[linecounter] = line[17:28]
                p1[linecounter] = line[28:39]
                p2[linecounter] = line[39:50]
                p3[linecounter] = line[50:61]
                p4[linecounter] = line[61:71]
                p5[linecounter] = ''

                alpha[linecounter] = line[71:80]
                beta[linecounter]  = 0e0
                gamma[linecounter] = line[81:90]
                # Temperature dependences
                Tmin[linecounter] = -9999.
                Tmax[linecounter] = 9999.

                index[linecounter]  = linecounter+1  #line[0:6]
                rtype[linecounter]  = 1
                formul[linecounter] = 3
                multiform[linecounter] = 1
                linecounter = linecounter + 1
    elif ('updates_sipila' in input_file) or ('updates_legal2014' in input_file):
        comment='Sipila2015 updates'
        for line in f:
            if line[0] != '!':
                r1[linecounter] = line[0:12]
                r2[linecounter] = line[12:23]
                p1[linecounter] = line[23:36]
                p2[linecounter] = line[36:48]
                p3[linecounter] = line[48:58]
                p4[linecounter] = ''
                p5[linecounter] = ''

                alpha[linecounter] = line[58:68]
                beta[linecounter]  = line[68:74]
                gamma[linecounter] = line[74:79]
                # Temperature dependences
                Tmin[linecounter] = 0.
                Tmax[linecounter] = 9999.

                index[linecounter]  = linecounter+1  #line[0:6]
                rtype[linecounter]  = -1 #line[91:93]
                formul[linecounter] = 3
                multiform[linecounter] = 1

                if r2[linecounter] == "CRPHOT":
                    print( "Warning! alpha / 1.3E-17 in CRPHOT reaction!" )
                    alpha[linecounter] = alpha[linecounter] / 1.3E-17
                linecounter = linecounter + 1
    elif 'updates_ruaud' in input_file:
        comment='Ruaud 2015 updates'
        for line in f:
            if line[0] != '!':
                r1[linecounter] = line[0:11]
                r2[linecounter] = line[11:34]
                p1[linecounter] = line[34:45]
                p2[linecounter] = line[45:56]
                p3[linecounter] = line[56:67]
                p4[linecounter] = line[67:78]
                p5[linecounter] = ''

                alpha[linecounter] = line[90:100]
                beta[linecounter]  = line[101:111]
                gamma[linecounter] = line[112:122]
                # Temperature dependences
                Tmin[linecounter] = -9999.
                Tmax[linecounter] = 9999.

                index[linecounter]  = linecounter+1  #line[0:6]
                rtype[linecounter]  = -1 #line[91:93]
                formul[linecounter] = line[123:125]
                multiform[linecounter] = 1

                linecounter = linecounter + 1
    elif input_file[0:7] == 'OSU06XR':
        comment='This is OSU updated of Semenov+ (2010)'
        for line in f:
            if line[0] != '!':
                r1[linecounter] = line[7:19]
                r2[linecounter] = line[19:43]
                p1[linecounter] = line[43:55]
                p2[linecounter] = line[55:67]
                p3[linecounter] = line[67:79]
                p4[linecounter] = line[79:91]
                p5[linecounter] = line[91:103]

                alpha[linecounter] = line[103:112]
                beta[linecounter]  = line[112:121]
                gamma[linecounter] = line[121:130]
                # Temperature dependences
                Tmin[linecounter] = -9999.
                Tmax[linecounter] = 9999.

                index[linecounter]  = linecounter+1
                rtype[linecounter]  = line[130:133]
                formul[linecounter] = 3
                multiform[linecounter] = 1

                linecounter = linecounter + 1
    elif input_file[-3:] == 'rxn':
        comment='X-ray data from Adamkovics et al.'
        for line in f:
            if line[0] != '!' and line[0] != '#':
                
                parse = line.split('&')
                
                reaction = parse[1]
                coeff = parse[2]
                
                reactant = reaction.split('->')[0].split(' + ')
                products = reaction.split('->')[1].split(' + ')
                
                r1[linecounter] = reactant[0].strip()
                if len(reactant) >= 2:
                    r2[linecounter] = reactant[1].strip()
                    
                else:
                    r2[linecounter] = 'XRAYS'
                p1[linecounter] = products[0].strip()
                if len(products) >= 2:
                    p2[linecounter] = products[1].strip()
                else:
                    p2[linecounter] = ''
                if len(products) >= 3:
                    p3[linecounter] = products[2].strip()
                else:
                    p3[linecounter] = ''
                if len(products) >= 4:
                    p4[linecounter] = products[3].strip()
                else:
                    p4[linecounter] = ''
                if len(products) >= 5:
                    p5[linecounter] = products[4].strip()
                else:
                    p5[linecounter] = ''

                alpha[linecounter] = coeff.split()[0]
                beta[linecounter]  = coeff.split()[1]
                gamma[linecounter] = coeff.split()[2]
                # Temperature dependences
                Tmin[linecounter] = -9999.
                Tmax[linecounter] = 9999.

                index[linecounter]  = linecounter+1
                rtype[linecounter]  = 39  # change later
                formul[linecounter] = 3
                multiform[linecounter] = 1

                linecounter = linecounter + 1
    else:
        for line in f:
            if line[0] != '!':
                index[linecounter] = line[0:5]
                r1[linecounter] = line[5:17]
                r2[linecounter] = line[17:28]
                p1[linecounter] = line[28:39]
                p2[linecounter] = line[39:50]
                p3[linecounter] = line[50:61]
                p4[linecounter] = line[61:72]
                p5[linecounter] = ''

                alpha[linecounter] = line[72:80]
                beta[linecounter] = line[80:90]
                gamma[linecounter] = line[90:100]
                # Temperature dependences
                Tmin[linecounter] = -9999.
                Tmax[linecounter] = 9999.

                formul[linecounter] = 3
                multiform[linecounter] = 1
                linecounter = linecounter + 1
    f.close()
    print( "Reading file {} is done!\n".format(input_file) )
    # Create and return the network
    return network(index, r1, r2, p1, p2, p3, p4, p5, alpha, beta, gamma,
                   Tmin, Tmax, rtype, formul, multiform, comment)


def add_grain(GasNet, adsorp_file = 'adsorp.energy',
              acten_file = 'active.energy',
              grain_file = 'garrod2008_network.dat',
              enthalp_file = 'enthalpies.dat',
              comment = 'This is Olli\'s desorbtion energies',
              AddAll=False,
              AddAdsDes=True,
              AddCRPH=True,
              AddGr=True,
              AddRdes=False,
              chkExo=False,
              varUVyield=False,
              exclMetal=False,
              ExtraSpec=['N2H2','CH3O','CH2OH']):
    """ Builds a grain surface chemical network from scratch.

    Function to read the desoroption energy file supplied by Sipila (2015), the
    activation energy database of Garrod (2008) and optionally a grain reaction
    network set. A gas phase network also needs to be given.

    The function first constructs freeze out and desorption processes for
    neutral species in the gas phase network. A list of species that should
    not freeze out can be given (**needs implementation**). Next the photo-
    and cosmic ray induced reactions from the gas phase network are cloned to
    the surface species. Finally a grain surface reaction network is read and
    the reactions invoving the neutral species set of the gas phase network
    are added to the new network (if AddAll set to True then all reactions will
    be added). If the reaction is found in the activation energy database, then
    its activation energy is updated from the database.

    The resultnetwork is constructed in similar fasion as the main network.
    This can be added to other networks by the addition operator and written
    to ALCHEMIC network file format.

    :param GasNet: gas phase network
    :type GasNet: pychemic.network
    :param adsorp_file: ascii file containing the adsorption energy database
    :type adsorp_file: str
    :param acten_file: ascii file containing the activation energy database
    :type acten_file: str
    :grain_file: ascii file containing a grain surface reaction network
    :type grain_file: str
    :param enthalp_file: aschii file containing species enthalpies.
    :type enthalp_file: str
    :param comment: string to be added to the network description
    :type comment: str
    :param AddAdsDes: if True then adds molecule adsorption and desorption
                mechanisms to grain surfaces (default True)
    :type AddAdsDes: boolean
    :param AddGr: if True then adds grain surface reactions to the network
                (default True)
    :type AddGr: boolean
    :param AddCRPH: if True then adds photon and CR induced photon reactions
                on the grain surface (default True)
    :type AddCRPH: boolean
    :param AddAll: if True all reactions from grain_file are added
                (default False)
    :type AddAll: boolean
    :param AddRdes: if True then reactive desorption will be added.
                (default False)
    :type AddRdes: boolean
    :param chkExo: Checks exothermicity when reactive desorption is added.
    :type chkExo: boolean
    :param varUVyield: If true then variable UV desorption yields are used for
                some species. (default False)
    :type varUVyield: boolean
    """
    # Some precaution to deal with senseless input keywords:
    if AddAdsDes or AddGr or AddCRPH:
    # Define the UV desorption yields for a set of species:
        # Note on the desorption yields:
        # It is hard to determine the desorption yields even for the above given
        # species and it depends strongly on the composition and thickness of the
        # ice. Some species desorb with other species from deeper in the ice.
        # Use the above values with cotion!
        # Relevant paper are: Oeberg et al. 2009a and 2009b.
        # References of CO, H2 and CH3OH: Bertin et al. 2013, 2016
        SpecYield = ['CO','CO2','N2','H2O','D2O','CH3OH']
        uvYield = [3.0E-3,2.3E-3,3.0E-3,1.0E-3,1.0E-3,1.2e-5]  # at ~10 K
        # Define metal species that should not freeze out to the grains
        # i.e. to be used with low metal abundances
        Metals = ["NA","MG","SI","P","S","CL","FE","F"]
    # Get the number of lines for array creation
        print( "Reading binding energies from {} ...".format(adsorp_file) )
        with open(adsorp_file) as edes_file:
            nlines = len(edes_file.readlines())
    # Create the arrays:
        index = np.zeros(nlines, dtype=np.int32)
        SpecEd = []
        edes = np.zeros(nlines, dtype=np.float64)
    # Read the data by lines and split the lines by character spacing
        f = open(adsorp_file, 'r')
        linecounter = 0
        for line in f:
            if line[0] != '#' and line[0] != ' ' and line[0] != '\n':
                index[linecounter] = linecounter
                tmp = line.split()
                Stmp = tmp[0].replace('o','').replace('p','')
                Stmp = Stmp.replace('Mg','MG').replace('He','HE').replace('Fe','FE')
                Stmp = Stmp.replace('Si','SI').replace('Na','NA').replace('Cl','CL')
                SpecEd.append(Stmp.strip())
                edes[linecounter] = float(tmp[1])
                linecounter = linecounter + 1
        f.close()
        edes = edes[0:linecounter]
    # Get the list of neutral species in the gas-phase network
    # and try to find them in the SpecEd list
        SpecFound = []
        Eds = []
        SpecNotFound = []
        GasNetSpec = GasNet.species
        GasNetSpec[len(GasNetSpec):] = ExtraSpec
        for s in GasNet.species:
            # check if it is on the metals list
            exclude = 0
            if (exclMetal):
                for m in Metals:
                    if m in s:
                       exclude = 1
                       break
            # check wether it is neutral
            if ( al_commons.chargenumber(s) == 0 ) and ( exclude == 0 ):
                try:
                    iEds = SpecEd.index(s)
                    SpecFound.append(s)
                    Eds.append(edes[iEds])
                except:
                    print( "{} not found!".format(s) )
        nreact = len(SpecFound)
        index  = np.zeros(2*nreact,dtype=np.int32)
        rtype  = np.zeros(2*nreact,dtype=np.int32)
        formul = np.zeros(2*nreact,dtype=np.int32)
        multiform = np.zeros(2*nreact,dtype=np.int32)

        r1 = np.chararray(2*nreact,11)
        r1[:] = ''
        r2 = np.chararray(2*nreact,11)
        r2[:] = ''
        p1 = np.chararray(2*nreact,11)
        p1[:] = ''
        p2 = np.chararray(2*nreact,11)
        p2[:] = ''
        p3 = np.chararray(2*nreact,11)
        p3[:] = ''
        p4 = np.chararray(2*nreact,11)
        p4[:] = ''
        p5 = np.chararray(2*nreact,11)
        p5[:] = ''

        alpha = np.zeros(2*nreact,dtype=np.float64)
        beta  = np.zeros(2*nreact,dtype=np.float64)
        gamma = np.zeros(2*nreact,dtype=np.float64)
        Tmin  = np.zeros(2*nreact,dtype=np.float64)
        Tmax  = np.zeros(2*nreact,dtype=np.float64)

        for cc in range(nreact):
        # Freeze out reaction
            r1[cc] = SpecFound[cc]
            r2[cc] = 'FREEZE'
            p1[cc] = 'g'+SpecFound[cc]
            alpha[cc] = 1.0
            Tmin[cc] = -9999.
            Tmax[cc] = 9999.
        # Desorbtion reaction
            jj = cc+nreact
            r1[jj] = 'g'+SpecFound[cc]
            r2[jj] = 'DESORB'
            p1[jj] = SpecFound[cc]
            alpha[jj] = 1.0
            if varUVyield and SpecFound[cc] in SpecYield:
                beta[jj] = uvYield[SpecYield.index(SpecFound[cc])]
            else:
                beta[jj] = 0.0
            gamma[jj] = Eds[cc]
            Tmin[jj] = -9999.
            Tmax[jj] = 9999.
        # Metadata
            index[cc] = cc+1
            index[jj] = jj+1
            rtype[cc] = 16
            rtype[jj] = 15
            formul[cc] = 3
            formul[jj] = 3
            multiform[cc] = 1
            multiform[jj] = 1
    else:
        "Print for this code to work you must set at least the AddAdsDes keyword!"

    if AddCRPH:
    # Copy the photo- and CRP induced reactions from the gas phase network.
        print( "Copying gas phase photo processes to grain surface species..." )
        PhotoList = ["PHOTON","CRPHOT"]
        for cc in range(len(GasNet.index)):
            if ( (GasNet.r1[cc] in SpecFound) and (GasNet.r2[cc] in PhotoList)
                    and (al_commons.chargenumber(GasNet.p1[cc]) == 0)
                    and (al_commons.chargenumber(GasNet.p2[cc]) == 0) ):
                r1 = al_commons.add2nparr(r1, 'g' + GasNet.r1[cc])
                r2 = al_commons.add2nparr(r2, GasNet.r2[cc])
                p1 = al_commons.add2nparr(p1, 'g' + GasNet.p1[cc])
                if GasNet.p2[cc] == "":
                    p2 = al_commons.add2nparr(p2, GasNet.p2[cc])
                else:
                    p2 = al_commons.add2nparr(p2, 'g' + GasNet.p2[cc])
                if GasNet.p3[cc] == "":
                    p3 = al_commons.add2nparr(p3, GasNet.p3[cc])
                else:
                    p3 = al_commons.add2nparr(p3, 'g' + GasNet.p3[cc])
                if GasNet.p4[cc] == "":
                    p4 = al_commons.add2nparr(p4, GasNet.p4[cc])
                else:
                    p4 = al_commons.add2nparr(p4, 'g' + GasNet.p4[cc])
                if GasNet.p5[cc] == "":
                    p5 = al_commons.add2nparr(p5, GasNet.p5[cc])
                else:
                    p5 = al_commons.add2nparr(p5, 'g' + GasNet.p5[cc])

                alpha = al_commons.add2nparr(alpha, GasNet.alpha[cc])
                beta = al_commons.add2nparr(beta, GasNet.beta[cc])
                gamma = al_commons.add2nparr(gamma, GasNet.gamma[cc])
                Tmin = al_commons.add2nparr(Tmin, GasNet.Tmin[cc])
                Tmax = al_commons.add2nparr(Tmax, GasNet.Tmax[cc])
                rtype = al_commons.add2nparr(rtype, 10+GasNet.rtype[cc])
                formul = al_commons.add2nparr(formul, GasNet.formul[cc])
                multiform = al_commons.add2nparr(multiform, GasNet.multiform[cc])
                index = al_commons.add2nparr(index, index[-1] + 1)

    if AddGr:
    # Read the activation energy file (Garrod 2008)
        print( "Reading reaction activation energies from {} ...".format(acten_file) )
        f = open(acten_file, 'r')
        linecounter = 0
        ActEn_index = []
        ActEn_r1 = []
        ActEn_r2 = []
        ActEn_p1 = []
        ActEn_p2 = []
        ActEn_p3 = []
        ActEn_p4 = []
        ActEn_react = []
        ActEn = []
        BarWidth = []
        for line in f:
            if (line[0] != '#') and (line[0] != ' ') and (line[0] != '\n'):
                ActEn_index.append(linecounter+1)
                ActEn_r1.append(line[0:8].strip())
                ActEn_r2.append(line[8:16].strip())
                ActEn_p1.append(line[16:24].strip())
                ActEn_p2.append(line[24:32].strip())
                ActEn_p3.append(line[32:40].strip())
                ActEn_p4.append(line[40:48].strip())
                ActEn_react.append(line[0:8].strip() + '+' + line[8:16].strip() +
                          '->' + line[16:24].strip())
                ActEn.append(float(line[48:57]))
                BarWidth.append(float(line[58:63]))
                linecounter = linecounter + 1
        f.close()

    # Read enthalpies if endo/exothermicity is checked
        enthalpies = {}
        if enthalp_file:
            try:
                f_enth = open(enthalp_file)
                for line in f_enth:
                    if len(line) > 1:
                        (key, val) = line.split()[0:2]
                        enthalpies[key] = float(val)
                print( enthalpies.keys() )
            except:
                print( "WARNING: chkExo is set but enthalpy file not found. \
                        Continuing without check!" )
        binding_energy = {}
        if adsorp_file:
            try:
                f_ads = open(adsorp_file)
                for line in f_ads:
                    if len(line) > 1:
                        (key, val) = line.split()[0:2]
                        binding_energy[key] = float(val)
                print( binding_energy.keys() )
            except:
                print( "WARNING: adsorp_file is set but file not found. \
                        Continuing without check!" )
    # Read the grain surface chemistry file (curretly Garrod 2008)
        print( "Reading surface reaction network {} ...".format(grain_file) )
        if "garrod2008" in grain_file:
            rformat = 1
        elif "grain_surf_dima" in grain_file:
            rformat = 2
        elif "ir.15_grain" in grain_file:
            rformat = 3
        elif "grain_ruaud" in grain_file:
            rformat = 4
        elif "kida.grain" in grain_file:
            rformat = 4
        else:
            print( "Grain file not known, stopping!" )
            return -1
        # Besides grain surface reactions the following might be accepted:
        Accepted = []#['PHOTON','CRPHOT'] # get photo reactions always from gas phase network!
        f = open(grain_file, 'r')
        rcounter = 0
        for line in f:
            if (line[0] not in ['#', '\n', '!']):
                if (rformat == 1):
                    GNtw_r1 = line[0:8].strip()
                    GNtw_r2 = line[8:18].strip()
                    GNtw_p1 = line[24:32].strip()
                    GNtw_p2 = line[32:40].strip()
                    GNtw_p3 = line[40:48].strip()
                    GNtw_p4 = line[48:56].strip()
                    GNtw_p5 = line[56:64].strip()
                    GNtw_alpha = float(line[64:73])
                    if ( (GNtw_r1.replace("g","") in SpecFound) and
                        (GNtw_r2.replace("g","") in SpecFound) and
                        (GNtw_p1.replace("g","") in SpecFound) ):
                        if ( (GNtw_p2 == '') or
                            (GNtw_p2.replace("g","") in SpecFound) ):
                            r1 = al_commons.add2nparr(r1, GNtw_r1)
                            r2 = al_commons.add2nparr(r2, GNtw_r2)
                            p1 = al_commons.add2nparr(p1, GNtw_p1)
                            p2 = al_commons.add2nparr(p2, GNtw_p2)
                            p3 = al_commons.add2nparr(p3, GNtw_p3)
                            p4 = al_commons.add2nparr(p4, GNtw_p4)
                            p5 = al_commons.add2nparr(p5, GNtw_p5)

                            alpha = al_commons.add2nparr(alpha, GNtw_alpha)
                            beta = al_commons.add2nparr(beta, 0.0)
                        # Find the activation energy if exists
                            try:
                                jj = ActEn_react.index(GNtw_r1 + '+' + GNtw_r2 +
                                            '->' + GNtw_p1)
                                ActivationEnergy = ActEn[jj]
                            except:
                                ActivationEnergy = 0.0
                            gamma = al_commons.add2nparr(gamma, ActivationEnergy)

                            Tmin = al_commons.add2nparr(Tmin, -9999.)
                            Tmax = al_commons.add2nparr(Tmax, 9999.)
                            rtype = al_commons.add2nparr(rtype, 14)
                            formul = al_commons.add2nparr(formul, 3)
                            multiform = al_commons.add2nparr(multiform, 1)
                            index = al_commons.add2nparr(index, index[-1] + 1)
                            rcounter = rcounter + 1
                elif (rformat == 2):
                    GNtw_r1 = line[5:17].strip()
                    GNtw_r2 = line[17:36].strip()
                    GNtw_p1 = line[41:53].strip()
                    GNtw_p2 = line[53:65].strip()
                    GNtw_p3 = line[65:77].strip()
                    GNtw_p4 = line[77:89].strip()
                    GNtw_p5 = line[89:100].strip()

                    GNtw_alpha = float(line[101:110])
                    GNtw_beta = float(line[110:119])
                    GNtw_gamma = float(line[119:128])

                    if ( (GNtw_r1.replace("g","") in SpecFound)
                            and (GNtw_r2.replace("g","") in SpecFound)
                            and (GNtw_p1.replace("g","") in SpecFound)
                            and ("g" in GNtw_p1) ):
                        r1 = al_commons.add2nparr(r1, GNtw_r1)
                        r2 = al_commons.add2nparr(r2, GNtw_r2)
                        p1 = al_commons.add2nparr(p1, GNtw_p1)
                        p2 = al_commons.add2nparr(p2, GNtw_p2)
                        p3 = al_commons.add2nparr(p3, GNtw_p3)
                        p4 = al_commons.add2nparr(p4, GNtw_p4)
                        p5 = al_commons.add2nparr(p5, GNtw_p5)

                        alpha = al_commons.add2nparr(alpha, GNtw_alpha)
                        beta = al_commons.add2nparr(beta, 0.0)

                        Tmin = al_commons.add2nparr(Tmin, -9999.)
                        Tmax = al_commons.add2nparr(Tmax, 9999.)
                        rtype = al_commons.add2nparr(rtype, 14)
                        formul = al_commons.add2nparr(formul, 3)
                        multiform = al_commons.add2nparr(multiform, 1)
                        index = al_commons.add2nparr(index, index[-1] + 1)
                        rcounter = rcounter + 1
                        try:
                            jj = ActEn_react.index(GNtw_r1 + '+' + GNtw_r2 +
                                        '->' + GNtw_p1)
                            if (GNtw_beta != ActEn[jj]) and (GNtw_beta != 0. or ActEn[jj] != 0.):
                                print( ActEn_react[jj], ActEn[jj], GNtw_gamma )
                            ActivationEnergy = GNtw_beta #ActEn[jj]
                        except:
                            ActivationEnergy = GNtw_beta
                        # Activation energy of the reaction
                        gamma = al_commons.add2nparr(gamma, ActivationEnergy)
                elif (rformat == 3):
                    print( line )
                    GNtw_r1 = line[6:17].strip()
                    if "*" in GNtw_r1:
                        GNtw_r1 = 'g' + GNtw_r1.replace("*","")
                        GNtw_r1.replace("gH2O2","gHOOH")
                        GNtw_r1.replace("gH2S2","gHSSH")
                        GNtw_r1.replace("gHS2","gHSS")
                        GNtw_r1.replace("gSIC3","gl-SiC3")
                        GNtw_r1.replace("gSIC2H2","gHCCHSI")
                        GNtw_r1.replace("gSIC2","gHCCHSI")
                        GNtw_r1.replace("gSiC2H","gHCCSI")
                    GNtw_r2 = line[17:28].strip()
                    if "*" in GNtw_r2:
                        GNtw_r2 = 'g' + GNtw_r2.replace("*","")
                        GNtw_r2.replace("gH2O2","gHOOH")
                        GNtw_r2.replace("gH2S2","gHSSH")
                        GNtw_r2.replace("gHS2","gHSS")
                        GNtw_r2.replace("gSIC3","gl-SiC3")
                        GNtw_r2.replace("gSIC2H2","gHCCHSI")
                        GNtw_r2.replace("gSIC2","gHCCHSI")
                        GNtw_r2.replace("gSiC2H","gHCCSI")
                    GNtw_p1 = line[28:39].strip()
                    if "*" in GNtw_p1:
                        GNtw_p1 = 'g' + GNtw_p1.replace("*","")
                        GNtw_p1.replace("gH2O2","gHOOH")
                        GNtw_p1.replace("gH2S2","gHSSH")
                        GNtw_p1.replace("gHS2","gHSS")
                        GNtw_p1.replace("gSIC3","gl-SiC3")
                        GNtw_p1.replace("gSIC2H2","gHCCHSI")
                        GNtw_p1.replace("gSIC2","gHCCHSI")
                        GNtw_p1.replace("gSiC2H","gHCCSI")
                    GNtw_p2 = line[39:50].strip()
                    if "*" in GNtw_p2:
                        GNtw_p2 = 'g' + GNtw_p2.replace("*","")
                        GNtw_p2.replace("gH2O2","gHOOH")
                        GNtw_p2.replace("gH2S2","gHSSH")
                        GNtw_p2.replace("gHS2","gHSS")
                        GNtw_p2.replace("gSIC3","gl-SiC3")
                        GNtw_p2.replace("gSIC2H2","gHCCHSI")
                        GNtw_p2.replace("gSIC2","gHCCHSI")
                        GNtw_p2.replace("gSiC2H","gHCCSI")
                    GNtw_p3 = line[50:61].strip()
                    if "*" in GNtw_p3:
                        GNtw_p3 = 'g' + GNtw_p3.replace("*","")
                        GNtw_p3.replace("gH2O2","gHOOH")
                        GNtw_p3.replace("gH2S2","gHSSH")
                        GNtw_p3.replace("gHS2","gHSS")
                        GNtw_p3.replace("gSIC3","gl-SiC3")
                        GNtw_p3.replace("gSIC2H2","gHCCHSI")
                        GNtw_p3.replace("gSIC2","gHCCHSI")
                        GNtw_p3.replace("gSiC2H","gHCCSI")
                    GNtw_p4 = ""
                    GNtw_p5 = ""
                    # Rename species that they match with kida

                    GNtw_alpha = float(line[71:80])
                    GNtw_beta = float(line[81:90])
                    GNtw_gamma = float(line [91:100])

                    GNtw_formul = float(line [103:106])

                    if ( ((GNtw_r1.replace("g","") in SpecFound) and
                        ((GNtw_r2.replace("g","") in SpecFound) or
                        GNtw_r2.replace("g","") in Accepted) and
                        (GNtw_p1.replace("g","") in SpecFound) and
                        ("g" in GNtw_p1) ) or AddAll ):
                        r1 = al_commons.add2nparr(r1, GNtw_r1)
                        r2 = al_commons.add2nparr(r2, GNtw_r2)
                        p1 = al_commons.add2nparr(p1, GNtw_p1)
                        p2 = al_commons.add2nparr(p2, GNtw_p2)
                        p3 = al_commons.add2nparr(p3, GNtw_p3)
                        p4 = al_commons.add2nparr(p4, GNtw_p4)
                        p5 = al_commons.add2nparr(p5, GNtw_p5)

                        alpha = al_commons.add2nparr(alpha, GNtw_alpha)
                        beta = al_commons.add2nparr(beta, 0.0)
#                        gamma = al_commons.add2nparr(gamma, GNtw_beta)

                        Tmin = al_commons.add2nparr(Tmin, -9999.)
                        Tmax = al_commons.add2nparr(Tmax, 9999.)
                        rtype = al_commons.add2nparr(rtype, 14)
                        formul = al_commons.add2nparr(formul, GNtw_formul)
                        multiform = al_commons.add2nparr(multiform, 1)
                        index = al_commons.add2nparr(index, index[-1] + 1)
                        rcounter = rcounter + 1
                        try:
                            jj = ActEn_react.index(GNtw_r1 + '+' + GNtw_r2 +
                                        '->' + GNtw_p1)
                            if (GNtw_beta != ActEn[jj]) and (GNtw_beta != 0. or ActEn[jj] != 0.):
                                print( ActEn_react[jj], ActEn[jj], GNtw_beta )
                                print( "No changes were made!" )
                        except:
                            ActivationEnergy = GNtw_beta
                        # Activation energy of the reaction
                        gamma = al_commons.add2nparr(gamma, ActivationEnergy)
                elif (rformat == 4):
                    GNtw_r1 = line[0:11].strip()
                    if "*" in GNtw_r1:
                        GNtw_r1 = 'g' + GNtw_r1.replace("*","")
                    GNtw_r2 = line[11:34].strip()
                    if "*" in GNtw_r2:
                        GNtw_r2 = 'g' + GNtw_r2.replace("*","")
                    GNtw_p1 = line[34:45].strip()
                    if "*" in GNtw_p1:
                        GNtw_p1 = 'g' + GNtw_p1.replace("*","")
                    GNtw_p2 = line[45:56].strip()
                    if "*" in GNtw_p2:
                        GNtw_p2 = 'g' + GNtw_p2.replace("*","")
                    GNtw_p3 = line[56:67].strip()
                    if "*" in GNtw_p3:
                        GNtw_p3 = 'g' + GNtw_p3.replace("*","")
                    GNtw_p4 = ""
                    GNtw_p5 = ""

                    GNtw_alpha = float(line[90:100])
                    GNtw_beta = float(line[101:111])
                    GNtw_gamma = float(line[112:122])

                    GNtw_formul = 3 # float(line[165:167])

                    if ( ((GNtw_r1.replace("g","") in SpecFound) and
                        (GNtw_r2.replace("g","") in SpecFound) and
                        (GNtw_p1.replace("g","") in SpecFound) and
                        ("g" in GNtw_p1) ) or AddAll ):
                        r1 = al_commons.add2nparr(r1, GNtw_r1)
                        r2 = al_commons.add2nparr(r2, GNtw_r2)
                        p1 = al_commons.add2nparr(p1, GNtw_p1)
                        p2 = al_commons.add2nparr(p2, GNtw_p2)
                        p3 = al_commons.add2nparr(p3, GNtw_p3)
                        p4 = al_commons.add2nparr(p4, GNtw_p4)
                        p5 = al_commons.add2nparr(p5, GNtw_p5)

                        alpha = al_commons.add2nparr(alpha, GNtw_alpha)
                        beta = al_commons.add2nparr(beta, GNtw_beta)
#                        gamma = al_commons.add2nparr(gamma, GNtw_gamma)

                        Tmin = al_commons.add2nparr(Tmin, -9999.)
                        Tmax = al_commons.add2nparr(Tmax, 9999.)
                        rtype = al_commons.add2nparr(rtype, 14)
                        formul = al_commons.add2nparr(formul, GNtw_formul)
                        multiform = al_commons.add2nparr(multiform, 1)
                        index = al_commons.add2nparr(index, index[-1] + 1)
                        rcounter = rcounter + 1
                        try:
                            jj = ActEn_react.index(GNtw_r1 + '+' + GNtw_r2 +
                                        '->' + GNtw_p1)
                            if (GNtw_beta != ActEn[jj]) and (GNtw_beta != 0. or ActEn[jj] != 0.):
                                print( ActEn_react[jj], ActEn[jj], GNtw_beta )
                                ActivationEnergy = ActEn[jj]
                        except:
                            ActivationEnergy = GNtw_beta
                        # Activation energy of the reaction
                        gamma = al_commons.add2nparr(gamma, ActivationEnergy)
                    else:
                        if (GNtw_r1.replace("g","") not in SpecFound):
                            if (GNtw_r1.replace("g","") not in SpecNotFound):
                                SpecNotFound.append(GNtw_r1.replace("g",""))
                        if (GNtw_r2.replace("g","") not in SpecFound):
                            if GNtw_r2.replace("g","") not in SpecNotFound:
                                SpecNotFound.append(GNtw_r2.replace("g",""))
                        if (GNtw_p1.replace("g","") not in SpecFound):
                            if GNtw_p1.replace("g","") not in SpecNotFound:
                                SpecNotFound.append(GNtw_p1.replace("g",""))

                if ( AddRdes and (GNtw_r2 not in Accepted) ):
                    exo = check_exoendo( GNtw_r1.replace("g",""),
                                GNtw_r2.replace("g",""),
                                GNtw_p1.replace("g",""),
                                enthalpies )
#                    if bool(exo):
#                    	print( "  Exothermic, applying reactive desorption\n" )
#                    else:
#                        print( "  Endothermic, NO reactive desorption\n" )
                    if ( (chkExo and bool(exo)) or not chkExo):
                        print( "adding reactive desorption" )
                        r1 = al_commons.add2nparr(r1, GNtw_r1)
                        r2 = al_commons.add2nparr(r2, GNtw_r2)
                        p1 = al_commons.add2nparr(p1, GNtw_p1.replace("g",""))
                        p2 = al_commons.add2nparr(p2, GNtw_p2.replace("g",""))
                        p3 = al_commons.add2nparr(p3, GNtw_p3.replace("g",""))
                        p4 = al_commons.add2nparr(p4, GNtw_p4.replace("g",""))
                        p5 = al_commons.add2nparr(p5, GNtw_p5.replace("g",""))

                        excess = excess_energy(GNtw_r1,GNtw_r2,GNtw_p1,GNtw_p2,
                              GNtw_p3, enthalpies, binding_energy)

                        alpha = al_commons.add2nparr(alpha, GNtw_alpha)
                        beta[-1] = excess # TODO: solve this better
                        beta = al_commons.add2nparr(beta, excess)
                        gamma = al_commons.add2nparr(gamma, GNtw_beta)

                        Tmin = al_commons.add2nparr(Tmin, -9999.)
                        Tmax = al_commons.add2nparr(Tmax, 9999.)
                        # change rtype for the grain product reaction as well
                        rtype[-1] = 99
                        # let's call it 99 for now
                        rtype = al_commons.add2nparr(rtype, 99)
                        formul = al_commons.add2nparr(formul, 3)
                        multiform = al_commons.add2nparr(multiform, GNtw_formul)
                        index = al_commons.add2nparr(index, index[-1] + 1)
                        rcounter = rcounter + 1

    print( "\nSpecies not found:" )
    for ntf in SpecNotFound:
        print( ntf )
    f.close()
    if AddGr:
        print( "{} grain surface reactions are found and added!".format(rcounter) )
    return network(index, r1, r2, p1, p2, p3, p4, p5, alpha, beta, gamma,
                   Tmin, Tmax, rtype, formul, multiform, comment)

def check_exoendo(r1, r2, p1, enthalpies):
    """
    Check whether the given reaction is exothermic or endothermic.
    This code is from Olli Sipilae (2017)
    """

    if r1 not in enthalpies.keys(): return 0
    elif r2 not in enthalpies.keys(): return 0
    elif p1 not in enthalpies.keys(): return 0

    ent1 = enthalpies[r1]
    ent2 = enthalpies[r2]
    ent3 = enthalpies[p1]

    exoerg = -(ent3 - (ent1 + ent2))
    print( r1, r2, p1, exoerg )
    if exoerg >= 0.0: return 1
    else: return 0

def excess_energy(r1, r2, p1, p2, p3, enthalpies, binding_energy):
    """
    This functin precomputes the excess energy of a reaction that
    is available to break the surface bonds of products.

    Based on NAUTILUS code. (Wakelam 2017)
    """
    AVOGADRO = 6.02214129e+23 #avogadro number : number of atom in 1 mol

    r1 = r1.replace("g","")
    r2 = r2.replace("g","")
    p1 = p1.replace("g","")
    p2 = p2.replace("g","")
    p3 = p3.replace("g","")

    print( r1, r2, p1, p2, p3, enthalpies['C'] )

    ent1 = enthalpies[r1]
    ent2 = enthalpies[r2]
    ent3 = enthalpies[p1]

    if (p2 != ''): ent4 = enthalpies[p2]
    else: 	   ent4 = 0.
    if (p3 != ''): ent5 = enthalpies[p3]
    else: 	   ent5 = 0.

    bind1 = binding_energy[r1]
    bind2 = binding_energy[r2]
    bind3 = binding_energy[p1]

    if (p2 != ''): bind4 = binding_energy[p2]
    else: 	   bind4 = 0.
    if (p3 != ''): bind5 = binding_energy[p3]
    else: 	   bind5 = 0.

    dhfsum = ent1 + ent2 - ent3 - ent4 - ent5
    # convert units
    dhfsum = dhfsum * 4.184e3 / 1.38054e-23 / AVOGADRO

    sum1 = max([bind3, bind4, bind5])
    if dhfsum != 0.0e0:
        sum2 = 1.0e0 - (sum1 / dhfsum)

        atoms = al_commons.atom_count(p1)['natm']
        if (atoms == 2):
            sum2 = sum2**(3.0e0 * atoms - 5)
        else:
            sum2 = sum2**(3.0e0 * atoms - 6)
        print( atoms, sum2*1e-2/(1.+sum2*1e-2), "\n" )
    else:
        sum2 = 0.0e0
        print( sum2*1e-2/(1.+sum2*1e-2), "\n" )
    return sum2
