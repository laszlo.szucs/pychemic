#!/usr/bin/env python

from distutils.core import setup

setup(name='pychemic',
      version='0.2',
      requires=['h5py', 'numpy', 'matplotlib', 'scipy','astropy'],
      description='Chemical model and network manipulation tool for MPChem',
      author='Laszlo Szucs',
      author_email='laszlo.szucs@mpe.mpg.de',
      url='https://gitlab.mpcdf.mpg.de/szucs/pychemic',
      packages=['pychemic'],
      package_data={'.':['docs/']},
     )
