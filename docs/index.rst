.. PyChemic documentation master file, created by
   sphinx-quickstart on Wed Aug 10 12:08:16 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyChemic's documentation!
====================================

This is a python code for reading, plotting, analyzing and setting up ALCHEMIC_SPH \
and MPChem chemical models. The features of the module include creating chemical \
networks from scratch, updating them if new measurements or calculations are \
available on some reaction rates, easy comparison of chemical models, reaction \
rate analysis, and the creation of reaction network graphs.


Contents:
=========
.. toctree::
   :maxdepth: 2

   pychemic


Requirements:
=============

* `NumPy <http://www.numpy.org/>`_
* `Matplotlib <http://matplotlib.org/>`_
* `PyGraphViz <https://pygraphviz.github.io/>`_
* `pytest <http://docs.pytest.org/en/latest/>`_ (for code testing)

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

