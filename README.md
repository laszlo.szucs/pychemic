PyChemic
--------

This is a Python module for reading, plotting, analyzing and setting up 
[ALCHEMIC_SPH](https://gitlab.mpcdf.mpg.de/szucs/alchemic_sph) and 
[MPChem](https://gitlab.mpcdf.mpg.de/szucs/mpchem)
chemical models. The features of the module include creating chemical
networks from scratch, updating them if new measurements or calculations are
available on some reaction rates, easy comparison of chemical models, reaction 
rate analysis, and the creation of reaction network graphs.